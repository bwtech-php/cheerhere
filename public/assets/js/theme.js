document.querySelectorAll('.timeline a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
    $('[data-toggle="tooltip"]').tooltip()
});
function openNav() {
    document.getElementById("myNav").style.width = "50%";
}
function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}
//
// $('body').on('click', '.parent-nav' , function(e){
// 	e.preventDefault()
// 	var self = $(this);
// 	self.slideUp();
// 	  self.closest('.parent-nav').find('.sub-menu').slideDown();
// });
// $('body').on('click', '.sub-menu', function(event) {
// 	event.preventDefault();
// 	var self = $(this);
// 	self.closest('.sub-menu').slideUp();
// 	self.closest('.sub-menu').closest('.sub-menu').closest('li').find('.parent-nav').slideDown();
// });
// $(document).ready(function(){
// 	// Show hide popover
// 	$(".dropdown").click(function(){
// 		$(this).find(".dropdown-menu").slideToggle("fast");
// 	});
// });
// $(document).on("click", function(event){
// 	var $trigger = $(".dropdown");
// 	if($trigger !== event.target && !$trigger.has(event.target).length){
// 		$(".dropdown-menu").slideUp("fast");
// 	}
// });
// $(function() {
// 	$('#menu-navigation').click(function() {
// 		$('.parent-nav').css('margin-left', '-=100px');
// 	});
// });
// $(document).ready(function(){
//
// 	(function($) {
//
// 		$('#menu-navigation').click(function(e){
// 			e.preventDefault();
// 			$('body').toggleClass('.sub-menu');
// 		});
//
// 		$('.sub-menu').click(function(e){
// 			$('body').removeClass('#menu-navigation');
// 		});
//
// 	})(jQuery);
//
// });
$(document).ready(function() {
    $('body').on('click', '.parent-nav > a', function(event) {
        event.preventDefault();
        var self = $(this)
        if (self.closest('li').find('.sub-menu').length > 0) {
            self.closest('li').find('.sub-menu').css({
                'background': 'transparent',
                'border': 'none',
                'opacity': '1',
                'margin-left': '50%',
                'top': '0',
                'display': 'block',
                'position': 'absolute',
                'transition': '1s'
            });
            $('#menu-navigation').css('margin-left', '-50%');
            $('.parent-nav').each(function(index) {
                $(this).find('.parent_menu_master').css({
                    visibility: 'hidden',
                    opacity: '0'
                });
                // $(this).
            });
        }
    });
    $('body').on('click', '.sub_menu_icon', function(event) {
        event.preventDefault();
        $(this).closest('.sub-menu').removeAttr('style');
        $(this).closest('.sub-menu').css('opacity', 0)
        $('#menu-navigation').removeAttr('style');
        $('.parent-nav').each(function(index) {
            $(this).find('.parent_menu_master').removeAttr('style');
        });
    });
});




