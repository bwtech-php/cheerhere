
$(document).ready(function() {

	$("body").on('click','.singleDelete', function(e) {
            e.preventDefault(); // Disable link functionality.

            var href = $(this).attr('href');
            var self = $(this);
            var selfHtml = $(this).html();

            self.html("<img src='/assets/ajax-loader.gif'>");
           swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then((willDelete) => {
                  if (willDelete.value) {

                    $.ajax({
                        url: href,
                        type: 'DELETE',
                        dataType: 'json',
                    })
                    .done(function(response) {
                       if (response.status)
                        {
                            self.closest('tr').css('background-color', '#f88f9e').fadeOut('slow');
                            setTimeout(function() { self.closest('tr').remove(); }, 3000);
                        }
                    })
                    .fail(function() {
                    })
                    self.html(selfHtml);
                  }else{
                    self.html(selfHtml);
                  }
                });
            });

    $('table').DataTable();

});