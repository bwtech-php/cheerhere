<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use App\User;
// use Auth;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Admin'] , function(){

	Auth::routes(['register' => false]);
});

//Admin Routes
Route::group(['middleware' => 'Admin', 'namespace' => 'Admin' ,'prefix' => 'admin'] , function(){

	Route::get('dashboard' , 'HomeController@dashboard')->name('dashboard');

	Route::resource('team', 'TeamController', ['as' => 'admin']);
	Route::resource('skill', 'SkillController', ['as' => 'admin']);
	Route::resource('school', 'SchoolController', ['as' => 'admin']);
	Route::resource('coach', 'CoachController');
	Route::resource('member', 'MemberController');

	//Admins
	Route::get('admins', 'AdminController@show')->name('admins');
	Route::get('admin/form', 'AdminController@newAdmin')->name('newAdmin');
	Route::post('admin/new/create', 'AdminController@store')->name('createNewAdmin');
	Route::get('admin/form/{id}', 'AdminController@form')->name('editAdmin');
	Route::put('admin/update/{id}', 'AdminController@update')->name('updateAdmin');
	Route::get('admin/delete/{id}', 'AdminController@destroy')->name('deleteAdmin');

	//Member
	Route::get('member/delete/{id}', 'MemberController@deleteMember')->name('memberDelete');
	Route::get('coach/delete/{id}', 'CoachController@deleteCoach')->name('coachDelete');

	//Clinics
	Route::get('clinics', 'HomeController@clinics')->name('clinics');

});



//Auth routes
Route::group(['middleware' => ['auth']], function () {
	//Tickets
	Route::get('tickets' , 'TicketController@index')->name('tickets');
	Route::get('tickets/new' , 'TicketController@newTicketForm')->name('newTicketForm');
	Route::post('tickets/new/create' , 'TicketController@createTicket')->name('createTicket');
	Route::get('tickets/close/{id}' , 'TicketController@closeTicket')->name('closeTicket');
	Route::get('tickets/detail/{id}' , 'TicketController@ticketDetail')->name('ticketDetail');
	Route::get('tickets/delete/{id}' , 'TicketController@ticketDelete')->name('ticketDelete');
	
	
	//Comments
	Route::post('comment/new', 'CommentController@createComment')->name('createComment');
	
});

//Coach Routes
Route::group(['middleware' => 'Coach' , 'prefix' => 'coach' , 'namespace' => 'Coach'] , function(){
	Route::get('/dashboard' , 'DashboardController@index')->name('coach.dashboard');
	Route::get('/profile' , 'DashboardController@profile')->name('coach.profile');
	Route::post('/profile/update' , 'DashboardController@profileUpdate')->name('coach.profile.update');

	Route::get('schedule-clinic' , 'ClinicController@index')->name('coach.clinic');
	Route::get('schedule-clinic/clinic/update/{id}' , 'ClinicController@updateClinic')->name('updateClinic');
	Route::get('schedule-clinic/{id}/show' , 'ClinicController@show')->name('coach.clinic.show');
	Route::post('schedule-clinic/store' , 'ClinicController@store')->name('coach.clinic.store');
	Route::get('schedule-clinic/{id}/edit' , 'ClinicController@edit')->name('coach.clinic.edit');
	Route::put('schedule-clinic/update/{id}' , 'ClinicController@update')->name('coach.clinic.update');
	Route::get('schedule-clinic/delete/{id}' , 'ClinicController@delete')->name('coach.clinic.delete');
	Route::get('summary' , 'DashboardController@coachSummary')->name('coach.summary');

	Route::get('clinic/registers/{id}' , 'ClinicController@clinicEnroll')->name('coach.clinic.enroll');
	Route::post('uniformrequirements' , 'DashboardController@setUniformRequirements')->name('uniform.requirements');

	Route::get('/clinics/all' , 'ClinicController@getAllClinics')->name('coach.clinics.all');
	
});
Route::post('clinic/location/new' , 'ClinicLocationController@store')->name('clinic.location.store');

//Member Routes
Route::group(['middleware' => 'Member' , 'prefix' => 'member' , 'namespace' => 'Member'] , function(){

    Route::get('/dashboard' , 'DashboardController@index')->name('member.dashboard');
    Route::get('/enrolled/clinics' , 'DashboardController@enrolled_clinic');
	Route::get('/profile', 'ProfileController@index')->name('member.profile');
	Route::post('/profile/update', 'ProfileController@profileUpdate')->name('member.profile.update');
	// Route::get('schedule/clinic' , 'DashboardController@scheduleClinic')->name('member.schedule.clinic');

});



// Route::post('/member/login' , 'Admin\Auth\LoginController@login')->name('login.attempt');

Route::group(['middleware' => 'guest'], function() {
	Route::get('register/member' , 'RegisterController@memberRegisterForm')->name('member.register.form');
	Route::post('register/member/store' , 'RegisterController@memberStore')->name('member.register.store');
	Route::get('register/coach' , 'RegisterController@coachRegisterForm')->name('coach.register.form');
	// Route::view('user/login' , 'member.login')->name('user.login');
});
	Route::post('register/school/coach' , 'RegisterController@school')->name('coach.school.store');


Route::view('about' , 'pages.about' )->name('about');
Route::get('/' , 'HomeController@index')->name('home');

Route::get('/clinics' , 'ClinicController@index')->name('clinic.index');
Route::get('clinics/search' , 'ClinicController@searchCLinics')->name('clinicSearch');
Route::get('clinic/detail/{id}', 'ClinicController@detail')->name('clinic.detail');
Route::get('clinic/enrolled/detail/{id}', 'ClinicController@enrolledDetail')->name('clinic.enrolled.detail');
Route::get('clinic/enroll/{id}' , 'ClinicController@enroll')->name('enroll.index');
Route::post('clinic/enroll/update/{id}' , 'ClinicController@updateClinic')->name('clinic.enroll.update');

Route::get('contact-us' , 'ContactController@index')->name('contact');

Route::get('/pay','PaymentController@pay')->name('pay');
Route::post('/dopay/online', 'PaymentController@handleonlinepay')->name('dopay.online');

Route::get('/waiverA',function(){

    return view('waivers.waiverA');
});

Route::get('/waiverB','WaiverController@waiverB');
Route::post('/waiverB/submit','WaiverController@waiverB_send');
Route::get('/waiver/view/{id}','WaiverController@waiver_view');
Route::get('/waiverA','WaiverController@waiverA');
Route::post('/waiver/submit','WaiverController@WaiverA_submit');

Route::post('/notes/new','Member\DashboardController@notes');

Route::get('enrollments/paid', [
    'uses' => 'Coach\DashboardController@paidEnrollments',
    'as' => 'paid-enrollments'
]);

Route::get('enrollments/unpaid', [
    'uses' => 'Coach\DashboardController@unPaidEnrollments',
    'as' => 'unpaid-enrollments'
]);

Route::get('mailable', function () {
	// Mail::to(Auth::user())->send(new NewComment($ticket));
	
	$ticket = App\Ticket::find(18);
	$clinic = App\Clinic::find(7);

	// return new App\Mail\newUserRegistration();
	// return new App\Mail\clinicEnrollment($clinic);
	return new App\Mail\PasswordResetConfirmation();
});

Route::get('/test', function(Request $request) {
	return Auth::user();
	
	
	// return Auth::user()->email;
	// return 2 + 1;
	// return $request;
	// return App\Clinic::find(1)->with('clinicDetails')->get();
	// return Auth::user();
	// return view('auth.passwords.reset');
});

Route::get('coach/school/detail', 'Coach\DashboardController@getSchool');