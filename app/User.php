<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function schools()
    {
        return $this->belongsToMany(School::class, 'school_notifications', 'user_id', 'school_id');
    }

    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'user_skills', 'user_id', 'skill_id');
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'user_teams', 'user_id', 'team_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
 
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function minor()
    {
        return $this->hasOne(UniformRequirement::class, 'coach_id', 'id')->where('uniform_requirements.uniform_type', 'Minor');
    }

    public function freshman()
    {
        return $this->hasOne(UniformRequirement::class, 'coach_id', 'id')->where('uniform_requirements.uniform_type', 'Freshman');
    }

    public function sophomore()
    {
        return $this->hasOne(UniformRequirement::class, 'coach_id', 'id')->where('uniform_requirements.uniform_type', 'Sophomore');
    }

    public function junior()
    {
        return $this->hasOne(UniformRequirement::class, 'coach_id', 'id')->where('uniform_requirements.uniform_type', 'Junior');
    }

    public function senior()
    {
        return $this->hasOne(UniformRequirement::class, 'coach_id', 'id')->where('uniform_requirements.uniform_type', 'Senior');
    }

    public function school()
    {
        return $this->hasOne(School::class, 'id', 'school_id');
    }
}
