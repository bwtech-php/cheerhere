<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clinicLocation extends Model
{
    /**
     * Get the user associated with the clinicLocation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
}
