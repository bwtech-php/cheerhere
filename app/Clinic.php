<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    protected $guarded = [];

    public function school()
    {
    	return $this->belongsTo(School::class , 'school_id');
    }

     public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = strtolower($value);
    }

    public function clinicDetails()
    {
        return $this->hasOne('App\clinicDetails');
    }
}