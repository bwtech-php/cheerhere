<?php

namespace App\Mail;

use App\Clinic;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class clinicEnrollment extends Mailable
{
    use Queueable, SerializesModels;

    public $clinic;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Clinic $clinic)
    {
        $this->clinic = $clinic;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@cheerhere.com')
                    ->markdown('emails.clinicenrollment');
    }
}
