<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enroll extends Model
{
    protected $guarded = [];

    public function clinic()
    {
    	return $this->belongsTo(Clinic::class , 'clinic_id');
    }
}
