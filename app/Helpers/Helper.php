<?php
    function createSlug($title , $table)
    {
        $slug = str_slug($title);
        $firstSlug =  \DB::table($table)->select('slug')->where('slug', 'like', $slug.'%')->get();
        if (! $firstSlug->contains('slug', $slug)){
            return $slug;
        }
        for ($i = 1; $i <= $i; $i++) {
            $newSlug = $slug.'-'.$i;
            $scendSlug =  \DB::table($table)->select('slug')->where('slug', 'like', $newSlug.'%')->get();
            if (! $scendSlug->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
    }

    function checkboxChecked($checkboxvalues , $id){        
        $checkboxChecked = '';
            if (count($checkboxvalues) > 0) {
                foreach ($checkboxvalues as $key => $checkbox) {
                    if ($checkbox->id == $id) {
                        $checkboxChecked = 'checked';
                    }
                }
            }
        return $checkboxChecked;
    }

    function selectOptionSelect($selectOptionValues , $id){        
        $selectSelected = '';
            if (count($selectOptionValues) > 0) {
                foreach ($selectOptionValues as $key => $select) {
                    if ($select->id == $id) {
                        $selectSelected = 'selected';
                    }
                }
            }
        return $selectSelected;
    }

    function image($path)
    {
        return $path && file_exists($path) ? asset($path) : asset('/assets/images/product-1.png');
    }


    function dateformate($date)
    {
        return date('d,D M Y' , strtotime($date));
    }

    function timeformate($time)
    {
        return date('H:i a' , strtotime($time));
    }


    function authredirect()
    {
        if (Auth::user()->type == 'Admin') {
            
            return route('dashboard');
        
        }elseif (Auth::user()->type == 'Member') {
        
            return route('member.dashboard');
        
        }elseif(Auth::user()->type == 'Coach'){
            
            return route('coach.dashboard');
        }

        return '/';
    }

    function assigned_waiver($value)
    {
         return str_replace('_', ' ', $value);
    }

    function age($data)
    {
         $date = new DateTime($data);
         $now = new DateTime();
         $interval = $now->diff($date);
         return $interval->y;
    }






 ?>