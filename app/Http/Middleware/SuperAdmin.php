<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class SuperAdmin
{
    // /**
    //  * Handle an incoming request.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \Closure  $next
    //  * @return mixed
    //  */
    // public function handle($request, Closure $next)
    // {
    //     return $next($request);
    // }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->to(route('login'));
        }else{

            if (Auth::user()->type == 'Super Admin') {
                return $next($request);
            }

            return redirect(authredirect());
        }
    }
}
