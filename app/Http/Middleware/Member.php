<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Member
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->to(route('login'));
        }else{

            if (Auth::user()->type == 'Member') {
                return $next($request);
            }

            return redirect(authredirect());
        }
    }
}
