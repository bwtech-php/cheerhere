<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->to(route('login'));
        }else{

            if (Auth::user()->type == 'Admin' || Auth::user()->type == 'Super Admin') {
                // dd(4);
                return $next($request);
            }
            // dd(4);
            return redirect(authredirect());
        }

    }
}
