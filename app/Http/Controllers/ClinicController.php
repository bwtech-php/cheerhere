<?php

namespace App\Http\Controllers;

use Auth;
use App\State;
use App\Clinic;
use App\clinicDetails;
use App\Enroll;
use App\School;
use Carbon\Carbon;
use Illuminate\Http\Request;

//Mail
use App\Mail\clinicEnrollment;
use Illuminate\Support\Facades\Mail;

class ClinicController extends Controller
{
    public function index(Request $request)
    {
        // $clinics = Clinic::whereHas('school' , function($query) use ($request){

        //     if ($request->q) {
        //        $query->where('pay_by_date','>=',date('Y-m-d'))->where('name', 'LIKE', "%".$request->q."%");
        //     }

        // })->where('pay_by_date','>=',date('Y-m-d'))->Where('assigned_waiver' , 'LIKE', $request->q)->latest()->get();

        $clinics = Clinic::join('schools', 'clinics.school_id', '=', 'schools.id')
        ->where('clinics.pay_by_date','>=',date('Y-m-d'))
        ->where('clinics.assigned_waiver' , 'LIKE', $request->q)
        ->latest()
        ->select('clinics.*', 'schools.image')
        ->get();

    	return view('clinic.index' , compact('clinics'));
    }

    public function detail($id)
    {
        // dd(2);
        // dd($id);
        $dateCheck = Clinic::find($id);
        if($dateCheck->pay_by_date <= date('Y-m-d')) {
            return back()->with('status', 'Sorry this clinic is currently not accepting any enrollments');
        }

        $row = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
        ->join('schools', 'clinics.school_id', '=', 'schools.id')
        ->join('clinic_locations', 'clinics.clinic_location_id', '=', 'clinic_locations.id')
        ->join('states', 'clinic_locations.state_id', '=', 'states.id')
        ->where('clinics.id',$id)
        ->where('clinics.pay_by_date','>=',date('Y-m-d'))
        // ->select('clinics.*', 'schools.name as school_name', 'schools.state as school_state', 'schools.city as school_city', 'schools.country as school_country', 'schools.address as school_address', 'waivers.id as waiver_id', 'waivers.*', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
        ->select('clinics.id as clinic_id', 'clinics.user_id as clinic_user_id', 'clinics.school_id as clinic_school_id', 'clinics.open_age_to as clinic_open_age_to', 'clinics.open_age_from as clinic_open_age_from', 'clinics.assigned_waiver as clinic_assigned_waiver', 'clinics.date as clinic_date', 'clinics.time as clinic_time', 'clinics.in_time as clinic_in_time', 'clinics.cost as clinic_cost', 'clinics.what_to_wear as clinic_what_to_wear', 'clinics.pay_by_date as clinic_pay_by_date', 'clinics.created_at as clinic_created_at', 'clinics.updated_at as clinic_updated_at', 'schools.id as school_id', 'schools.name as school_name', 'schools.state as school_state', 'schools.city as school_city', 'schools.country as school_country', 'schools.address as school_address', 'schools.created_at as school_created_at', 'schools.updated_at as school_updated_at', 'waivers.id as waiver_id', 'waivers.name as waiver_name', 'waivers.file as waiver_file', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at', 'clinic_locations.id as clinic_location_id', 'clinic_locations.name as clinic_location_name', 'clinic_locations.address as clinic_location_address', 'clinic_locations.city as clinic_location_city', 'clinic_locations.zip as clinic_location_zip', 'states.name as state_name')
        ->first();

        $clinicDetails = clinicDetails::where('clinic_id', $id)->get();

        $enrollCheck = Enroll::where('clinic_id' , $id)->where('member_id', Auth::id())->first();
        
        // $school=School::where(['id'=>$row->school_id])->first();
        $school=School::where('id', $row->school_id)->first();
        // dd($school);

        if(Auth::check()) {
            $state = State::find(Auth::user()->state);

            return view('clinic.detail', compact(['row' , 'school', 'id','enrollCheck', 'state', 'clinicDetails']));
        }
        else {
            return view('clinic.detail', compact(['row' , 'school', 'id','enrollCheck', 'clinicDetails']));
        }

        return view('clinic.detail', compact(['row' , 'school', 'id','enrollCheck', 'state', 'clinicDetails']));

        // return view('clinic.detail', compact(['row','school']));
    }

    public function enrolledDetail($id)
    {
        // dd(2);
        // dd($id);
        $dateCheck = Clinic::find($id);
        // if($dateCheck->pay_by_date <= date('Y-m-d')) {
        //     return back()->with('status', 'Sorry this clinic is currently not accepting any enrollments');
        // }

        $row = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
        ->join('schools', 'clinics.school_id', '=', 'schools.id')
        ->where('clinics.id',$id)
        // ->where('clinics.pay_by_date','>=',date('Y-m-d'))
        // ->select('clinics.*', 'schools.name as school_name', 'schools.state as school_state', 'schools.city as school_city', 'schools.country as school_country', 'schools.address as school_address', 'waivers.id as waiver_id', 'waivers.*', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
        ->select('clinics.id as clinic_id', 'clinics.user_id as clinic_user_id', 'clinics.school_id as clinic_school_id', 'clinics.open_age_to as clinic_open_age_to', 'clinics.open_age_from as clinic_open_age_from', 'clinics.assigned_waiver as clinic_assigned_waiver', 'clinics.date as clinic_date', 'clinics.time as clinic_time', 'clinics.in_time as clinic_in_time', 'clinics.cost as clinic_cost', 'clinics.what_to_wear as clinic_what_to_wear', 'clinics.pay_by_date as clinic_pay_by_date', 'clinics.created_at as clinic_created_at', 'clinics.updated_at as clinic_updated_at', 'schools.id as school_id', 'schools.name as school_name', 'schools.state as school_state', 'schools.city as school_city', 'schools.country as school_country', 'schools.address as school_address', 'schools.created_at as school_created_at', 'schools.updated_at as school_updated_at', 'waivers.id as waiver_id', 'waivers.name as waiver_name', 'waivers.file as waiver_file', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
        ->first();

        $clinicDetails = clinicDetails::where('clinic_id', $id)->first();
        $clinic = Clinic::find($id);

        $enrollCheck = Enroll::where('clinic_id' , $id)->where('member_id', Auth::id())->first();
        
        // $school=School::where(['id'=>$row->school_id])->first();
        $school=School::where('id', $clinic->school_id)->first();
        // dd($school);

        if(Auth::check()) {
            $state = State::find(Auth::user()->state);

            return view('clinic.detail', compact(['row' , 'school', 'id','enrollCheck', 'state', 'clinicDetails']));
        }
        else {
            return view('clinic.detail', compact(['row' , 'school', 'id','enrollCheck', 'clinicDetails']));
        }

        return view('clinic.detail', compact(['row' , 'school', 'id','enrollCheck', 'state', 'clinicDetails']));

        // return view('clinic.detail', compact(['row','school']));
    }

    public function enroll($id)
    {
        $state = State::find(Auth::user()->state);
        $clinic = Clinic::find($id);
        return view('clinic.enroll' , compact('state' , 'clinic'));
    }

    public function updateClinic(Request $request , $id)
    {
        $age=$request->age;

        $this->validate(request(), [
                'first_name'    => 'required',
                'last_name'     => 'required',
                'address'       => 'required',
                'city'          => 'required',
                'state'         => 'required',
                'zip'           => 'required',
                'age'           => 'required',
                'phone'         => 'required',
                'email'         => 'required',

            ]);


        $clinic = Clinic::find($id);
        $cal_age= Carbon::parse($age)->age;


        $enrollCheck = Enroll::where('clinic_id' , $id)->where('member_id', Auth::id())->first();

        if (isset($enrollCheck->id)) {
             return redirect()->to(route('clinic.detail', $clinic->id))->with(['msg' => 'You are already enrolled.']);
        }

        $age  = age(Auth::user()->date_of_birth);

        if( $cal_age>=$clinic->open_age_from  & $cal_age <= $clinic->open_age_to){



        $data = [
                'clinic_id'     => $clinic->id,
                'member_id'     => Auth::id(),
                'coach_id'      => $clinic->user_id,
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'address'       => $request->address,
                'city'          => $request->city,
                'state'         => Auth::user()->state,
                'zip'           => $request->zip,
                'age'           => $request->age,
                'phone'         => $request->phone,
                'email'         => $request->email,
                'participate'   => $request->participate,
                'profileLink'   => $request->profileLink,
                'home_gym'      => $request->home_gym,
        ];

        $data= Enroll::create($data);

        // Mail::to(Auth::user())->send(new clinicEnrollment($clinic));


        return redirect('/clinic/detail/'.$clinic->id.'?enroll_id='.$data->id.'&waiver='.$clinic->assigned_waiver)->with(['msg' => 'Successfully Enroll.']);
        }

        return redirect()->to(route('clinic.detail', $clinic->id))->with(['msg' => 'you are not eligible of this enrollment.']);
    }

    public function searchCLinics(Request $request) {

        $state = $request->state;
        $school = $request->school;
        $city = $request->city;
        if($request->state == "Search By State") {
            $state = null;
        }
        if($request->school == "Search By School") {
            $school = null;
        }
        if($request->city == "Search by City") {
            $city = null;
        }
        // dd($request->date);
        
        if($request->date != null) {
            $clinics = Clinic::join('schools', 'clinics.school_id', '=', 'schools.id')
                            ->join('states', 'schools.state', '=', 'states.id')
                            ->where('clinics.cost', '=', $request->cost)
                            ->orwhere('states.name', '=', $state)
                            ->orwhere('schools.name', '=', $school)
                            ->orwhere('schools.city', '=', $city)
                            ->orwhere('clinics.date', '>=', $request->date)
                            ->select('clinics.*', 'schools.image')
                            ->get();

            return view('clinic.search', compact('clinics'));
        }
        else if ($request->date == null) {
            $clinics = Clinic::join('schools', 'clinics.school_id', '=', 'schools.id')
                            ->join('states', 'schools.state', '=', 'states.id')
                            ->where('clinics.cost', '=', $request->cost)
                            ->orwhere('states.name', '=', $state)
                            ->orwhere('schools.name', '=', $school)
                            ->orwhere('schools.city', '=', $city)
                            ->select('clinics.*', 'schools.image')
                            ->get();

            return view('clinic.search', compact('clinics'));
        }
    }
}
