<?php

namespace App\Http\Controllers\Coach;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Team;
use App\School;
use App\Waiver;
use Auth;
use App\State;
use App\Clinic;
use App\Enroll;
use App\UniformRequirement;
use DataTables;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        // $clinics = Clinic::where('user_id' , Auth::id())->get();
		
		$clinics = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
		->join('schools', 'clinics.school_id', '=', 'schools.id')
        ->where('clinics.user_id' , Auth::id())
		->where('clinics.pay_by_date', '>', today())
        ->select('clinics.*', 'schools.image', 'waivers.id as waiver_id', 'waivers.name as waiver_name', 'waivers.file as waiver_file', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
		->get();

        return view('coach.dashboard' , compact('clinics'));
    }

    public function profile()
    {
    	$teams = Team::get();
    	$schools = School::get();
    	$states = State::where('country_id' , 231)->get();
		$waivers = Waiver::all();
		$waiver = Waiver::join('users', 'waivers.id', '=', 'users.waiver_id')
			->where('users.id', Auth::id())
			->select('users.*', 'waivers.name as waiver_name', 'waivers.id as waiver_table_id')
			->first();
			// ->get();
			// return $waiver;
    	return view('coach.profile' , compact('teams', 'states' , 'schools', 'waivers', 'waiver'));
    }

    public function profileUpdate(Request $request)
    {
		// dd($request);
		if ($request->email != Auth::user()->email) {
        $rule['email'] = 'required|unique:users';
    	}
        $rule['first_name']	= 'required';
		$this->validate(request(), $rule);
		
		// $filenameprofile = Auth::user()->profile;

		global $filenameprofile;

    	 if ($request->hasFile('profile')) {
			// dd(4);
			$profile = $request->file('profile');
            $extension = $profile->getClientOriginalExtension();
			$filenameprofile  = 'uploads/profile/'. md5($profile) . time() . '.' . $extension;
			// dd($filenameprofile);
            $profile->move('./uploads/profile/', $filenameprofile);
            if (isset($row->profile)) {
                if(file_exists($row->profile)){ 
                     File::delete($row->profile);
                }
            }
		}
		// dd($filenameprofile);

		if ($request->hasFile('profile')) {
			$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'address'		=> $request->address,
    		'city'		    => $request->city,
    		'state'	    	=> $request->state,
			'school_id'		=> $request->school_id,
			'profile'			=> $filenameprofile,
    		'phone'			=> $request->phone,
    		'zip'		    => $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year,
    		'participate'		=> $request->participate,
    		'home_gym'		=> $request->home_gym,
			'waiver_id'		=> $request->waiver,
    		// 'type'			=> $request->type,
    		// 'password'	=> Hash::make($request->password)
    	];
		}
		else {
			$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'address'		=> $request->address,
    		'city'		    => $request->city,
    		'state'	    	=> $request->state,
			'school_id'		=> $request->school_id,
			// 'profile'			=> $filenameprofile,
    		'phone'			=> $request->phone,
    		'zip'		    => $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year,
    		'participate'		=> $request->participate,
    		'home_gym'		=> $request->home_gym,
			'waiver_id'		=> $request->waiver,
    		// 'type'			=> $request->type,
    		// 'password'	=> Hash::make($request->password)
    	];
		}
		

    	$userCreate = Auth::user()->update($data);

    		Auth::user()->teams()->sync($request->teams);


    	return redirect()->to(route('coach.profile'))->with('status', 'Profile Updated Successfully!');
	}
	
	public function coachSummary() {
		$totalEnrolls = Enroll::where('coach_id', Auth::id())->count();
		
		$totalCosts = Enroll::join('clinics', 'enrolls.clinic_id', 'clinics.id')
								->where('enrolls.coach_id', Auth::id())
								->where('enrolls.payment_status', 1)
								->get();

		// dd($totalCosts);
		
								$totalAmount = 0;

		foreach($totalCosts as $cost) {
			$totalAmount = $cost->cost + $totalAmount;
		}

		$coachCut = (0.98) * $totalAmount;
		$cheerHereCut = (0.02) * $totalAmount;
		
		$paidEnrollments = Clinic::join('schools', 'clinics.school_id', 'schools.id')
						->join('enrolls', 'clinics.id', 'enrolls.clinic_id')
						->where('clinics.user_id', Auth::id())
						->where('enrolls.payment_status', 1)
						// ->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("concat('$', SUM(clinics.cost)) as total_amount"), DB::raw("concat('$', round((0.98 * SUM(clinics.cost)), 2)) as coach_cut"), DB::raw("concat('$', round((0.02 * SUM(clinics.cost)), 2)) as cheer_here_cut"), 'schools.name', 'clinics.date', 'clinics.time'])
						->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("concat('$', round((0.98 * SUM(clinics.cost)), 2)) as total_earnings"), 'schools.name', 'clinics.date', 'clinics.time', 'enrolls.clinic_id'])
						->groupBy('enrolls.clinic_id')
						->get(); 
						

		$unpaidEnrollments = Clinic::join('schools', 'clinics.school_id', 'schools.id')
							->join('enrolls', 'clinics.id', 'enrolls.clinic_id')
							->where('clinics.user_id', Auth::id())
							->where('enrolls.payment_status', 0)
							// ->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("concat('$', SUM(clinics.cost)) as total_amount"), DB::raw("concat('$', round((0.98 * SUM(clinics.cost)), 2)) as coach_cut"), DB::raw("concat('$', round((0.02 * SUM(clinics.cost)), 2)) as cheer_here_cut"), 'schools.name', 'clinics.date', 'clinics.time'])
							->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("0 as total_earnings"), 'schools.name', 'clinics.date', 'clinics.time', 'enrolls.clinic_id'])
							->groupBy('enrolls.clinic_id')
							->get();
							// dd($unpaidEnrollments);
		
		return view('coach.summary', compact('totalEnrolls', 'totalAmount' ,'coachCut', 'cheerHereCut', 'paidEnrollments', 'unpaidEnrollments'));
	}

	public function paidEnrollments(Request $request) {
		if ($request->ajax()) {			
			
			$data = Clinic::join('schools', 'clinics.school_id', 'schools.id')
							->join('enrolls', 'clinics.id', 'enrolls.clinic_id')
							->where('clinics.user_id', Auth::id())
							->where('enrolls.payment_status', 1)
							// ->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("concat('$', SUM(clinics.cost)) as total_amount"), DB::raw("concat('$', round((0.98 * SUM(clinics.cost)), 2)) as coach_cut"), DB::raw("concat('$', round((0.02 * SUM(clinics.cost)), 2)) as cheer_here_cut"), 'schools.name', 'clinics.date', 'clinics.time'])
							->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("concat('$', round((0.98 * SUM(clinics.cost)), 2)) as total_earnings"), 'schools.name', 'clinics.date', 'clinics.time'])
							->groupBy('enrolls.clinic_id')
							->get();
			
			return Datatables::of($data)
                // ->addIndexColumn()
                // ->addColumn('action', function($row){
                //     $btn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                //     return $btn;
                // })
                // ->rawColumns(['action'])
                ->make(true);
		}
		else {
			return "Couldn't find the data";
		}
	}

	public function unPaidEnrollments(Request $request) {
		if ($request->ajax()) {			
			
			$data = Clinic::join('schools', 'clinics.school_id', 'schools.id')
							->join('enrolls', 'clinics.id', 'enrolls.clinic_id')
							->where('clinics.user_id', Auth::id())
							->where('enrolls.payment_status', 0)
							// ->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("concat('$', SUM(clinics.cost)) as total_amount"), DB::raw("concat('$', round((0.98 * SUM(clinics.cost)), 2)) as coach_cut"), DB::raw("concat('$', round((0.02 * SUM(clinics.cost)), 2)) as cheer_here_cut"), 'schools.name', 'clinics.date', 'clinics.time'])
							->select([DB::raw("COUNT(enrolls.clinic_id) as total_enrolls"), DB::raw("0 as total_earnings"), 'schools.name', 'clinics.date', 'clinics.time'])
							->groupBy('enrolls.clinic_id')
							->get();
			
			return Datatables::of($data)
                // ->addIndexColumn()
                // ->addColumn('action', function($row){
                //     $btn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                //     return $btn;
                // })
                // ->rawColumns(['action'])
                ->make(true);
		}
		else {
			return "Couldn't find the data";
		}
	}

	public function getSchool() {
		$test = 'testing';

		return response()->json(['test' => $test]);
	}

	public function setUniformRequirements(Request $request) {
		
		// return $request;
		
		$oldRecords = UniformRequirement::where('coach_id', Auth::id())->get();

		// dd($oldRecords);
		// if($oldRecords != null) {
		// 	echo 1;
		// }
		// else
		// 	echo 0;
		
		foreach($oldRecords as $oldRecord) {
			$oldRecord->delete();
		}
		
		$minor = new UniformRequirement;
		$minor->coach_id = Auth::id();
		$minor->uniform_type = 'Minor';
		$minor->sports_bra = $request->minor1;
		$minor->shorts = $request->minor2;
		$minor->t_shirt = $request->minor3;
		$minor->bow = $request->minor4;
		$minor->save();

		$freshman = new UniformRequirement;
		$freshman->coach_id = Auth::id();
		$freshman->uniform_type = 'Freshman';
		$freshman->sports_bra = $request->freshman1;
		$freshman->shorts = $request->freshman2;
		$freshman->t_shirt = $request->freshman3;
		$freshman->bow = $request->freshman4;
		$freshman->save();

		$sophomore = new UniformRequirement;
		$sophomore->coach_id = Auth::id();
		$sophomore->uniform_type = 'Sophomore';
		$sophomore->sports_bra = $request->sophomore1;
		$sophomore->shorts = $request->sophomore2;
		$sophomore->t_shirt = $request->sophomore3;
		$sophomore->bow = $request->sophomore4;
		$sophomore->save();

		$junior = new UniformRequirement;
		$junior->coach_id = Auth::id();
		$junior->uniform_type = 'Junior';
		$junior->sports_bra = $request->junior1;
		$junior->shorts = $request->junior2;
		$junior->t_shirt = $request->junior3;
		$junior->bow = $request->junior4;
		$junior->save();

		$senior = new UniformRequirement;
		$senior->coach_id = Auth::id();
		$senior->uniform_type = 'Senior';
		$senior->sports_bra = $request->senior1;
		$senior->shorts = $request->senior2;
		$senior->t_shirt = $request->senior3;
		$senior->bow = $request->senior4;
		$senior->save();

		return back()->with('status', 'Profile Updated Successfully!');
	}

	
}