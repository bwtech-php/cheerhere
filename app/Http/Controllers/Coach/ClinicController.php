<?php

namespace App\Http\Controllers\Coach;

use Auth;
use App\Clinic;
use App\clinicDetails;
use App\Enroll;
use App\School;
use App\Waiver;
use App\State;
use App\clinicLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClinicController extends Controller
{
    public function index()
    {
		// dd(Auth::user());
		$schools = School::get();
		$waiver=Waiver::all();
		$states = State::where('country_id' , 231)->get();
		
		$clinics = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
		->join('schools', 'clinics.school_id', '=', 'schools.id')
        ->where('clinics.user_id' , Auth::id())
        ->select('schools.name', 'clinics.*', 'waivers.id as waiver_id', 'waivers.name as waiver_name', 'waivers.file as waiver_file', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
		->get();

		$clinicLocations = clinicLocation::where('coach_id', Auth::id())->with('state')->get();
		// return $clinicLocations;
		// dd($clinicLocations);

		// dd($clinics);

    	return view('coach.clinic' , compact(['schools','waiver', 'clinics', 'states', 'clinicLocations']));
    }

    public function store (Request $request)
    { 
		// return $request;
		
		global $sportsbra;
		global $shorts;
		global $tshirt;

		if ($request->has('sportsbra')) {
			$sportsbra = $request->sportsbravalue;
		}
		else {
			$sportsbra = null;
		}
		if ($request->has('shorts')) {
			$shorts = $request->shortsvalue;
		}
		else {
			$shorts = null;
		}
		if ($request->has('tshirt')) {
			$tshirt = $request->tshirtvalue;
		}
		else {
			$tshirt = null;
		}

		$rule['school_id']	= 'required';
        $rule['open_age_to']	= 'required';
        $rule['open_age_from']	= 'required';
    	$rule['assigned_waiver']	= 'required';
    	$rule['date']	= 'required';
    	$rule['time']	= 'required';
    	$rule['in_time']	= 'required';
    	$rule['cost']	= 'required';
    	// $rule['what_to_wear']	= 'required';
    	$rule['pay_by_date']	= 'required';
		$rule['clinic_location_id']	= 'required';

    	$this->validate(request(), $rule);
		// return;

    	$clinic = Clinic::create([

    		'user_id'	=> Auth::id(),
    		'school_id'	=> $request->school_id,
            'open_age_to'	=> $request->open_age_to,
            'open_age_from'=>$request->open_age_from,
    		'assigned_waiver'	=> $request->assigned_waiver,
    		'date'	=> $request->date,
    		'time'	=> $request->time,
    		'in_time'	=> $request->in_time,
    		'cost'	=> $request->cost,
    		// 'what_to_wear'	=> $request->what_to_wear,
    		'pay_by_date'	=> $request->pay_by_date,
			'clinic_location_id' => $request->clinic_location_id
		]);
		
		clinicDetails::create([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			// 'freshman' => $request->freshman,
			'uniform_type' => 'Minor',
			'sports_bra' => $request->minor1,
			'shorts' => $request->minor2,
			't_shirt' => $request->minor3,
			'bow' => $request->minor4,
			// 'additional_instructions' => $request->what_to_wear_add
		]);

		clinicDetails::create([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			// 'freshman' => $request->freshman,
			'uniform_type' => 'Freshman',
			'sports_bra' => $request->freshman1,
			'shorts' => $request->freshman2,
			't_shirt' => $request->freshman3,
			'bow' => $request->freshman4,
			// 'additional_instructions' => $request->what_to_wear_add
		]);

		clinicDetails::create([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			// 'freshman' => $request->freshman,
			'uniform_type' => 'Sophomore',
			'sports_bra' => $request->sophomore1,
			'shorts' => $request->sophomore2,
			't_shirt' => $request->sophomore3,
			'bow' => $request->sophomore4,
			// 'additional_instructions' => $request->what_to_wear_add
		]);
		
		clinicDetails::create([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			// 'freshman' => $request->freshman,
			'uniform_type' => 'Junior',
			'sports_bra' => $request->junior1,
			'shorts' => $request->junior2,
			't_shirt' => $request->junior3,
			'bow' => $request->junior4,
			// 'additional_instructions' => $request->what_to_wear_add
		]);

		clinicDetails::create([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			// 'freshman' => $request->freshman,
			'uniform_type' => 'Senior',
			'sports_bra' => $request->senior1,
			'shorts' => $request->senior2,
			't_shirt' => $request->senior3,
			'bow' => $request->senior4,
			// 'additional_instructions' => $request->what_to_wear_add
		]);

    	return redirect()->to(route('coach.clinic'))->with('status', 'Clinic Created Successfully!');
    }

    public function show($id)
    {
		$row = Clinic::find($id);
		// return $row;
    	if(!isset($row->id)){
    	    return redirect()->to(route('coach.clinic'));
		}
		
		$clinicDetails = clinicDetails::where('clinic_id', $id)->get();

        $waiver = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
        ->where('clinics.id' , $id)
        ->select('clinics.*', 'waivers.id as waiver_id', 'waivers.*', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
        ->first();

    	return view('coach.clinic_show' , compact('row', 'waiver', 'clinicDetails'));
    }

    public function edit($id)
    {
    	// return $id;
		$schools = School::get();
		$clinic = Clinic::find($id);
		$row = Clinic::find($id);
		$waiver=Waiver::all();
		$clinics = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
		->join('schools', 'clinics.school_id', '=', 'schools.id')
        ->where('clinics.user_id' , Auth::id())
        ->select('schools.name', 'clinics.*', 'waivers.id as waiver_id', 'waivers.name as waiver_name', 'waivers.file as waiver_file', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
		->get();
		$states = State::where('country_id' , 231)->get();
		$clinicLocations = clinicLocation::where('coach_id', Auth::id())->get();
    	return view('coach.clinic', compact('schools' , 'row', 'waiver', 'clinic', 'clinics', 'states', 'clinicLocations'));
    }

    public function update(Request $request , $id)
    {
		global $sportsbra;
		global $shorts;
		global $tshirt;

		if ($request->has('sportsbra')) {
			$sportsbra = $request->sportsbravalue;
		}
		else {
			$sportsbra = null;
		}
		if ($request->has('shorts')) {
			$shorts = $request->shortsvalue;
		}
		else {
			$shorts = null;
		}
		if ($request->has('tshirt')) {
			$tshirt = $request->tshirtvalue;
		}
		else {
			$tshirt = null;
		}
		
		$rule['school_id']	= 'required';
    	$rule['open_age_to']	= 'required';
        $rule['open_age_from']	= 'required';
    	$rule['assigned_waiver']	= 'required';
    	$rule['date']	= 'required';
    	$rule['time']	= 'required';
    	$rule['in_time']	= 'required';
    	$rule['cost']	= 'required';
    	// $rule['what_to_wear']	= 'required';
    	$rule['pay_by_date']	= 'required';
		$rule['clinic_location_id']	= 'required';

    	$this->validate(request(), $rule);


    	Clinic::where('id' , $id)->update([
    		'user_id'	=> Auth::id(),
    		'school_id'	=> $request->school_id,
    		'open_age_to'	=> $request->open_age_to,
            'open_age_from'=>$request->open_age_from,
    		'assigned_waiver'	=> $request->assigned_waiver,
    		'date'	=> $request->date,
    		'time'	=> $request->time,
    		'in_time'	=> $request->in_time,
    		'cost'	=> $request->cost,
    		// 'what_to_wear'	=> $request->what_to_wear,
    		'pay_by_date'	=> $request->pay_by_date,
			'clinic_location_id' => $request->clinic_location_id
		]);
		
		$clinic = Clinic::find($id);
		
		// clinicDetails::updateOrCreate([
		// 	'clinic_id' => $clinic->id,
		// 	'freshman' => $request->freshman,
		// 	'sports_bra' => $sportsbra,
		// 	'shorts' => $shorts,
		// 	't_shirt' => $tshirt,
		// 	'additional_instructions' => $request->what_to_wear_add
		// ]);

		clinicDetails::updateOrCreate([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			'uniform_type' => 'Minor',
			'sports_bra' => $request->minor1,
			'shorts' => $request->minor2,
			't_shirt' => $request->minor3,
			'bow' => $request->minor4,
		]);

		clinicDetails::updateOrCreate([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			'uniform_type' => 'Freshman',
			'sports_bra' => $request->freshman1,
			'shorts' => $request->freshman2,
			't_shirt' => $request->freshman3,
			'bow' => $request->freshman4,
		]);

		clinicDetails::updateOrCreate([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			'uniform_type' => 'Sophomore',
			'sports_bra' => $request->sophomore1,
			'shorts' => $request->sophomore2,
			't_shirt' => $request->sophomore3,
			'bow' => $request->sophomore4,
		]);

		clinicDetails::updateOrCreate([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			'uniform_type' => 'Junior',
			'sports_bra' => $request->junior1,
			'shorts' => $request->junior2,
			't_shirt' => $request->junior3,
			'bow' => $request->junior4,
		]);

		clinicDetails::updateOrCreate([
			'clinic_id' => $clinic->id,
			'coach_id' => Auth::id(),
			'uniform_type' => 'Senior',
			'sports_bra' => $request->senior1,
			'shorts' => $request->senior2,
			't_shirt' => $request->senior3,
			'bow' => $request->senior4,
		]);

    	return redirect()->to(route('coach.clinic'))->with('status', 'Clinic Updated Successfully!');
    }

    public function delete($id){
        Clinic::where('id' , $id)->delete();
        return redirect()->to(route('coach.dashboard'));
    }

    public function clinicEnroll($id)
    {
		
		// $enrolls = Enroll::join('users', 'enrolls.member_id', '=', 'users.id')
		// ->join('states', 'enrolls.state', '=', 'states.id')
		// ->where('coach_id' , Auth::id())
		// ->get();

		$enrolls = Enroll::join('users', 'enrolls.member_id', '=', 'users.id')
		->join('states', 'enrolls.state', '=', 'states.id')
		->join('clinics', 'enrolls.clinic_id', '=', 'clinics.id')
		->where('clinics.id', $id)
		->where('enrolls.coach_id' , Auth::id())
		->select('enrolls.*', 'states.name as state_name')
		->get();
        
		return view('coach.enroll' , compact('enrolls'));
	}
	
	public function updateClinic($id) {
		$clinic = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
		->where('clinics.id' , $id)
		->select('clinics.*', 'waivers.name as waiver_name', 'waivers.id as waiver_id')
		->first();

		$schools = School::get();
		$waiver=Waiver::all();
		
		$clinics = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
		->join('schools', 'clinics.school_id', '=', 'schools.id')
        ->where('clinics.user_id' , Auth::id())
        ->select('schools.name', 'clinics.*', 'waivers.id as waiver_id', 'waivers.name as waiver_name', 'waivers.file as waiver_file', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
		->get();

		$states = State::where('country_id' , 231)->get();

		$clinicLocations = clinicLocation::where('coach_id', Auth::id())->get();

		// return $clinic;

		// return redirect()->back()->with(compact('clinic'));
		return view('coach.clinic' , compact(['schools','waiver', 'clinics', 'clinic', 'states', 'clinicLocations']));
	}

	public function getAllClinics() {
		$clinics = Clinic::join('waivers', 'clinics.assigned_waiver', '=', 'waivers.id')
		->join('schools', 'clinics.school_id', '=', 'schools.id')
        ->where('clinics.user_id' , Auth::id())
        ->select('clinics.*', 'schools.image', 'waivers.id as waiver_id', 'waivers.name as waiver_name', 'waivers.file as waiver_file', 'waivers.created_at as waiver_created_at', 'waivers.updated_at as waiver_updated_at')
		->get();

        return view('coach.allclinics' , compact('clinics'));
	}
}