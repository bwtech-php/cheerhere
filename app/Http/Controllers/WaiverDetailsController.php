<?php

namespace App\Http\Controllers;

use App\Waiver_details;
use Illuminate\Http\Request;

class WaiverDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Waiver_details  $waiver_details
     * @return \Illuminate\Http\Response
     */
    public function show(Waiver_details $waiver_details)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Waiver_details  $waiver_details
     * @return \Illuminate\Http\Response
     */
    public function edit(Waiver_details $waiver_details)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Waiver_details  $waiver_details
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Waiver_details $waiver_details)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Waiver_details  $waiver_details
     * @return \Illuminate\Http\Response
     */
    public function destroy(Waiver_details $waiver_details)
    {
        //
    }
}
