<?php

namespace App\Http\Controllers;

use App\Waiver_A;
use Illuminate\Http\Request;

class WaiverAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Waiver_A  $waiver_A
     * @return \Illuminate\Http\Response
     */
    public function show(Waiver_A $waiver_A)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Waiver_A  $waiver_A
     * @return \Illuminate\Http\Response
     */
    public function edit(Waiver_A $waiver_A)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Waiver_A  $waiver_A
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Waiver_A $waiver_A)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Waiver_A  $waiver_A
     * @return \Illuminate\Http\Response
     */
    public function destroy(Waiver_A $waiver_A)
    {
        //
    }
}
