<?php

namespace App\Http\Controllers;

//Mail
use App\Mail\newTicket;
use App\Mail\ticketClose;
use App\Mail\mailTesting;
use App\Mail\deleteTicket;
use Illuminate\Support\Facades\Mail;

use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->type == 'Member') {
            $tickets = Ticket::leftjoin('users', 'tickets.user_id', 'users.id')
                                ->leftjoin('users as recipients', 'tickets.recipient_id', 'recipients.id')
                            ->where('tickets.user_id', Auth::user()->id)
                            ->select('tickets.*', 'users.first_name', 'users.last_name', 'recipients.first_name as recipient_first_name', 'recipients.last_name as recipient_last_name')
                            ->get();
        }
        elseif(Auth::user()->type == 'Admin' or Auth::user()->type == 'Super Admin') {
            $tickets = Ticket::leftjoin('users', 'tickets.user_id', 'users.id')
                                ->leftjoin('users as recipients', 'tickets.recipient_id', 'recipients.id')
                            // ->where('tickets.user_id', Auth::user()->id)
                            ->select('tickets.*', 'users.first_name', 'users.last_name', 'recipients.first_name as recipient_first_name', 'recipients.last_name as recipient_last_name')
                            ->get();
        }
        elseif(Auth::user()->type == 'Coach') {
            $tickets = Ticket::leftjoin('users', 'tickets.user_id', 'users.id')
                                ->leftjoin('users as recipients', 'tickets.recipient_id', 'recipients.id')
                            ->where('tickets.user_id', Auth::user()->id)
                            ->orwhere('tickets.recipient_id', Auth::user()->id)
                            ->select('tickets.*', 'users.first_name', 'users.last_name', 'recipients.first_name as recipient_first_name', 'recipients.last_name as recipient_last_name')
                            ->get();
        }
        
        // dd($tickets);
        return view('ticket', compact('tickets'));
    }

    public function newTicketForm() {
        $coaches = User::where('type', 'Coach')->orwhere('type', 'Admin')->orwhere('type', 'Super Admin')->get();
        
        return view('newticket', compact('coaches'));
    }

    public function createTicket(Request $request) {
        // return $request;
        
        $ticket = new Ticket;
        $ticket->user_id = Auth::user()->id;
        $ticket->recipient_id = $request->recipient_id;
        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->category = $request->category;
        // $ticket->hidden = $request->hidden;
        // dd($ticket);
        $ticket->save();
        // dd($ticket);

        Mail::to(Auth::user())->send(new newTicket($ticket));
        
        return redirect()->route('tickets')->with('status', 'Ticket with id: ' . $ticket->id . ' created successfully!');
    }

    public function closeTicket($id) {
        // return $id;
        $ticket = Ticket::find($id);
        $user = User::find($ticket->user_id);
        Mail::to($user)->send(new ticketClose($ticket));
        // return "Mail sent!";
        $ticket->status = 0;
        $ticket->save();

        return redirect()->back()->with('status', 'Ticket closed successfully!');
    }

    public function ticketDetail($id) {
        $ticket = Ticket::where('id', $id)->firstOrFail();
        
        return view('ticketdetail', compact('ticket'));
    }

    public function ticketDelete($id) {
        // return $id;
        $ticket = Ticket::findorfail($id);
        $ticket->delete();

        $user = User::find($ticket->user_id);
        Mail::to($user)->send(new deleteTicket($ticket));

        // return $ticket;

        return redirect()->back()->with('status', 'Ticket deleted successfully!');
    }
}