<?php

namespace App\Http\Controllers;

use App\clinicLocation;
use Illuminate\Http\Request;
use App\Team;
use App\School;
use App\State;
use App\Waiver;

class ClinicLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;

		$this->validate(request(), [
			'name' => 'required',
			'address' => 'required',
			'state' => 'required',
			'city' => 'required',
			'zip' => 'required',
            'school_id' => 'required',
		]);

        $clinicLocation = new clinicLocation;
        $clinicLocation->coach_id = \Auth::id();
        $clinicLocation->school_id = $request->school_id;
        $clinicLocation->name = $request->name;
        $clinicLocation->address = $request->address;
        $clinicLocation->state_id = $request->state;
        $clinicLocation->city = $request->city;
        $clinicLocation->zip = $request->zip;
        $clinicLocation->save();

		// $id = $school->id;
		// $school = School::join('states', 'schools.state', '=', 'states.id')->where('schools.state', $request->state)->select('schools.*', 'states.name as state_name')->first();

		$teams = Team::get();
		$schools = School::get();
		$states = State::where('country_id' , 231)->get();
        $waivers = Waiver::all();
		
        return view('coach.profile', compact('teams', 'schools', 'states', 'waivers'))->with('status', 'Clinic Location Added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\clinicLocation  $clinicLocation
     * @return \Illuminate\Http\Response
     */
    public function show(clinicLocation $clinicLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\clinicLocation  $clinicLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(clinicLocation $clinicLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\clinicLocation  $clinicLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, clinicLocation $clinicLocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\clinicLocation  $clinicLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(clinicLocation $clinicLocation)
    {
        //
    }
}
