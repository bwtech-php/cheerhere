<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Ticket;
use Illuminate\Http\Request;
use Auth;

use Illuminate\Support\Facades\Mail;
use App\Mail\NewComment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createComment(Request $request)
    {
        if($request->submit == 'open') {
            $comment = Comment::create([
            'ticket_id' => $request->ticket_id,
            'user_id' => Auth::user()->id,
            'comment' => $request->comment
            ]);

            $ticket = Ticket::find($request->ticket_id);
            
            Mail::to(Auth::user())->send(new NewComment($ticket));

            return redirect()->back()->with("status", "Your comment has been added.");
        }
        elseif($request->submit == 'close') {
            $comment = Comment::create([
            'ticket_id' => $request->ticket_id,
            'user_id' => Auth::user()->id,
            'comment' => $request->comment
            ]);

            $ticket = Ticket::find($request->ticket_id);
            $ticket->status = 0;
            $ticket->save();

            Mail::to(Auth::user())->send(new NewComment($ticket));

            return redirect()->back()->with("status", "Your comment has been added and the ticket has been closed.");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
