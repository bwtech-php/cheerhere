<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clinic;
use App\School;
use App\State;

class HomeController extends Controller
{

    public function index()
    {
        $search = School::select('city', 'state', 'name')->get();
        $clinics = Clinic::whereDate('date' ,'>=' , date('Y-m-d'))->limit(8)->latest()->get();
        $states = State::where('country_id', 231)->get();
        return view('index' , compact('clinics', 'search', 'states'));
    }
}
