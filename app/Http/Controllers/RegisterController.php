<?php

namespace App\Http\Controllers;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\newUserRegistration;

use Illuminate\Http\Request;
use App\State;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\School;
use App\Team;
use File;

class RegisterController extends Controller
{
    public function memberRegisterForm()
    {
    	$schools = School::get();
    	$states = State::where('country_id' , 231)->get();
    	return view('member.register', compact('states' , 'schools'));
    }

    public function memberStore(Request $request)
    {
    	$this->validate(request(), [
                'email'         => 'required|unique:users',
                'first_name'	=> 'required',
                'password'		=> 'required',
			]);	
        $filenameprofile = null;

         if ($request->hasFile('profile')) {
            $profile = $request->file('profile');
            $extension = $profile->getClientOriginalExtension();
            $filenameprofile  = 'uploads/profile/'. md5($profile) . time() . '.' . $extension;
            $profile->move('./uploads/profile/', $filenameprofile);
           
        }



    	$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'address'		=> $request->address,
    		'city'		    => $request->city,
    		'state'	    	=> $request->state,
    		'school_id'		=> $request->school_id,
    		'phone'		=> $request->phone,
    		'zip'		    => $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year, 
            'participate'		=> $request->participate,
            'youtubelinks' => $request->youtubelinks,
    		'home_gym'		=> $request->home_gym,
    		'type'			=> $request->type,
    		'password'	=> Hash::make($request->password),
            'profile'   => $filenameprofile,
    	];

        $userCreate = User::create($data);
        // return $userCreate;
    	if ($request->type == 'Member') {
    		$userCreate->schools()->sync($request->schools);
    	}
    	if ($request->type == 'Coach') {
    		$userCreate->teams()->sync($request->teams);
        }
        
        // Mail::to($userCreate)->send(new newUserRegistration());

    	return redirect()->to(route('login'));
    }

    public function coachRegisterForm()
    {
    	$schools = School::get();
    	$states = State::where('country_id' , 231)->get();
    	$teams	= Team::get();
    	return view('member.coachRegister', compact('states', 'schools' , 'teams'));
    }

    public function school(Request $request)
    {
        // dd($request->file('image'));
        $this->validate(request(), [
                'name' => 'required',
                'city' => 'required',
                'state' => 'required',
            ]);

        global $imageName;
        
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $imageName = $request->file('image')->getClientOriginalName();
                $path = $request->image->move(public_path('img/schools'), $imageName);
            }
        }

        $school = School::create([
                'name'         => $request->name,
                'city'          => $request->city,
                'state'         => $request->state,
                'country'       => 'US',
                'address'       => $request->address,
                'image'       => $imageName
            ]);

        $id = $school->id;
        $school = School::join('states', 'schools.state', '=', 'states.id')->where('schools.state', $request->state)->select('schools.*', 'states.name as state_name')->first();

        // dd($school);

        $teams = Team::get();
    	$schools = School::get();
    	$states = State::where('country_id' , 231)->get();

        // return back()->with('status', 'School Added successfully!');
        return view('coach.profile', compact('school', 'teams', 'schools', 'states'))->with('status', 'School Added successfully!');
        // return response()->json(['id' => $school->id , 'name' => $school->name]);
    }
}
