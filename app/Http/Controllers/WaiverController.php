<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\Enroll;
use App\Waiver;
use App\Waiver_A;
use App\WaiverB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WaiverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function waiverB(){
         return view('waivers.waiverB');
     }
     public function WaiverA_submit(Request $request){
        
        // dd($request);
        
        if (!$request->has('esign')) {
            return redirect()->back()->with('error_msg', 'Error! Please tick the checkbox and confirm the signatures');
        }

        // global $sign_1;
        // if ($request->hasFile('sign_1')) {

        //     $sign_1 = time().'.'.$request->sign_1->extension();

        //     $request->sign_1->move(public_path('img/waiver'), $sign_1);

        // }
        // global $p_sign_1;
        // if ($request->hasFile('p_sign_1')) {

        //     $p_sign_1 = time().'.'.$request->p_sign_1->extension();

        //     $request->p_sign_1->move(public_path('img/waiver'), $p_sign_1);

        // }
        // global $sign_2;
        // if ($request->hasFile('sign_2')) {

        //     $sign_2 = time().'.'.$request->sign_2->extension();

        //     $request->sign_2->move(public_path('img/waiver'), $sign_2);

        // }
        // global $p_sign_2;
        // if ($request->hasFile('p_sign_2')) {

        //     $p_sign_2 = time().'.'.$request->p_sign_2->extension();

        //     $request->p_sign_2->move(public_path('img/waiver'), $p_sign_2);

        // }
        // global $sign_3;
        // if ($request->hasFile('sign_3')) {

        //     $sign_3 = time().'.'.$request->sign_3->extension();

        //     $request->sign_3->move(public_path('img/waiver'), $sign_3);

        // }
        // global $p_sign_3;
        // if ($request->hasFile('p_sign_3')) {

        //     $p_sign_3 = time().'.'.$request->p_sign_3->extension();

        //     $request->p_sign_3->move(public_path('img/waiver'), $p_sign_3);

        // }

        $clinic=Clinic::where('id',$request->clinic_id)->first();
        Waiver_A::create([
            'coach_id'=>$clinic->user_id,
            'name'=>$request->name,
            'sport'=>$request->sport,
            'dob'=>$request->dob,
            'clinic_id'=>$request->clinic_id,
            'member_id'=>Auth::user()->id,
            'enrolls_id'=>$request->enroll_id,
            'date_1'=>now(),
            'date_2'=>now(),
            'date_3'=>now(),
            'sign_1'=>$request->sign_1,
            'sign_2'=>$request->sign_2,
            'sign_3'=>$request->sign_3,
            'p_sign_1'=>$request->p_sign_1,
            'p_sign_2'=>$request->p_sign_2,
            'p_sign_3'=>$request->p_sign_3

            ]);

            $waiverStatus = Enroll::where('id', $request->enroll_id)->first();
            $waiverStatus->waiver_status = 1;
            $waiverStatus->save();

            return redirect('/clinic/detail/'.$request->clinic_id)->with('waiver_submit','message');
    }
     public function waiverB_send(Request $request){

        // return $request;

        if (!$request->has('esign')) {
            return redirect()->back()->with('error_msg', 'Error! Please tick the checkbox and confirm the signatures');
        }
        
        $clinic=Clinic::where('id',$request->clinic_id)->first();

        // global $sign_1;
        // if ($request->hasFile('sign_1')) {

        //     $sign_1 = time().'.'.$request->sign_1->extension();

        //     $request->sign_1->move(public_path('img/waiver'), $sign_1);

        // }
        // global $sign_2;
        // if ($request->hasFile('sign_2')) {

        //     $sign_2 = time().'.'.$request->sign_2->extension();

        //     $request->sign_2->move(public_path('img/waiver'), $sign_2);

        // }

        WaiverB::create([
        
            'team_name_1'=>$request->team_name_1,
            'name'=>$request->name,
            'minor_name_1'=>$request->minor_name_1,
            'address_1'=>$request->address_1,
            'clinic_id'=>$request->clinic_id,
            'member_id'=>Auth::user()->id,
            'enrolls_id'=>$request->enroll_id,
            'city'=>$request->city,
            'state'=>Auth::user()->state,
            'zip'=>$request->zip,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'sign_1'=>$request->sign_1,
            'sign_2'=>$request->sign_2,
            'team_name_2'=>$request->team_name_2,
            'minor_name_2'=>$request->minor_name_2,
            'witness_1'=>$request->witness_1,
            'witness_2'=>$request->witness_2,
            'm_taking'=>$request->m_taking,
            'm_allergic'=>$request->m_allergic,
            'team_name_3'=>$request->team_name_3,
            'minor_name_3'=>$request->minor_name_3,
            'address_2'=>$request->address_2,
            'telephone'=>$request->telephone,
            'home'=>$request->home,
            'work'=>$request->work
        ]);

        $waiverStatus = Enroll::where('id', $request->enroll_id)->first();
        $waiverStatus->waiver_status = 1;
        $waiverStatus->save();

        return redirect('/clinic/detail/'.$request->clinic_id)->with('waiver_submit','message');

     }

     public function waiver_view(Request $request){
        // return $request->id;
        $enroll_data=Enroll::where('id',$request->id)->first();
        //  return($enroll_data);
        $clinic_data=Clinic::where('id',$enroll_data->clinic_id)->first();
        // return $clinic_data;
        // dd($clinic_data);
        // $test = Waiver_A::all();
        // dd($test);
        if($clinic_data->assigned_waiver=="2")
        {
            $waiver_data=WaiverB::where([
                'clinic_id'=>$enroll_data->clinic_id,
                'member_id'=>$enroll_data->member_id,
                // 'coach_id'=>$enroll_data->coach_id,
            ])->first();
            // dd($waiver_data);
            return view('waivers.waiverB',compact('waiver_data'));
        }


        if($clinic_data->assigned_waiver=="1")
        {
            $waiver_data=Waiver_A::where([
                'clinic_id'=>$enroll_data->clinic_id,
                'member_id'=>$enroll_data->member_id,
                'enrolls_id'=>$request->id,
            ])->first();
            // dd($waiver_data);
            return view('waivers.waiverA',compact('waiver_data'));
        }

     }

     public function waiverA(Request $request){

        return view('waivers.waiverA');
     }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Waiver  $waiver
     * @return \Illuminate\Http\Response
     */
    public function show(Waiver $waiver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Waiver  $waiver
     * @return \Illuminate\Http\Response
     */
    public function edit(Waiver $waiver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Waiver  $waiver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Waiver $waiver)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Waiver  $waiver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Waiver $waiver)
    {
        //
    }
}
