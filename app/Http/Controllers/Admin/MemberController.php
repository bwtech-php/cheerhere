<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\School;
use App\Team;
use Hash;
use App\State;

class MemberController extends Controller
{
    public function index()
    {
    	$members = User::where('type' , 'Member')->get();
    	return view('admin.member.index' , compact('members'));
    }

    public function create()
    {
    	$schools = School::get();
    	$teams = Team::get();
    	$states = State::where('country_id' ,231)->get();
    	return view('admin.member.update' , compact('schools', 'states' , 'teams'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
                'email' => 'required|unique:users',
                'first_name'	=> 'required',
                'password'		=> 'required',
			]);	

    	$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'address'		=> $request->address,
    		'city'		    => $request->city,
    		'state'	    	=> $request->state,
    		'school_id'		=> $request->school_id,
    		'phone'			=> $request->phone,
    		'zip'		    => $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year, 
    		'participate'		=> $request->participate,
    		'home_gym'		=> $request->home_gym,
    		'type'			=> 'Member',
    		'password'	=> Hash::make($request->password) 
    	];

    		$userCreate = User::create($data);

    		$userCreate->schools()->sync($request->schools);

    		return redirect()->to(route('member.index'));

    	}

    	public function edit($id)
    	{
    		$schools = School::get();
    		$teams = Team::get();
    		$row = User::find($id);
    		$states = State::where('country_id' ,231)->get();
    		return view('admin.member.update' , compact('schools' , 'teams' , 'row' , 'states'));
    	}

    	public function update(Request $request , $id)
    	{
    		$row = User::find($id);
                $rule['first_name']	='required';
    			if ($request->email != $row->email) {
    			$rule['email'] ='required|unique:users';
    			}
    		$this->validate(request(), $rule);	

    	$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'address'		=> $request->address,
    		'city'		    => $request->city,
    		'state'	    	=> $request->state,
    		'school_id'		=> $request->school_id,
    		'phone'			=> $request->phone,
    		'zip'		    => $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year, 
    		'participate'		=> $request->participate,
    		'home_gym'		=> $request->home_gym,
    		'type'			=> 'Member',
    		'email'		=> $request->email,
    		'password'	=> $request->password ?  Hash::make($request->password) :  $row->password ,
    	];

    	$row->update($data);

    		$row->schools()->sync($request->schools);

    		return redirect()->to(route('member.index'));
		}
		
		public function deleteMember($id) {
			$member = User::find($id);
			$member->delete();

			return redirect()->back()->with('status', 'Member Deleted Successfully!');
		}
}
