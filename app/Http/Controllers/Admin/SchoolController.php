<?php

namespace App\Http\Controllers\Admin;

use App\School;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::join('states', 'schools.state', '=', 'states.id')
                            ->select('schools.*', 'states.name as state_name')
                            ->get();
        return view('admin.school.index' , compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::where('country_id', 231)->get();
        return view('admin.school.update' , compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
                'name' => 'required',
                'city' => 'required',
                'state' => 'required',
            ]);

        $school = School::create([
                'name'         => $request->name,
                'city'          => $request->city,
                'state'         => $request->state,
                'country'       => 'US',
                'address'       => $request->address
            ]);

        return redirect()->to(route('admin.school.index'))->with(['msg' => 'School Successfully create.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        $row = $school;
        $selectedState = State::findorfail($row->state);
        // dd($row->state);
        // dd($selectedState);
        $states = State::where('country_id', 231)->get();
        return view('admin.school.update' , compact('row', 'states', 'selectedState'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school)
    {
        $this->validate(request(), [
                'name' => 'required',
                'city' => 'required',
                'state' => 'required',
            ]);

        $school->update([
                'name'         => $request->name,
                'city'          => $request->city,
                'state'         => $request->state,
                'country'       => 'US',
                'address'       => $request->address
            ]);

        return redirect()->to(route('admin.school.index'))->with(['msg' => 'School Successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        $school->delete();
        return response()->json(['status' => TRUE]);
    }
}
