<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;

class AdminController extends Controller
{
    public function show() {
        $admins = User::where('type', 'Admin')->orWhere('type', 'Super Admin')->get();
        return view('admin.admin.admin', compact('admins'));
    }

    public function newAdmin() {
        return view('admin.admin.adminform');
    }
    
    public function form($id) {
        $user = User::find($id);
        // dd($user);
        return view('admin.admin.adminform', compact('user'));
    }

    public function store(Request $request) {
        $this->validate(request(), [
                'email' => 'required|unique:users',
                'first_name'	=> 'required',
                'password'		=> 'required',
			]);	

    	$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'type'			=> 'Admin',
    		'password'	=> Hash::make($request->password) 
    	];

    		$adminCreate = User::create($data);

    		return redirect()->to(route('admins'))->with('status', 'Admin created successfully!');
    }

    public function update(Request $request, $id) {
        $user = User::find($id);
                $rule['first_name']	='required';
    			if ($request->email != $user->email) {
    			$rule['email'] ='required|unique:users';
    			}
    		$this->validate(request(), $rule);	

    	$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'password'	=> $request->password ?  Hash::make($request->password) :  $user->password ,
    	];

    	$user->update($data);

    	return redirect()->to(route('admins'))->with('status', 'Admin updated successfully!');
    }

    public function destroy($id) {
        $user = User::find($id);

        $user->delete();

        return redirect()->to(route('admins'))->with('status', 'Admin deleted successfully!'); 
    }
}
