<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Skill;
use Auth;

class SkillController extends Controller
{
    public function index()
    {
        $skills = Skill::get();
        return view('admin.skill.index' , compact('skills'));
    }

    public function create()
    {
        return view('admin.skill.update');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
                'title' => 'required|unique:skills',
            ]);

        $skill = Skill::create([
                'user_id' 		=> Auth::id(),
                'title' 		=> $request->title,
                'slug' 			=> createSlug($request->title , 'skills'),
                'description' 	=> $request->description,
            ]);

        return redirect()->to(route('admin.skill.index'))->with(['msg' => 'skill Successfully create.']);
    }

    public function edit($id)
    {
        $row = Skill::find($id);
        return view('admin.skill.update' , compact( 'row'));
    }

    public function update(Request $request , $id)
    {
    	$row = Skill::find($id);

    	$slug  = $row->slug;

    	if ($request->title != $row->title) {
	        $this->validate(request(), [
	                'title' => 'required',
	            ]);
	        $slug = createSlug($request->title , 'skills');
    	}


        $row->update([
            'user_id' 		=> Auth::id(),
            'title' 		=> $request->title,
            'slug' 			=> $slug,
            'description' 	=> $request->description,
        ]);

        return redirect()->to(route('admin.skill.index'))->with(['msg' => 'Skill Successfully updated.']);
    }

    public function destroy($id)
    {
        Skill::where('id' , $id)->delete();
        return response()->json(['status' => TRUE]);
    }
}
