<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\School;
use App\Team;
use Hash;
use App\State;

class CoachController extends Controller
{
    public function index()
    {
    	$coachs = User::where('type' , 'Coach')->get();
    	return view('admin.coach.index' , compact('coachs'));
    }

    public function create()
    {
    	$schools = School::get();
    	$teams = Team::get();
    	$states = State::where('country_id' ,231)->get();
    	return view('admin.coach.update' , compact('schools', 'states' , 'teams'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
                'email' => 'required|unique:users',
                'first_name'	=> 'required',
                'password'		=> 'required',
			]);	

    	$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'address'		=> $request->address,
    		'city'		    => $request->city,
    		'state'	    	=> $request->state,
    		'school_id'		=> $request->school_id,
    		'phone'			=> $request->phone,
    		'zip'		    => $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year, 
    		'participate'		=> $request->participate,
    		'home_gym'		=> $request->home_gym,
    		'type'			=> 'Coach',
    		'password'	=> Hash::make($request->password) 
    	];

    		$userCreate = User::create($data);

    		$userCreate->teams()->sync($request->teams);

    		return redirect()->to(route('coach.index'));

    	}

    	public function edit($id)
    	{
    		$schools = School::get();
    		$teams = Team::get();
    		$row = User::find($id);
    		$states = State::where('country_id' ,231)->get();
    		return view('admin.coach.update' , compact('schools' , 'teams' , 'row' , 'states'));
    	}

    	public function update(Request $request , $id)
    	{
    		$row = User::find($id);
                $rule['first_name']	='required';
    			if ($request->email != $row->email) {
    			$rule['email'] ='required|unique:users';
    			}
    		$this->validate(request(), $rule);	

    	$data = [
    		'email'	=> $request->email,
    		'first_name'	=> $request->first_name,
    		'last_name'		=> $request->last_name,
    		'address'		=> $request->address,
    		'city'		    => $request->city,
    		'state'	    	=> $request->state,
    		'school_id'		=> $request->school_id,
    		'phone'			=> $request->phone,
    		'zip'		    => $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year, 
    		'participate'		=> $request->participate,
    		'home_gym'		=> $request->home_gym,
    		'type'			=> 'Coach',
    		'password'	=> $request->password ?  Hash::make($request->password) :  $row->password ,
    	];

    	$row->update($data);

    		$row->teams()->sync($request->teams);

    		return redirect()->to(route('coach.index'));
		}

		public function show() {

		}
		
		public function deleteCoach($id) {
			$member = User::find($id);
			$member->delete();

			return redirect()->back()->with('status', 'Coach Deleted Successfully!');
		}

}
