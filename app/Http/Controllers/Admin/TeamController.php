<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use Auth;

class TeamController extends Controller
{
    public function index()
    {
        $teams = Team::get();
        return view('admin.team.index' , compact('teams'));
    }

    public function create()
    {
        return view('admin.team.update');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
                'title' => 'required|unique:teams',
            ]);

        $team = Team::create([
                'user_id' 		=> Auth::id(),
                'title' 		=> $request->title,
                'slug' 			=> createSlug($request->title , 'teams'),
                'description' 	=> $request->description,
            ]);

        return redirect()->to(route('admin.team.index'))->with(['msg' => 'Team Successfully create.']);
    }

    public function edit($id)
    {
        $row = Team::find($id);
        return view('admin.team.update' , compact( 'row'));
    }

    public function update(Request $request , $id)
    {
    	$row = Team::find($id);

    	$slug  = $row->slug;

    	if ($request->title != $row->title) {
	        $this->validate(request(), [
	                'title' => 'required',
	            ]);
	        $slug = createSlug($request->title , 'teams');
    	}


        $row->update([
            'user_id' 		=> Auth::id(),
            'title' 		=> $request->title,
            'slug' 			=> $slug,
            'description' 	=> $request->description,
        ]);

        return redirect()->to(route('admin.team.index'))->with(['msg' => 'Team Successfully updated.']);
    }

    public function destroy($id)
    {
        Team::where('id' , $id)->delete();
        return response()->json(['status' => TRUE]);
    }
}
