<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PaymentLogs;
use App\Clinic;
use App\Enroll;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function dashboard()
    {
    	$totalRevenue = PaymentLogs::sum('amount');
        $todaysRevenue = PaymentLogs::whereDate('created_at', '=', Carbon::today()->toDateString())->sum('amount');
        $totalClinics = Clinic::count();
        
        $todaysEnrollments = Enroll::join('clinics', 'enrolls.clinic_id', '=', 'clinics.id')
                                    ->join('schools', 'clinics.school_id', '=', 'schools.id')
                                    ->join('users', 'clinics.user_id', '=', 'users.id')
                                    ->whereDate('enrolls.created_at', '=', Carbon::today()->toDateString())
                                    ->select('enrolls.*', 'schools.name', 'users.first_name as clinic_first_name', 'users.last_name as clinic_last_name', 'clinics.cost')
                                    ->get();
        
        // dd($todaysEnrollments);
        
        return view('admin.dashboard', compact('totalRevenue', 'todaysRevenue', 'totalClinics', 'todaysEnrollments'));
    }

    public function clinics() {
        $clinics = Clinic::all();

        return view('admin.clinic.index', compact('clinics'));
    }
}
