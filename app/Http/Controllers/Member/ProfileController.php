<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Skill;
use App\School;
use App\State;
use Auth;
use Illuminate\Validation\ValidationException;

class ProfileController extends Controller
{

    public function index()
    {
    	$schools = School::get();
    	$skills	= Skill::get();
    	$states = State::where('country_id' , 231)->get();
    	return view('member.profile' , compact('skills' , 'schools' , 'states'));
    }

    public function profileUpdate(Request $request)
    { 
		// return $request;
		// $validated = $request->validated();
		// $this->validate(request(), [ 
        //         'first_name'	=> 'required', 
		// 	]);

		// $request->validate([
		// 	'first_name'	=> 'required'
		// ]);

		// $validated = $request->validated();
		$this->validate($request, [
			'first_name'	=> 'required',
			'date_of_birth'  =>  'required|date|before:today',
          ]);
		// dd(5);
    	$filenameprofile = Auth::user()->profile;

    	 if ($request->hasFile('profile')) {
            $profile = $request->file('profile');
            $extension = $profile->getClientOriginalExtension();
            $filenameprofile  = 'uploads/profile/'. md5($profile) . time() . '.' . $extension;
            $profile->move('./uploads/profile/', $filenameprofile);
            if (isset($row->profile)) {
                if(file_exists($row->profile)){ 
                     File::delete($row->profile);
                }
            }
        }

		if ($request->has('graduation_year')) {
			$data = [ 
    		'first_name'		=> $request->first_name,
    		'last_name'			=> $request->last_name,
    		'address'			=> $request->address,
    		'city'		    	=> $request->city,
    		'state'	    		=> $request->state, 
    		'phone'				=> $request->phone,
    		'zip'		    	=> $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		'graduation_year'	=> $request->graduation_year, 
    		'participate'		=> $request->participate,
    		'home_gym'			=> $request->home_gym,
    		// 'type'				=> $request->type, 
			'profile'			=> $filenameprofile,
			'youtubelinks'		=> $request->youtubelinks,
    		'shirt_size'		=> $request->shirt_size,
    	];
		}
		else {
			$data = [ 
    		'first_name'		=> $request->first_name,
    		'last_name'			=> $request->last_name,
    		'address'			=> $request->address,
    		'city'		    	=> $request->city,
    		'state'	    		=> $request->state, 
    		'phone'				=> $request->phone,
    		'zip'		    	=> $request->zip,
    		'date_of_birth'		=> $request->date_of_birth,
    		// 'graduation_year'	=> $request->graduation_year, 
    		'participate'		=> $request->participate,
    		'home_gym'			=> $request->home_gym,
    		// 'type'				=> $request->type, 
			'profile'			=> $filenameprofile,
			'youtubelinks'		=> $request->youtubelinks,
    		'shirt_size'		=> $request->shirt_size,
    	];
		}

    	Auth::user()->update($data);

    	Auth::user()->schools()->sync($request->schools);
    	Auth::user()->skills()->sync($request->skills);

    	return redirect()->back()->with('status', 'Profile Updated Successfully!');
    }
}
