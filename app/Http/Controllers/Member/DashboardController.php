<?php

namespace App\Http\Controllers\Member;

use Auth;
use App\Clinic;
use App\Enroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Notes;

class DashboardController extends Controller
{ 
    public function index(Request $request)
    {
        $schools_list=DB::table('school_notifications')->where('user_id',Auth::id())->get();
        global $schools_ids;
        foreach ($schools_list as $value) {
            # code...
            $schools_ids[]=$value->school_id;
        }
        // $clinics_list=Clinic::whereIn('school_id',$schools_ids)->get();

        // global $clinics_ids;
        // foreach ($clinics_list as $value) {
        //     # code...
        //     $clinics_ids[]=$value->id;
        // }

        // // $enrolls = Enroll::WhereIn('clinic_id',$clinics_ids)->get();
        // $clinics=$clinics_list;

        $clinics = Clinic::whereHas('school' , function($query) use ($request){


            if ($request->q) {
               $query->where('pay_by_date','>=',date('Y-m-d'))->where('name', 'LIKE', "%".$request->q."%");
            }

        })->where('pay_by_date','>=',date('Y-m-d'))->where(function ($query) use($schools_ids)
            {
                if(!empty($schools_ids))
                 $query->whereIn('school_id',$schools_ids);
            })->latest()->get();

   
    	return view('member.dashboard' , compact('clinics'));
    }

    public function enrolled_clinic(Request $request){

        $enrolls = Enroll::join('clinics', 'enrolls.clinic_id', 'clinics.id')
        ->where('member_id', Auth::user()->id)
        ->where('clinics.pay_by_date','>=',date('Y-m-d'))
        ->select('enrolls.*', 'clinics.id as clinic_primary_id', 'clinics.user_id as clinic_user_id', 'clinics.school_id as clinic_school_id', 'clinics.open_age_to as clinic_open_age_to', 'clinics.open_age_from as clinic_open_age_from', 'clinics.assigned_waiver as clinic_assigned_waiver', 'clinics.date as clinic_date', 'clinics.time as clinic_time', 'clinics.in_time as clinic_in_time', 'clinics.cost as clinic_cost', 'clinics.what_to_wear as clinic_what_to_wear', 'clinics.pay_by_date as clinic_pay_by_date', 'clinics.created_at as clinic_created_at', 'clinics.updated_at as clinic_updated_at')
        ->get();
        // dd($enrolls);
        
        $schools_list=DB::table('school_notifications')->where('user_id',Auth::id())->get();
        global $schools_ids;
        foreach ($schools_list as $value) {
            # code...
            $schools_ids[]=$value->school_id;
        }
        // return $schools_ids;
        // dd(array_push($schools_ids));
        // $clinics_list=Clinic::whereIn('school_id',$schools_ids)->get();
        $clinics_list=Clinic::where('school_id',$schools_ids)->get();
        // dd($clinics_list);
        global $clinics_ids;
        foreach ($clinics_list as $value) {
            # code...
            $clinics_ids[]=$value->id;
        }
        // dd($clinics_ids);

        // $enrolls = Enroll::WhereIn('clinic_id',$clinics_ids)->where('member_id' , Auth::id())->get();
        // return $enrolls;
        // return view('member.enrolled_clinics' , compact('enrolls'));

        // $enrolls = Enroll::where('member_id', Auth::user()->id)->get();
        
        // $enrolls = Enroll::join('clinics', 'enrolls.clinic_id', 'clinics.id')
        // ->Where('clinic_id',$clinics_ids)
        // ->where('member_id', Auth::user()->id)
        // ->where('clinics.pay_by_date','>=',date('Y-m-d'))
        // ->select('enrolls.*', 'clinics.id as clinic_primary_id', 'clinics.user_id as clinic_user_id', 'clinics.school_id as clinic_school_id', 'clinics.open_age_to as clinic_open_age_to', 'clinics.open_age_from as clinic_open_age_from', 'clinics.assigned_waiver as clinic_assigned_waiver', 'clinics.date as clinic_date', 'clinics.time as clinic_time', 'clinics.in_time as clinic_in_time', 'clinics.cost as clinic_cost', 'clinics.what_to_wear as clinic_what_to_wear', 'clinics.pay_by_date as clinic_pay_by_date', 'clinics.created_at as clinic_created_at', 'clinics.updated_at as clinic_updated_at')
        // ->get();

        // return $enrolls;

        $notes = Notes::where('user_id', Auth::user()->id)->get();
        
        return view('member.enrolled_clinics' , compact('enrolls', 'notes'));
    }


    // public function scheduleClinic()
    // {
    // 	$clinics = [];
    // 	return view('member.scheduleClinic' , compact('clinics'));
    // }

    public function notes(Request $request) {
        // return $request;
        $n = new Notes;

        $n->user_id = Auth::user()->id;
        $n->title = $request->title;
        $n->date = $request->date;
        $n->time = $request->time;

        $n->save();

        return $n;
    }
}
