<?php

namespace App\Http\Controllers;

use App\UniformRequirement;
use Illuminate\Http\Request;

class UniformRequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UniformRequirement  $uniformRequirement
     * @return \Illuminate\Http\Response
     */
    public function show(UniformRequirement $uniformRequirement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UniformRequirement  $uniformRequirement
     * @return \Illuminate\Http\Response
     */
    public function edit(UniformRequirement $uniformRequirement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UniformRequirement  $uniformRequirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UniformRequirement $uniformRequirement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UniformRequirement  $uniformRequirement
     * @return \Illuminate\Http\Response
     */
    public function destroy(UniformRequirement $uniformRequirement)
    {
        //
    }
}
