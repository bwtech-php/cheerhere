<?php

namespace App\Http\Controllers;

use App\clinicDetails;
use Illuminate\Http\Request;

class ClinicDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\clinicDetails  $clinicDetails
     * @return \Illuminate\Http\Response
     */
    public function show(clinicDetails $clinicDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\clinicDetails  $clinicDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(clinicDetails $clinicDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\clinicDetails  $clinicDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, clinicDetails $clinicDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\clinicDetails  $clinicDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(clinicDetails $clinicDetails)
    {
        //
    }
}
