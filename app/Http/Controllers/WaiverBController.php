<?php

namespace App\Http\Controllers;

use App\WaiverB;
use Illuminate\Http\Request;

class WaiverBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WaiverB  $waiverB
     * @return \Illuminate\Http\Response
     */
    public function show(WaiverB $waiverB)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WaiverB  $waiverB
     * @return \Illuminate\Http\Response
     */
    public function edit(WaiverB $waiverB)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WaiverB  $waiverB
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WaiverB $waiverB)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WaiverB  $waiverB
     * @return \Illuminate\Http\Response
     */
    public function destroy(WaiverB $waiverB)
    {
        //
    }
}
