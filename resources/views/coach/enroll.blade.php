@extends('coach.layouts.master')

@section('mainContent')
{{-- @dd($__data) --}}
<div class="coach-form">

        <div class="container">

          <div class="row">

            @include('coach.layouts.sidebar')

            <div class="col-9 naves-content">

              <div class="tab-content" id="v-pills-tabContent">

                <div class="tab-pane fade show active coach-dashboard" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">

                  {{-- <h2>Coach Dashboard</h2> --}}

                  <div class="row">

                   @forelse($enrolls as $clinic)

                    <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Clinic</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip</th>
                        <th>Age</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Youtube Links</th>
                        <th>Payment Status</th>
                        <th>Waiver View</th>
                      </tr> 
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{ $clinic->clinic->school->name ?? null }}</td>
                        <td>{{ $clinic->first_name . '  ' . $clinic->last_name }}</td>
                        <td>{{ $clinic->address }}</td>
                        <td>{{ $clinic->city }}</td>
                        <td>{{ $clinic->state_name }}</td>
                        <td>{{ $clinic->zip }}</td>
                        <td>{{ $clinic->age }}</td>
                        <td>{{ $clinic->phone }}</td>
                        <td>{{ $clinic->email }}</td>
                        {{-- <td><a href="{{ $clinic->youtubelinks }}">{{ $clinic->youtubelinks }}</a> </td> --}}

                        {{-- <td>
                          @php 
                          // echo $clinic->youtubelinks;
                            foreach(explode(' ',$clinic->youtubelinks) as $a)
                            // echo $a;
                            echo "<a href='$a' > $a </a>";

                            
                            // for ($x = 0; $x < count(explode(PHP_EOL,$clinic->youtubelinks)); $x++) {
                            //   echo "<a href='{{ $clinic->youtubelinks }}' >" . $clinic->youtubelinks . "  </a>";
                            // }
                          @endphp
                        </td> --}}

                        {{-- <td>@php echo "<a href='{{ $clinic->youtubelinks }}'>" . nl2br( $clinic->youtubelinks) . "</a>"; echo count(explode(PHP_EOL,$clinic->youtubelinks)) @endphp</td> --}}
                        <td>{{ $clinic->youtubelinks }}</td>
                        @if($clinic->payment_status==0)
                        <td>
                            <i class="fas fa-ban text-danger"></i> <b><span>UnPaid</span></b>
                        </td>
                        @else
                        <td>
                          <i class="fas fa-check text-success"></i> <b><span>Paid</span></b>
                        </td>
                        @endif

                        <td><a href="{{ url('/waiver/view/'.$clinic->id) }}">view</a></td>

                      </tr>

                    </tbody>
                  </table>

                  {{-- 	<div class="col-lg-3">

                  	    <div class="clinic-box">

                          <div class="img-box">

                            <img src="/assets/front/images/1.jpg" class="img-fluid" alt="">

                          </div>

                          <div class="overlay">

                            <h4>{{ $clinic->clinic->school->name ?? null }}</h4>

                            <h5>{{ assigned_waiver($clinic->clinic->assigned_waiver) ?? null }}</h5>

                            <a href="{{ route('coach.clinic.show' , $clinic->clinic_id) }}" class="btn">View Clinic<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-2" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg><!-- <i class="fas fa-angle-right ml-2"></i> --></a>

                          </div>

                        </div>

                  	</div>  --}}

                  	@empty

                  	@endforelse



                  </div>





                </div>





              </div>

            </div>

          </div>

        </div>

      </div>





@endsection

