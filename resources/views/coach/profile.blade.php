@extends('coach.layouts.master')
@section('mainContent')
{{-- @dd($__data) --}}
{{-- @dd($waiver) --}}
{{-- @dd(Auth::user()) --}}
{{-- @if(isset($school))
@dd($school->id)
@endif --}}
{{-- @dd(Auth::user()->senior) --}}
{{-- @dd(session()) --}}
<div class="coach-form">
        <div class="container">
          <div class="row">
             @include('coach.layouts.sidebar')
            <div class="col-9 naves-content">
              @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
              @endif
              <div class="tab-content" id="v-pills-tabContent">
                <!--Coach Profile-->
                <div class="tab-pane fade show active " id="v-pills-member" role="tabpanel" aria-labelledby="v-pills-member-tab">
                  <h2>Update Your Profile</h2>
                                  <div class="row">
                    <div class="col-lg-8">
                    <form action="{{ route('coach.profile.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="type" value="Coach">
                    <div class="form-group row">
                      <label for="first_name" class="col-sm-2 col-form-label">First Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="first_name" name="first_name" value="{{ Auth::user()->first_name }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="last_name" class="col-sm-2 col-form-label">Last Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="last_name" id="last_name" value="{{ Auth::user()->last_name }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="school_id" class="col-sm-2 col-form-label">School Name:</label>
                      <div class="col-sm-10 addnew">
                        <select id="school_id" name="school_id" class="form-control" style="-webkit-appearance: none;">
                          <option value="">Select School</option>
                          @forelse ($schools as $singleSchool)
                          @if(isset($school))
                          <option value="{{ $school->id }}" selected>{{ $school->name }}</option>
                          @endif
                          <option value="{{ $singleSchool->id }}" {{ Auth::user()->school_id == $singleSchool->id ? 'selected' : null }}>{{ $singleSchool->name }}</option>
                          @empty
                          @endforelse
                        </select>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Add New</button>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Profile Picture</label>
                      <div class="col-sm-10">
                        <input type="file" name="profile"  class="form-control">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="city" class="col-sm-2 col-form-label">City Name:</label>
                      <div class="col-sm-10">
                        @if(isset($school))
                        <input type="text" class="form-control" id="city" name="city" value="{{ $school->city }}">
                        @else
                        <input type="text" class="form-control" id="city" name="city" value="{{ Auth::user()->city }}">
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="state" class="col-sm-4 col-form-label">State</label>
                          <div class="col-sm-7 addnew2">
                            <select id="state" name="state" class="form-control" style="-webkit-appearance: none;">
                              <option value="">Select State</option>
                             @forelse ($states as $state)
                             {{-- @if(isset($school))
                             <option value="{{ $school->state }}" selected>{{ $school->state_name }}</option>
                             @endif --}}
                             <option value="{{ $state->id }}" {{ Auth::user()->state == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                              @empty 
                              @endforelse 
                            </select>
                            
                          </div>
                          
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="zip" class="col-sm-4 col-form-label">Zip Code:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="zip" name="zip" value="{{ Auth::user()->zip }}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="email" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="phone" name="phone" value="{{ Auth::user()->phone }}">
                      </div>
                    </div>
                   {{--  <div class="form-group row">
                      <label for="password" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password">
                      </div>
                    </div> --}}
                    <div class="form-group row checkbox-in">
                      <div class="col-sm-12 checkbx">Check All Teams that apply:</div>
                      @forelse ($teams as $team)
                      <div class="col-sm-2 p-0">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="teams[]" {{ checkboxChecked(Auth::user()->teams , $team->id) }} value="{{  $team->id }}" id="gridCheck{{  $team->id }}">
                          <label class="form-check-label" for="gridCheck{{  $team->id }}">
                            {{ $team->title }}
                          </label>
                        </div>
                      </div>
                      @empty
                      @endforelse
                      
                     
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <button type="submit" class="btn btn-primary">Save</button> 
                      </div>
                    </div>
                  {{-- </form> --}}
                    </div>
                    <div class="col-lg-4">
                        <div class="img-box">
                            <img src="{{ image(Auth::user()->profile) }}" class="img-fluid">
                        </div>
                        <div class="text-center">
                          {{-- <button class="mt-3 btn btn-primary">Add Waiver here</button> --}}
                          <button class="mt-3 btn btn-primary" type="button" data-toggle="modal" data-target="#clinicLocationModal">Add Clinic Location here</button>
                          <div class="mt-3 row">
                            {{-- <div class="col-sm-6"> --}}
                              
                              <label for="assigned_waiver" class="col-form-label">Select Waiver</label>
                            {{-- </div> --}}
                            <div class="col-sm-8">
                              {{-- @isset($waiver->waiver_table_id)
                                <button>test</button>
                                @endisset --}}
                            <select name="waiver" id="assigned_waiver" class="form-control">
                                <option value="">Select Waiver</option>
                                {{-- @if(isset($waiver))
                                <button>test</button>
                                <option value="{{ $waiver->waiver_table_id }}" selected>{{ $waiver->waiver_name }}</option>
                                @else --}}
                                @foreach ($waivers as $item)
                                <option value="{{ $item->id }}" {{ Auth::user()->waiver_id == $item->id ? 'selected' : null }} >{{ $item->name }}</option>
                                @endforeach
                                {{-- @endif --}}
                            </select>
                            </div>
                          </div>
                          
                          <button class="mt-3 btn btn-primary" type="button" data-toggle="modal" data-target="#uniformModal">Add Uniform requirements here</button>
                        </div>
                    </div>
                  </div>
                </div>
              </form>
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
        
      {{-- Add New School Modal --}}
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Add School Name</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('coach.school.store') }}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="modal-body">
            
            <div class="form-group row">
              <label for="name" class="col-sm-3 col-form-label">Add School</label>
              <div class="col-sm-9 ">
                <input type="text" class="form-control name" id="name" name="name">
              </div>
            </div>
            <div class="form-group row">
              <label for="city" class="col-sm-3 col-form-label">City Name:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control city" id="city" name="city">
              </div>
            </div>
            <div class="form-group row">
              <label for="state" class="col-sm-3 col-form-label">State</label>
              <div class="col-sm-9">
                <select id="state" class="form-control state" name="state">
                  <option value="">Select State</option>
                  @forelse ($states as $state)
                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                  @empty
                  @endforelse
                </select>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="address" class="col-sm-3 col-form-label">Address</label>
              <div class="col-sm-9">
                <input type="tel" class="form-control address" id="address" name="address">
              </div>
            </div>
            <div class="form-group">
                <label for="image">Choose Image</label>
                <input class="col-sm-9 col-form-label" id="image" type="file" name="image" accept="image/*">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary schooladd">Save changes</button>
        </div>
          </form>
      </div>
    </div>
  </div>

{{-- Uniform requirements modal --}}
<div class="modal fade bd-example-modal-lg" id="uniformModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Uniform requirements</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('uniform.requirements') }}">
        @csrf
        <div class="modal-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th></th>
                <th scope="col">Sports Bra</th>
                <th scope="col">Shorts</th>
                <th scope="col">T-shirt</th>
                <th scope="col">Bow</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="minor" id="minors"><label class="form-check-label" for="minors">Minors</label></td>
                <td><input value="{{ Auth::user()->minor != null ? Auth::user()->minor->sports_bra : null }}" class="form-control" type="text" name="minor1" id="minor1" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->minor != null ? Auth::user()->minor->shorts : null }}" class="form-control" type="text" name="minor2" id="minor2" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->minor != null ? Auth::user()->minor->t_shirt : null }}" class="form-control" type="text" name="minor3" id="minor3" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->minor != null ? Auth::user()->minor->bow : null }}" class="form-control" type="text" name="minor4" id="minor4" placeholder="type color here"></td>
              </tr>
              <tr>
                <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="freshman" id="freshman"><label class="form-check-label" for="freshman">Freshman</label></td>
                <td><input value="{{ Auth::user()->freshman != null ? Auth::user()->freshman->sports_bra : null }}" class="form-control" type="text" name="freshman1" id="freshman1" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->freshman != null ? Auth::user()->freshman->shorts : null }}" class="form-control" type="text" name="freshman2" id="freshman2" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->freshman != null ? Auth::user()->freshman->t_shirt : null }}" class="form-control" type="text" name="freshman3" id="freshman3" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->freshman != null ? Auth::user()->freshman->bow : null }}" class="form-control" type="text" name="freshman4" id="freshman4" placeholder="type color here"></td>
              </tr>
              <tr>
                <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="sophomore" id="sophomore"><label class="form-check-label" for="sophomore">Sophomore</label></td>
                <td><input value="{{ Auth::user()->sophomore != null ? Auth::user()->sophomore->sports_bra : null }}" class="form-control" type="text" name="sophomore1" id="sophomore1" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->sophomore != null ? Auth::user()->sophomore->shorts : null }}" class="form-control" type="text" name="sophomore2" id="sophomore2" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->sophomore != null ? Auth::user()->sophomore->t_shirt : null }}" class="form-control" type="text" name="sophomore3" id="sophomore3" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->sophomore != null ? Auth::user()->sophomore->bow : null }}" class="form-control" type="text" name="sophomore4" id="sophomore4" placeholder="type color here"></td>
              </tr>
              <tr>
                <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="junior" id="junior"><label class="form-check-label" for="junior">Junior</label></td>
                <td><input value="{{ Auth::user()->junior != null ? Auth::user()->junior->sports_bra : null }}" class="form-control" type="text" name="junior1" id="junior1" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->junior != null ? Auth::user()->junior->shorts : null }}" class="form-control" type="text" name="junior2" id="junior2" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->junior != null ? Auth::user()->junior->t_shirt : null }}" class="form-control" type="text" name="junior3" id="junior3" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->junior != null ? Auth::user()->junior->bow : null }}" class="form-control" type="text" name="junior4" id="junior4" placeholder="type color here"></td>
              </tr>
              <tr>
                <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="senior" id="senior"><label class="form-check-label" for="senior">Senior</label></td>
                <td><input value="{{ Auth::user()->senior != null ? Auth::user()->senior->sports_bra : null }}" class="form-control" type="text" name="senior1" id="senior1" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->senior != null ? Auth::user()->senior->shorts : null }}" class="form-control" type="text" name="senior2" id="senior2" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->senior != null ? Auth::user()->senior->t_shirt : null }}" class="form-control" type="text" name="senior3" id="senior3" placeholder="type color here"></td>
                <td><input value="{{ Auth::user()->senior != null ? Auth::user()->senior->bow : null }}" class="form-control" type="text" name="senior4" id="senior4" placeholder="type color here"></td>
              </tr>
            </tbody>
          </table>
          {{-- <div class="row">
            <div class="col form-check checkbox-in">
              <input class="form-check-input" value="1" type="checkbox" name="vehicle1" id="vehicle1">
              <label class="form-check-label" for="vehicle1"> I have a bike</label><br>
              <input type="checkbox" name="" id="">
              <input type="checkbox" name="" id="">
              <input type="checkbox" name="" id="">
              <input type="checkbox" name="" id="">
            </div>
            <div class="col">
              Minors
              Freshman
              Sophomore
              Junior
              Senior
            </div>
            <div class="col">
              Test
              Minors
              Freshman
              Sophomore
              Junior
              Senior
            </div>
            <div class="col">
              Test
              Minors
              Freshman
              Sophomore
              Junior
              Senior
            </div>
            <div class="col">
              Test
              Minors
              Freshman
              Sophomore
              Junior
              Senior
            </div>
            <div class="col">
              Test
              Minors
              Freshman
              Sophomore
              Junior
              Senior
            </div>
          </div> --}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

{{-- Add Clinic Location Modal --}}
<div class="modal fade" id="clinicLocationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content">
<div class="modal-header">
  <h5 class="modal-title" id="exampleModalCenterTitle">Add Clinic Location</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
</div>
<form action="{{ route('clinic.location.store') }}" method="post" enctype="multipart/form-data">
    @csrf
<div class="modal-body">
    
    <div class="form-group row">
      <label for="name" class="col-sm-3 col-form-label">Location Name</label>
      <div class="col-sm-9 ">
        <input type="text" class="form-control name" id="name" name="name">
      </div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-sm-3 col-form-label">School</label>
      <div class="col-sm-9 ">
        <select id="school_id" name="school_id" class="form-control">
          <option value="">Select School</option>
          @forelse ($schools as $singleSchool)
          <option value="{{ $singleSchool->id }}">{{ $singleSchool->name }}</option>
          @empty
          @endforelse
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label for="city" class="col-sm-3 col-form-label">Street Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control city" id="city" name="address">
      </div>
    </div>
    <div class="form-group row">
      <label for="state" class="col-sm-3 col-form-label">City</label>
      <div class="col-sm-9">
        <input type="text" class="form-control city" id="city" name="city">
      </div>
    </div>
    
    <div class="form-group row">
      <label for="address" class="col-sm-3 col-form-label">State</label>
      <div class="col-sm-9">
        <select id="state" class="form-control state" name="state">
          <option value="">Select State</option>
          @forelse ($states as $state)
          <option value="{{ $state->id }}">{{ $state->name }}</option>
          @empty
          @endforelse
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label for="address" class="col-sm-3 col-form-label">Zip</label>
      <div class="col-sm-9">
        <input type="text" class="form-control city" id="city" name="zip">
      </div>
    </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary schooladd">Save changes</button>
</div>
  </form>
</div>
</div>
</div>

@endsection

@section('style')
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
 <script>
   function test() {
    // axios.get('api/school/detail')
    // .then(res => {
    //   console.log(res.data.test);
    // })

    $.ajax({url: "school/detail", success: function(result){
      console.log(result.test);
      // $("#div1").html(result);
    }});
   }
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $( "#minor1" ).prop( "disabled", true );
    $( "#minor2" ).prop( "disabled", true );
    $( "#minor3" ).prop( "disabled", true );
    $( "#minor4" ).prop( "disabled", true );
    $( "#freshman1" ).prop( "disabled", true );
    $( "#freshman2" ).prop( "disabled", true );
    $( "#freshman3" ).prop( "disabled", true );
    $( "#freshman4" ).prop( "disabled", true );
    $( "#sophomore1" ).prop( "disabled", true );
    $( "#sophomore2" ).prop( "disabled", true );
    $( "#sophomore3" ).prop( "disabled", true );
    $( "#sophomore4" ).prop( "disabled", true );
    $( "#junior1" ).prop( "disabled", true );
    $( "#junior2" ).prop( "disabled", true );
    $( "#junior3" ).prop( "disabled", true );
    $( "#junior4" ).prop( "disabled", true );
    $( "#senior1" ).prop( "disabled", true );
    $( "#senior2" ).prop( "disabled", true );
    $( "#senior3" ).prop( "disabled", true );
    $( "#senior4" ).prop( "disabled", true );
      
    $('#minors').change(function(){
    if($(this).is(":checked")) {
      $( "#minor1" ).prop( "disabled", false );
      $( "#minor2" ).prop( "disabled", false );
      $( "#minor3" ).prop( "disabled", false );
      $( "#minor4" ).prop( "disabled", false );
    }
    else {
      $( "#minor1" ).prop( "disabled", true );
      $( "#minor2" ).prop( "disabled", true );
      $( "#minor3" ).prop( "disabled", true );
      $( "#minor4" ).prop( "disabled", true );
    }
    });

    $('#freshman').change(function(){
    if($(this).is(":checked")) {
      $( "#freshman1" ).prop( "disabled", false );
      $( "#freshman2" ).prop( "disabled", false );
      $( "#freshman3" ).prop( "disabled", false );
      $( "#freshman4" ).prop( "disabled", false );
    }
    else {
      $( "#freshman1" ).prop( "disabled", true );
      $( "#freshman2" ).prop( "disabled", true );
      $( "#freshman3" ).prop( "disabled", true );
      $( "#freshman4" ).prop( "disabled", true );
    }
    });

    $('#sophomore').change(function(){
    if($(this).is(":checked")) {
      $( "#sophomore1" ).prop( "disabled", false );
      $( "#sophomore2" ).prop( "disabled", false );
      $( "#sophomore3" ).prop( "disabled", false );
      $( "#sophomore4" ).prop( "disabled", false );
    }
    else {
      $( "#sophomore1" ).prop( "disabled", true );
      $( "#sophomore2" ).prop( "disabled", true );
      $( "#sophomore3" ).prop( "disabled", true );
      $( "#sophomore4" ).prop( "disabled", true );
    }
    });

    $('#junior').change(function(){
    if($(this).is(":checked")) {
      $( "#junior1" ).prop( "disabled", false );
      $( "#junior2" ).prop( "disabled", false );
      $( "#junior3" ).prop( "disabled", false );
      $( "#junior4" ).prop( "disabled", false );
    }
    else {
      $( "#junior1" ).prop( "disabled", true );
      $( "#junior2" ).prop( "disabled", true );
      $( "#junior3" ).prop( "disabled", true );
      $( "#junior4" ).prop( "disabled", true );
    }
    });

    $('#senior').change(function(){
    if($(this).is(":checked")) {
      $( "#senior1" ).prop( "disabled", false );
      $( "#senior2" ).prop( "disabled", false );
      $( "#senior3" ).prop( "disabled", false );
      $( "#senior4" ).prop( "disabled", false );
    }
    else {
      $( "#senior1" ).prop( "disabled", true );
      $( "#senior2" ).prop( "disabled", true );
      $( "#senior3" ).prop( "disabled", true );
      $( "#senior4" ).prop( "disabled", true );
    }
    });
      
      });
</script>

@stop