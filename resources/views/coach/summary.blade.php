@extends('coach.layouts.master')

@section('mainContent')
{{-- @dd($__data) --}}
<head>
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<div class="coach-form">
    <div class="container">
        <div class="row">
            @include('coach.layouts.sidebar')
            <div class="col-9 naves-content">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active coach-dashboard" id="v-pills-home" role="tabpanel"
                        aria-labelledby="v-pills-home-tab">
                        <h2>Coach Summary</h2>
                        <div class="row">
                            <div class="rounded-lg shadow-lg col card text-white bg-primary mb-3" style="max-width: 18rem;">
                                <div class="card-header h4 text-center">Total Enrollments</div>
                                <div class="card-body">
                                    <h1 class="card-title text-center">{{ $totalEnrolls }}</h1>
                                </div>
                            </div>
                            <div class="rounded-lg shadow-lg ml-2 col card text-white bg-success mb-3" style="max-width: 18rem;">
                                <div class="card-header h4 text-center">Total Earnings</div>
                                <div class="card-body">
                                    <h1 class="card-title text-center">${{ $coachCut }}</h1>
                                </div>
                            </div>
                            {{-- <div class="rounded-lg shadow-lg ml-2 col card text-white bg-warning mb-3" style="max-width: 18rem;">
                                <div class="card-header h4 text-center">Coach<br>Cut (98%)</div>
                                <div class="card-body">
                                    <h1 class="card-title text-center">${{ $coachCut }}</h1>
                                </div>
                            </div>
                            <div class="rounded-lg shadow-lg ml-2 col card text-white bg-info mb-3" style="max-width: 18rem;">
                                <div class="card-header h4 text-center">Cheer Here Cut (2%)</div>
                                <div class="card-body">
                                    <h1 class="card-title text-center">${{ $cheerHereCut }}</h1>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>

                <h2 class="mt-5">Coach Clinic Summary</h2>
                <h3 class="mt-5">Paid Enrollments</h3> <hr>
                <table class="table table-bordered paid-enrollments table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Clinic Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Total Enrollments</th>
                            <th>Total Earnings</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($paidEnrollments as $data)
                        <tr>
                            <td><a href="{{ route('coach.clinic.enroll', $data->clinic_id) }}">{{ $data->name }}</a></td>
                            <td>{{ $data->date }}</td>
                            <td>{{ $data->time }}</td>
                            <td>{{ $data->total_enrolls }}</td>
                            <td>{{ $data->total_earnings }}</td>
                        </tr>   
                        @endforeach
                    </tbody>
                </table>
                <hr>
                <h3 class="mt-5">Unpaid Enrollments</h3> <hr>
                <table class="table table-bordered unpaid-enrollments table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Clinic Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Total Enrollments</th>
                            <th>Total Earnings</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unpaidEnrollments as $data)
                        <tr>
                            <td><a href="{{ route('coach.clinic.enroll', $data->clinic_id) }}">{{ $data->name }}</a></td>
                            <td>{{ $data->date }}</td>
                            <td>{{ $data->time }}</td>
                            <td>{{ $data->total_enrolls }}</td>
                            <td>{{ $data->total_earnings }}</td>
                        </tr>   
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('style')
<link rel="stylesheet" href="/assets/front/css/calender/core_main.css">
<link rel="stylesheet" href="/assets/front/css/calender/daygrid_main.css">
<link rel="stylesheet" href="/assets/front/css/calender/timegrid_main.css">
@endsection

@section('script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
//   $(function () {
    
//     var table = $('.paid-enrollments').DataTable({
//         processing: true,
//         serverSide: true,
//         ajax: "{{ route('paid-enrollments') }}",
//         columns: [
//             {data: 'name', name: 'name'},
//             {data: 'date', name: 'date'},
//             {data: 'time', name: 'time'},
//             {data: 'total_enrolls', name: 'total_enrolls'},
//             {data: 'total_earnings', name: 'total_amount', 
//                 orderable: true, 
//                 searchable: true
//             },
//         ]
//     });

//     $('.paid-enrollments').on('click', 'tr', function () {
//         var data = table.row( this ).data();
//         // window.open({{ url("test") }});
//         // window.location.href = 'http://www.google.com';
//         alert( 'You clicked on '+data[0]+'\'s row' );
//     });
//     $('tr').css('cursor','pointer');
    
//   });

//   $(function () {
    
//     var table = $('.unpaid-enrollments').DataTable({
//         processing: true,
//         serverSide: true,
//         ajax: "{{ route('unpaid-enrollments') }}",
//         columns: [
//             {data: 'name', name: 'name'},
//             {data: 'date', name: 'date'},
//             {data: 'time', name: 'time'},
//             {data: 'total_enrolls', name: 'total_enrolls'},
//             {data: 'total_earnings', name: 'total_amount', 
//                 orderable: true, 
//                 searchable: true
//             },
//         ]
//     });
    
//   });
</script>

@stop