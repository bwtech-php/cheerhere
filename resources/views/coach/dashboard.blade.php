@extends('coach.layouts.master')
@section('mainContent')
{{-- @dd($clinics) --}}
<div class="coach-form">
        <div class="container">
          <div class="row">
            @include('coach.layouts.sidebar')
            <div class="col-9 naves-content">
              <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active coach-dashboard" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                  @if (session('status'))
                    <div class="alert alert-success" role="alert">
                      {{ session('status') }}
                    </div>
                  @endif
                  <h2>Coach Dashboard</h2>
                  <div class="row">
                   @forelse($clinics as $clinic)
                  	<div class="col-lg-3">
                  	    <div class="clinic-box">
                          <div class="img-box">
                            <img src="{{ asset('img/schools/'. $clinic->image) }}" class="img-fluid" alt="">
                          </div>
                          <div class="overlay">
                            <h4>{{ $clinic->school->name }}</h4>
                            <h5>{{ assigned_waiver($clinic->waiver_name) ?? null }}</h5>
                            <a href="{{ route('coach.clinic.show' , $clinic->id) }}" class="btn">View Clinic<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-2" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg><!-- <i class="fas fa-angle-right ml-2"></i> --></a>
                          </div>
                        </div>
                  	</div>
                  	@empty
                  	@endforelse

                  </div>
                  <div class="row mt-5">
                      <div class="col-lg-8">
                          <div id='calendar'></div>
                      </div>
                  </div>

                </div>
                <!--Coach Profile-->
                <div class="tab-pane fade " id="v-pills-member" role="tabpanel" aria-labelledby="v-pills-member-tab">
                  <h2>Coach Profile</h2>

                </div>
                <!--Coach Profile-->
                <!--Schedule-->
                <div class="tab-pane fade single_clinic" id="v-pills-Schedule" role="tabpanel" aria-labelledby="v-pills-Schedule-tab">
                  <h2>Schedule a Clinic</h2>
                  <div class="row">
                  	<div class="col-lg-12">
                  		<form>
                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">Clinic Location:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam1" value="Location" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Open to Ages:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam2" value="12 to 15" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Assigned Waiver:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam2" value="Waiver 01" readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">Clinic Date:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam1" value="03/13/2020" readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Clinic Time:</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" value="06:00 pm" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Check in Time:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword3" value="05:45 pm" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Clinic Cost:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword3" value="$ 150.00" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword4" class="col-sm-2 col-form-label">What to Wear:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword4" value="White T-shirt" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword5" class="col-sm-2 col-form-label">Pay by Date:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword4" value="03/13/2020" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <button type="submit" class="btn btn-primary float-left"> View Registration</button>
                        <button type="submit" class="btn btn-primary float-left">Edit Clinic</button>
                      </div>
                    </div>
                  </form>
                  	</div>
                  </div>
                </div>
                <!--Schedule-->

              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('style')
    <link rel="stylesheet" href="/assets/front/css/calender/core_main.css" >
    <link rel="stylesheet" href="/assets/front/css/calender/daygrid_main.css" >
    <link rel="stylesheet" href="/assets/front/css/calender/timegrid_main.css" >
@endsection
@section('script')
<script src="/assets/front/js/calender/core_js.js" ></script>
    <script src="/assets/front/js/calender/interaction_js.js" ></script>
    <script src="/assets/front/js/calender/daygrid_js.js" ></script>
    <script src="/assets/front/js/calender/timegrid_js.js" ></script>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },

        navLinks: true, // can click day/week names to navigate views
        selectable: true,
        selectMirror: true,
        select: function(arg) {
          var title = prompt('Event Title:');
          if (title) {
            calendar.addEvent({
              title: title,
              start: arg.start,
              end: arg.end,
              allDay: arg.allDay
            })
          }
          calendar.unselect()
        },
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
         @forelse ($clinics as $clinic)

          {
            groupId: {{ $clinic->id }},
            title: '{{ $clinic->school->name }}',
            start: '{{ $clinic->date }}'
          },
         @empty
         @endforelse

        ]
      });
      calendar.render();
    });
  </script>
@stop
