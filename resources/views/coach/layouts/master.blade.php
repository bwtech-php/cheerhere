<!doctype html>
<html lang="en">
  <head>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
    <link rel="stylesheet" href="/assets/front/css/bootstrap.min.css" >
    <link rel="stylesheet" href="/assets/front/css/style.css" >
    <link rel="stylesheet" href="/assets/front/css/responsive.css" >

    
@yield('style')
    <title>Cheer Here USA</title>
  </head>
  <body>
    <section class="dashboard">
      <header>
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="img-box">
                <a href="/"><img src="/assets/front/images/dash-logo.png" class="img-fluid" alt=""></a>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="dash-searh">
                <div class="wrap">
                <button type="submit" class="searchButton">
                <i class="fa fa-search"></i>
                </button>
                <div class="search">
                  <input type="text" class="searchTerm" placeholder=" Type to Search...">
                </div>
              </div>
              </div>
            </div>
            <div class="col-lg-4 disflexend">
              <ul class="pull-right">
                <li>
                  @if (Auth::check())
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-primary">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  @else
                    <a href="{{ route('login') }}" class="btn btn-primary">Login</a>
                  @endif
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
      @yield('mainContent')
      

      <footer>
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="footer-menu">
                <ul class="list-unstyled">
                <li class="list-inline-item"><a href="/">Home</a></li>
                <li class="list-inline-item"><a href="{{ route('about') }}">About</a></li>
                <li class="list-inline-item"><a href="{{ route('clinic.index') }}">Clinics</a></li>
                <li class="list-inline-item"><a href="{{ route('contact') }}">Contact Us</a></li>
              </ul>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4">
              <div class="social-icons">
                    <ul class="list-unstyled">
                      <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                      <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                      <li class="list-inline-item"><a href="#"><i class="fab fa-snapchat"></i></a></li>
                      <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                  </div>
            </div>
            <div class="col-lg-4 disflex d-flex justify-content-center">
              <a href="/">© 2020. CheerHereUsa.com</a>
            </div>
            <div class="col-lg-4  disflex">
              <div class="privacy">
                <ul class="list-unstyled">
                <li class="list-inline-item"><a href="#">Help</a></li>
                <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
                <li class="list-inline-item"><a href="#">Terms of Use</a></li>
              </ul>
              </div>
            </div>
          </div>
        </div>

      </footer>
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js" ></script>
    <script src="/assets/front/js/bootstrap.min.js" ></script>
    @yield('script')
  </body>
</html>