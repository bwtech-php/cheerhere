<div class="col-3 naves">

  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

    <a class="nav-link  {{ Request::segment(2) == 'dashboard' ? 'active' : null }}" href="{{ route('coach.dashboard') }}">Coach Dashboard</a>

    <a class="nav-link  {{ Request::segment(2) == 'profile' ? 'active' : null }}" href="{{ route('coach.profile') }}" >Coach Profile</a>

    <a class="nav-link  {{ Request::segment(2) == 'schedule-clinic' ? 'active' : null }}" href="{{ route('coach.clinic') }}" >Schedule a Clinic</a>

    <a class="nav-link  {{ request()->url() == url('/coach/clinics/all') ? 'active' : null }}" href="{{ route('coach.clinics.all') }}" >All Clinics</a>

    <a class="nav-link  {{ Request::segment(2) == 'summary' ? 'active' : null }}" href="{{ route('coach.summary') }}" >Coach Summary</a>

    <!--<a class="nav-link " id="v-pills-Schedule-tab" data-toggle="pill" href="#v-pills-Schedule" role="tab" aria-controls="v-pills-Schedule" aria-selected="false">Schedule a Clinic</a>-->

    <a class="nav-link {{request()->url() == url('/coach/tickets') ? 'active' : null }}" href="{{ url('/tickets') }}">My tickets</a>

  </div>

</div>