@extends('coach.layouts.master')
@section('mainContent')
{{-- @dd(Auth::user()->school) --}}
{{-- @isset($clinic->open_age_to)
{{ $clinic->open_age_to }}
@endisset --}}
{{-- @dd($clinicLocations) --}}
{{-- @dd($__data) --}}
@php
  if (isset($row->id)) {
   $action = route('coach.clinic.update' , $row->id);
  }else{
   $action = route('coach.clinic.store');
  }
@endphp

<div class="coach-form">
        <div class="container">
          <div class="row">
            @include('coach.layouts.sidebar')
            <div class="col-9 naves-content">
              <div class="tab-content" id="v-pills-tabContent">
                <!-- Profile -->
                <div class="tab-pane fade show active profile " id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                  <h2>Schedule a Clinic</h2>
                  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                  <div class="row">
                    <div class="col-lg-8">
                      <form action="{{ $action }}" method="post">
                        {{-- <form action="/test" method="get"> --}}
                        @csrf
                        @isset ($row->id)

                        @method('PUT')
                        @endisset
                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">Clinic Location:</label>
                      <div class="col-sm-10 addnew">
                        <select id="inputState" name="clinic_location_id" class="form-control @error('clinic_location_id') is-invalid @enderror" style="-webkit-appearance: none;">
                          <option value=""> Select Location</option>
                          {{-- @forelse ($schools as $school)
                            <option value="{{ $school->id }}" {{ isset($clinic->id) ? $clinic->id == $clinic->id ? 'selected' : null : null }}>{{ $school->name }}</option>
                          @empty
                          @endforelse --}}
                          @forelse ($clinicLocations as $clinicLocation)
                            {{-- <option value="{{ $clinicLocation->id }}" {{ isset($clinic->id) ? $clinic->id == $clinicLocation->id ? 'selected' : null : null }}>{{ "$clinicLocation->name, $clinicLocation->address" }}, {{ $clinicLocation->state->name}}, {{" $clinicLocation->city, $clinicLocation->zip " }}</option> --}}
                            <option value="{{ $clinicLocation->id }}" {{ isset($clinic->clinic_location_id) ? $clinic->clinic_location_id == $clinicLocation->id ? 'selected' : null : null }}>{{ "$clinicLocation->name, $clinicLocation->address" }}, {{ $clinicLocation->state->name}}, {{" $clinicLocation->city, $clinicLocation->zip " }}</option>
                          @empty
                          @endforelse
                        </select>
                        {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Add New</button> --}}
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">School:</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="text" readonly value="{{ Auth::user()->school->name }}">
                        <input type="hidden" name="school_id" value="{{ Auth::user()->school->id }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="open_age" class="col-sm-2 col-form-label">Open to Ages:</label>
                      <div class="col-sm-10">
                          <div class="row">

                              <div class="col-sm-4">
                                <input type="number" class="form-control @error('open_age') is-invalid @enderror" id="open_age" name="open_age_from" value="{{ $clinic->open_age_from ??  old('open_age_from') }}" >
                              </div>
                              @error('open_age')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                              @enderror
                              <div class="col-sm-2 ml-2 mt-2">
                                <span>To</span>
                              </div>

                              <div class="col-sm-4">
                                <input type="number" class="form-control @error('open_age') is-invalid @enderror" id="open_age" name="open_age_to" value="{{ $clinic->open_age_to ?? old('open_age_to') }}" >
                              </div>
                              @error('open_age')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                              @enderror
                          </div>

                      </div>
                    </div>
                    <div class="form-group row">

                      <div class="col-sm-12">
                          <div class="row">
                            <label for="assigned_waiver" class="col-sm-2 col-form-label">Assigned Waiver:</label>
                            <div class="col-sm-8">
                            <select name="assigned_waiver" id="assigned_waiver" class="form-control">
                                <option value="">Select Waiver</option>
                                @foreach ($waiver as $item)
                                {{-- <option value="{{ $item->id }}" {{ isset($clinic->waiver_id) ? $clinic->waiver_id == $item->id ? 'selected' : null : null }}>{{ $item->name }}</option> --}}
                                <option value="{{ $item->id }}" {{ Auth::user()->waiver_id == $item->id ? 'selected' : null }} >{{ $item->name }}</option>
                                @if(Auth::user()->waiver_id == $item->id)
                                <option value="{{ $item->id }}" selected >{{ $item->name }}</option>
                                @elseif(isset($clinic->waiver_id))
                                @if($clinic->waiver_id == $item->id)
                                <option value="{{ $item->id }}" selected >{{ $item->name }}</option>
                                @endif
                                @endif
                                @endforeach
                            </select>
                            </div>
                            <div class="col-sm-2">
                                <span id="view_link"> </span>
                            </div>
                          </div>



                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="date" class="col-sm-2 col-form-label">Clinic Date:</label>
                      <div class="col-sm-10">
                        <input type="date" class="form-control @error('date') is-invalid @enderror" id="date" name="date" value="{{ $clinic->date ?? old('date') }}" >
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="time" class="col-sm-2 col-form-label">Clinic Time:</label>
                      <div class="col-sm-10">
                        <input type="time" class="form-control @error('time') is-invalid @enderror" id="time" name="time" value="{{ $clinic->time ?? old('time') }}" >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="in_time" class="col-sm-2 col-form-label">Check in Time:</label>
                      <div class="col-sm-10">
                        <input type="time" class="form-control" id="in_time" name="in_time" value="{{ $clinic->in_time ?? old('in_time') }}" >
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="cost" class="col-sm-2 col-form-label">Clinic Cost($):</label>
                      <div class="col-sm-10">
                        <input type="number" class="form-control @error('cost') is-invalid @enderror" id="cost" name="cost" value="{{ $clinic->cost ?? old('cost') }}" >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="what_to_wear" class="col-sm-2 col-form-label">What to Wear:</label>
                      {{-- <div class="col-sm-10"> --}}
                        {{-- <input type="text" name="what_to_wear" class="form-control @error('what_to_wear') is-invalid @enderror" id="what_to_wear" value="{{ $clinic->what_to_wear ?? old('what_to_wear') }}" > --}}
                        {{-- <div class="row">
                          <div class="form-check checkbox-in">
                            <input class="form-check-input" type="checkbox" value="1" name="freshman" id="defaultCheck1">
                            <label class="form-check-label" for="defaultCheck1">Freshman</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-check checkbox-in">
                            <input class="form-check-input" type="checkbox" value="1" name="sportsbra" id="sportsbra">
                            <label class="form-check-label" for="sportsbra">Sports Bra</label>
                          </div>
                          <div class="form-check ml-3 sportsbraselect">
                            <select name="sportsbravalue" id="">
                              <option>Red</option>
                              <option>White</option>
                              <option>Black</option>
                              <option>Gray</option>
                              <option>Orange</option>
                              <option>Navy Blue</option>
                              <option>Royal Blue</option>
                              <option>Green</option>
                            </select>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-check checkbox-in">
                            <input class="form-check-input" type="checkbox" value="1" name="shorts" id="shorts">
                            <label class="form-check-label" for="shorts">Shorts</label>
                          </div>
                          <div class="form-check ml-3 shortsselect">
                            <select name="shortsvalue" id="">
                              <option>Red</option>
                              <option>White</option>
                              <option>Black</option>
                              <option>Gray</option>
                              <option>Orange</option>
                              <option>Navy Blue</option>
                              <option>Royal Blue</option>
                              <option>Green</option>
                            </select>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-check checkbox-in">
                            <input class="form-check-input" type="checkbox" value="1" name="tshirt" id="tshirt">
                            <label class="form-check-label" for="tshirt">T-shirt</label>
                          </div>
                          <div class="form-check ml-3 tshirtselect">
                            <select name="tshirtvalue" id="">
                              <option>Red</option>
                              <option>White</option>
                              <option>Black</option>
                              <option>Gray</option>
                              <option>Orange</option>
                              <option>Navy Blue</option>
                              <option>Royal Blue</option>
                              <option>Green</option>
                            </select>
                          </div>
                        </div>
                        <div class="row">
                          <input type="text" name="what_to_wear_add" class="form-control mt-2" placeholder="Additional Instructions">
                        </div> --}}
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th></th>
                              <th>Sports Bra</th>
                              <th>Shorts</th>
                              <th>T-shirt</th>
                              <th>Bow</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="minor" id="minors"><label class="form-check-label" for="minors">Minors</label></td>
                              <td><input value="{{ Auth::user()->minor->sports_bra != null ? Auth::user()->minor->sports_bra : null }}" class="form-control" type="text" name="minor1" id="minor1" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->minor->shorts != null ? Auth::user()->minor->shorts : null }}" class="form-control" type="text" name="minor2" id="minor2" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->minor->t_shirt != null ? Auth::user()->minor->t_shirt : null }}" class="form-control" type="text" name="minor3" id="minor3" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->minor->bow != null ? Auth::user()->minor->bow : null }}" class="form-control" type="text" name="minor4" id="minor4" placeholder="type color here"></td>
                            </tr>
                            <tr>
                              <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="freshman" id="freshman"><label class="form-check-label" for="freshman">Freshman</label></td>
                              <td><input value="{{ Auth::user()->freshman->sports_bra != null ? Auth::user()->freshman->sports_bra : null }}" class="form-control" type="text" name="freshman1" id="freshman1" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->freshman->shorts != null ? Auth::user()->freshman->shorts : null }}" class="form-control" type="text" name="freshman2" id="freshman2" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->freshman->t_shirt != null ? Auth::user()->freshman->t_shirt : null }}" class="form-control" type="text" name="freshman3" id="freshman3" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->freshman->bow != null ? Auth::user()->freshman->bow : null }}" class="form-control" type="text" name="freshman4" id="freshman4" placeholder="type color here"></td>
                            </tr>
                            <tr>
                              <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="sophomore" id="sophomore"><label class="form-check-label" for="sophomore">Sophomore</label></td>
                              <td><input value="{{ Auth::user()->sophomore->sports_bra != null ? Auth::user()->sophomore->sports_bra : null }}" class="form-control" type="text" name="sophomore1" id="sophomore1" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->sophomore->shorts != null ? Auth::user()->sophomore->shorts : null }}" class="form-control" type="text" name="sophomore2" id="sophomore2" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->sophomore->t_shirt != null ? Auth::user()->sophomore->t_shirt : null }}" class="form-control" type="text" name="sophomore3" id="sophomore3" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->sophomore->bow != null ? Auth::user()->sophomore->bow : null }}" class="form-control" type="text" name="sophomore4" id="sophomore4" placeholder="type color here"></td>
                            </tr>
                            <tr>
                              <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="junior" id="junior"><label class="form-check-label" for="junior">Junior</label></td>
                              <td><input value="{{ Auth::user()->junior->sports_bra != null ? Auth::user()->junior->sports_bra : null }}" class="form-control" type="text" name="junior1" id="junior1" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->junior->shorts != null ? Auth::user()->junior->shorts : null }}" class="form-control" type="text" name="junior2" id="junior2" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->junior->t_shirt != null ? Auth::user()->junior->t_shirt : null }}" class="form-control" type="text" name="junior3" id="junior3" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->junior->bow != null ? Auth::user()->junior->bow : null }}" class="form-control" type="text" name="junior4" id="junior4" placeholder="type color here"></td>
                            </tr>
                            <tr>
                              <td class="form-check checkbox-in"><input class="form-check-input" value="1" type="checkbox" name="senior" id="senior"><label class="form-check-label" for="senior">Senior</label></td>
                              <td><input value="{{ Auth::user()->senior->sports_bra != null ? Auth::user()->senior->sports_bra : null }}" class="form-control" type="text" name="senior1" id="senior1" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->senior->shorts != null ? Auth::user()->senior->shorts : null }}" class="form-control" type="text" name="senior2" id="senior2" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->senior->t_shirt != null ? Auth::user()->senior->t_shirt : null }}" class="form-control" type="text" name="senior3" id="senior3" placeholder="type color here"></td>
                              <td><input value="{{ Auth::user()->senior->bow != null ? Auth::user()->senior->bow : null }}" class="form-control" type="text" name="senior4" id="senior4" placeholder="type color here"></td>
                            </tr>
                          </tbody>
                        </table>
                      {{-- </div> --}}
                    </div>
                    <div class="form-group row">
                      <label for="pay_by_date" class="col-sm-2 col-form-label">Pay by Date:</label>
                      <div class="col-sm-10">
                        <input type="date" class="form-control @error('pay_by_date') is-invalid @enderror" id="pay_by_date" name="pay_by_date" value="{{ $clinic->pay_by_date ?? old('pay_by_date') }}" >
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <button type="submit" class="btn btn-primary float-left">Schedule</button>
                      </div>
                    </div>
                  </form>
                    </div>
                    <div class="col-lg-4">
                      {{-- <div class="card">
                        <div class="card-header">
                          Available Clinics
                        </div>
                        <div class="card-body">
                          <p>Click on a clinic to update</p>
                          <ul>
                            @forelse($clinics as $clinic)
                            <li><a href="{{ route('updateClinic', $clinic->id) }}">{{ $clinic->name }}</a> ({{ $clinic->date }})</li>
                            @empty
                            <p>No clinics available</p>
                            @endforelse
                          </ul>
                        </div>
                      </div> --}}
                    </div>
                  </div>
                </div>
                <!-- Profile -->
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Add School Name</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form action="{{ route('coach.school.store') }}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="modal-body">
            
            <div class="form-group row">
              <label for="name" class="col-sm-3 col-form-label">Add School</label>
              <div class="col-sm-9 ">
                <input type="text" class="form-control name" id="name" name="name">
              </div>
            </div>
            <div class="form-group row">
              <label for="city" class="col-sm-3 col-form-label">City Name:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control city" id="city" name="city">
              </div>
            </div>
            <div class="form-group row">
              <label for="state" class="col-sm-3 col-form-label">State</label>
              <div class="col-sm-9">
                <select id="state" class="form-control state" name="state">
                  <option value="">Select State</option>
                  @forelse ($states as $state)
                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                  @empty
                  @endforelse
                </select>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="address" class="col-sm-3 col-form-label">Address</label>
              <div class="col-sm-9">
                <input type="tel" class="form-control address" id="address" name="address">
              </div>
            </div>
            <div class="form-group">
                <label for="image">Choose Image</label>
                <input class="col-sm-9 col-form-label" id="image" type="file" name="image" accept="image/*">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary schooladd">Save changes</button>
        </div>
          </form>
      </div>
    </div>
  </div>
@endsection

@section('style')
@endsection

@section('script')
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
  <script>
    $(document).ready(function() {
    $('#multiple-checkboxes').multiselect({
    includeSelectAllOption: true,
    });
    });
    </script>
    <script>
    $(document).ready(function() {
    $('#multiple-checkboxess').multiselect({
    includeSelectAllOption: true,
    });

    $("#assigned_waiver").change(function () {
        var end = $('#assigned_waiver option:selected').val();
        if(end=="1"){

            document.getElementById('view_link').innerHTML='<a  target="_blank" rel="noopener noreferrer"  href="{{url('/assets/waiver/waiverA.pdf') }}">View</a>';

        }

        if(end=="2"){
            document.getElementById('view_link').innerHTML='<a  target="_blank" rel="noopener noreferrer"  href="{{url('/assets/waiver/waiverB.pdf') }}">View</a>';
        }


    });


    
    });

    // jQuery(document).ready(function($) {
    //   console.log(2);
    //   $('body').on('click', 'input[name=sportsbra]', function( ) {
    //     console.log($(this).val())
    //     console.log(2);
    //     if ($(this).val() == "1") {
    //       $('.sportsbraselect').show();
    //     }else{
    //       $('.sportsbraselect').hide();
    //     }
    //   });

    // });

    
    
    </script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.sportsbraselect').hide();
    $('.shortsselect').hide();
    $('.tshirtselect').hide();
  
    $('#sportsbra').change(function(){
    if($(this).is(":checked"))
    // console.log(1)
    $('.sportsbraselect').show();
    else
    // console.log(2)
    $('.sportsbraselect').hide();

    });

    $('#shorts').change(function(){
    if($(this).is(":checked"))
    // console.log(1)
    $('.shortsselect').show();
    else
    // console.log(2)
    $('.shortsselect').hide();

    });

    $('#tshirt').change(function(){
    if($(this).is(":checked"))
    // console.log(1)
    $('.tshirtselect').show();
    else
    // console.log(2)
    $('.tshirtselect').hide();
    });

    $( "#minor1" ).prop( "disabled", true );
    $( "#minor2" ).prop( "disabled", true );
    $( "#minor3" ).prop( "disabled", true );
    $( "#minor4" ).prop( "disabled", true );
    $( "#freshman1" ).prop( "disabled", true );
    $( "#freshman2" ).prop( "disabled", true );
    $( "#freshman3" ).prop( "disabled", true );
    $( "#freshman4" ).prop( "disabled", true );
    $( "#sophomore1" ).prop( "disabled", true );
    $( "#sophomore2" ).prop( "disabled", true );
    $( "#sophomore3" ).prop( "disabled", true );
    $( "#sophomore4" ).prop( "disabled", true );
    $( "#junior1" ).prop( "disabled", true );
    $( "#junior2" ).prop( "disabled", true );
    $( "#junior3" ).prop( "disabled", true );
    $( "#junior4" ).prop( "disabled", true );
    $( "#senior1" ).prop( "disabled", true );
    $( "#senior2" ).prop( "disabled", true );
    $( "#senior3" ).prop( "disabled", true );
    $( "#senior4" ).prop( "disabled", true );
      
    $('#minors').change(function(){
    if($(this).is(":checked")) {
      $( "#minor1" ).prop( "disabled", false );
      $( "#minor2" ).prop( "disabled", false );
      $( "#minor3" ).prop( "disabled", false );
      $( "#minor4" ).prop( "disabled", false );
    }
    else {
      $( "#minor1" ).prop( "disabled", true );
      $( "#minor2" ).prop( "disabled", true );
      $( "#minor3" ).prop( "disabled", true );
      $( "#minor4" ).prop( "disabled", true );
    }
    });

    $('#freshman').change(function(){
    if($(this).is(":checked")) {
      $( "#freshman1" ).prop( "disabled", false );
      $( "#freshman2" ).prop( "disabled", false );
      $( "#freshman3" ).prop( "disabled", false );
      $( "#freshman4" ).prop( "disabled", false );
    }
    else {
      $( "#freshman1" ).prop( "disabled", true );
      $( "#freshman2" ).prop( "disabled", true );
      $( "#freshman3" ).prop( "disabled", true );
      $( "#freshman4" ).prop( "disabled", true );
    }
    });

    $('#sophomore').change(function(){
    if($(this).is(":checked")) {
      $( "#sophomore1" ).prop( "disabled", false );
      $( "#sophomore2" ).prop( "disabled", false );
      $( "#sophomore3" ).prop( "disabled", false );
      $( "#sophomore4" ).prop( "disabled", false );
    }
    else {
      $( "#sophomore1" ).prop( "disabled", true );
      $( "#sophomore2" ).prop( "disabled", true );
      $( "#sophomore3" ).prop( "disabled", true );
      $( "#sophomore4" ).prop( "disabled", true );
    }
    });

    $('#junior').change(function(){
    if($(this).is(":checked")) {
      $( "#junior1" ).prop( "disabled", false );
      $( "#junior2" ).prop( "disabled", false );
      $( "#junior3" ).prop( "disabled", false );
      $( "#junior4" ).prop( "disabled", false );
    }
    else {
      $( "#junior1" ).prop( "disabled", true );
      $( "#junior2" ).prop( "disabled", true );
      $( "#junior3" ).prop( "disabled", true );
      $( "#junior4" ).prop( "disabled", true );
    }
    });

    $('#senior').change(function(){
    if($(this).is(":checked")) {
      $( "#senior1" ).prop( "disabled", false );
      $( "#senior2" ).prop( "disabled", false );
      $( "#senior3" ).prop( "disabled", false );
      $( "#senior4" ).prop( "disabled", false );
    }
    else {
      $( "#senior1" ).prop( "disabled", true );
      $( "#senior2" ).prop( "disabled", true );
      $( "#senior3" ).prop( "disabled", true );
      $( "#senior4" ).prop( "disabled", true );
    }
    });

    });
</script>

@stop

