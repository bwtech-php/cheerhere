@extends('coach.layouts.master')
@section('mainContent')
{{-- @dd($__data) --}}
<div class="coach-form">
        <div class="container">
          <div class="row">

              @include('coach.layouts.sidebar')
              {{-- @dd($waiver->id); --}}

            <div class="col-9 naves-content">
              <div class="tab-content" id="v-pills-tabContent">
                <!-- Profile -->
                <div class="tab-pane fade show active profile single_clinic" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                  <h2>Single Clinic</h2>
                  <div class="row">
                    <div class="col-lg-12">
                      <form>
                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">Clinic Location:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam1" value="{{ $row->school->name ?? null }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Open to Ages:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam2" value="{{ $row->open_age_to }} To {{ $row->open_age_from }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Assigned Waiver:</label>
                      <div class="col-sm-10">
                        <div class="row">
                          <input type="text" class="form-control col-md-2" id="inputnam2" value="{{ $waiver->name }}"  readonly>
                          @if($row->assigned_waiver == 1)
                          <a class="col-md-4 my-auto" target="_blank" rel="noopener noreferrer"  href="{{url('/assets/waiver/waiverA.pdf') }}">View</a>
                          @elseif($row->assigned_waiver == 2)
                          <a class="col-md-4 my-auto" target="_blank" rel="noopener noreferrer"  href="{{url('/assets/waiver/waiverB.pdf') }}">View</a>
                          @endif
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">Clinic Date:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam1" value="{{ $row->date }}" readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Clinic Time:</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" value="{{ $row->time }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Check in Time:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword3" value="{{ $row->in_time }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Clinic Cost:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword3" value="${{ $row->cost }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword4" class="col-sm-2 col-form-label">What to Wear:</label>
                      <div class="col-sm-10">
                        {{-- <input type="tel" class="form-control" id="inputPassword4" value="{{ $row->clinic_what_to_wear }}" readonly> --}}
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Uniform Type</th>
                              <th>Sports Bra</th>
                              <th>Shorts</th>
                              <th>T-shirt</th>
                              <th>Bow</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($clinicDetails as $clinicDetail)
                              <tr>
                                <td>{{ $clinicDetail->uniform_type }}</td>
                                @isset($clinicDetail->sports_bra)
                                  <td>{{ $clinicDetail->sports_bra }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                                @isset($clinicDetail->shorts)
                                  <td>{{ $clinicDetail->shorts }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                                @isset($clinicDetail->t_shirt)
                                  <td>{{ $clinicDetail->t_shirt }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                                @isset($clinicDetail->bow)
                                  <td>{{ $clinicDetail->bow }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                        {{-- @foreach($clinicDetails as $clinicDetail)
                          <p>{{ $clinicDetail->uniform_type }}</p>
                        @endforeach --}}
                        {{-- <div class="row">
                          @isset($clinicDetails->freshman)
                            <div class="col-4"><strong class="mr-3">Freshman:</strong></div> <div class="col-8"> <i class="fas fa-check-circle"></i> </div>
                          @else
                          <div class="col-4"><strong class="mr-3">Freshman:</strong></div> <div class="col-8"> <i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div>
                        <div class="row">
                          @isset($clinicDetails->sportsbra)
                            <div class="col-4"><strong class="mr-3">Sports Bra:</strong></div> <div class="col-8"> <i class="fas fa-check-circle"></i> {{ $clinicDetails->sportsbra }}</div>
                          @else
                          <div class="col-4"><strong class="mr-3">Sports Bra:</strong> </div> <div class="col-8"><i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div>
                        <div class="row">
                          @isset($clinicDetails->shorts)
                            <div class="col-4"><strong class="mr-3">Shorts:</strong> </div> <div class="col-8"><i class="fas fa-check-circle"></i> {{ $clinicDetails->shorts }}</div>
                          @else
                          <div class="col-4"><strong class="mr-3">Shorts:</strong></div> <div class="col-8"> <i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div>
                        <div class="row">
                          @isset($clinicDetails->t_shirt)
                            <div class="col-4"><strong class="mr-3">T-Shirt:</strong> </div> <div class="col-8"><i class="fas fa-check-circle"></i> {{ $clinicDetails->t_shirt }}</div>
                          @else
                          <div class="col-4"><strong class="mr-3">T-Shirt:</strong>  </div> <div class="col-8"><i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div> --}}
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword5" class="col-sm-2 col-form-label">Pay by Date:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword4" value="{{ $row->pay_by_date }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <a href="{{ route('coach.clinic.enroll', $row->id) }}" class="btn btn-primary float-left"> View Registration</a>
                        <a href="{{ route('coach.clinic.edit' , $row->id) }}" class="btn btn-primary float-left ml-2">Edit Clinic</a>
                        <a href="{{ route('coach.clinic.delete' , $row->id) }}" class="btn btn-danger float-left ml-2">Delete</a>
                      </div>
                    </div>
                  </form>
                    </div>
                  </div>
                </div>
                <!-- Profile -->
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('style')
@endsection
@section('script')
 <script>
  $(document).ready(function() {

    $('body').on('click', '.schooladd', function(event) {
      event.preventDefault();
       var self = $(this)
       var form = self.closest('form');
       $('.is-invalid').removeClass('is-invalid');
       $.ajax({
         url: form.attr('action'),
         type: 'POST',
         dataType: 'json',
         data: form.serialize(),
       })
       .done(function(data) {
        $('#school_id').append('<option value="'+data.id+'">'+data.name+'</option>')
        $('#exampleModalCenter').modal('hide')
       })
       .fail(function(data) {
          $.each(data.responseJSON.errors, function(index, val) {
             form.find('.' + index).addClass('is-invalid');
          });
       })
       .always(function() {

       });

    });
  });
</script>
@stop
