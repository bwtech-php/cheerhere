@extends('layouts.registermaster')

@section('mainContent')
<div class="coach-form">
        <div class="container">
          <div class="row">
            <div class="col-3 naves">
               <div class="nav flex-column nav-pills" id="v-pills-tab"  >
                <a class="nav-link active" id="v-pills-home-tab" href="{{ route('coach.register.form') }}" role="tab" aria-controls="v-pills-home" aria-selected="true">Coach Registration</a>
                <a class="nav-link " id="v-pills-profile-tab"  href="{{ route('member.register.form') }}"   aria-selected="true">Member Registration</a>
                
              </div>
            </div>
            <div class="col-9 naves-content"> 
              <div class="tab-content" id="v-pills-tabContent"> 
                  <h2>Coach Registration</h2>
                  <form action="{{ route('member.register.store') }}" method="post">
                    @csrf
                    <input type="hidden" name="type" value="Coach">
                    <div class="form-group row">
                      <label for="first_name" class="col-sm-2 col-form-label">First Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="first_name" name="first_name" >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="last_name" class="col-sm-2 col-form-label">Last Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="last_name" id="last_name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="school_id" class="col-sm-2 col-form-label">School Name:</label>
                      <div class="col-sm-10 addnew">
                        <select id="school_id" name="school_id" class="form-control" style="-webkit-appearance: none;">
                          <option value="">Select School</option>
                          @forelse ($schools as $school)
                          <option value="{{ $school->id }}">{{ $school->name }}</option>
                          @empty
                          @endforelse
                        </select>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Add New</button>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="city" class="col-sm-2 col-form-label">City Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="city" name="city">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="state" class="col-sm-4 col-form-label">State</label>
                          <div class="col-sm-7 addnew2">
                            <select id="state" name="state" class="form-control" style="-webkit-appearance: none;">
                              <option value="">Select State</option>
                             @forelse ($states as $state)
                                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                              @empty 
                              @endforelse 
                            </select>
                            
                          </div>
                          
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="zip" class="col-sm-4 col-form-label">Zip Code:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="zip" name="zip">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="email" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                      <div class="col-sm-10">
                        <input type="number" class="form-control" id="phone" name="phone">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="password" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password">
                      </div>
                    </div>
                    <div class="form-group row checkbox-in">
                      <div class="col-sm-12 checkbx">Check All Teams that apply:</div>
                      @forelse ($teams as $team)
                      <div class="col-sm-2 p-0">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="teams[]" value="{{  $team->id }}" id="gridCheck{{  $team->id }}">
                          <label class="form-check-label" for="gridCheck{{  $team->id }}">
                            {{ $team->title }}
                          </label>
                        </div>
                      </div>
                      @empty
                      @endforelse
                      
                     
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn btn-primary">Reset</button>
                      </div>
                    </div>
                  </form>
                
              </div>
            </div>
          </div>
        </div>
      </div>

  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Add School Name</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form action="{{ route('coach.school.store') }}" method="post">
            @csrf
        <div class="modal-body">
            
            <div class="form-group row">
              <label for="name" class="col-sm-3 col-form-label">Add School Name:</label>
              <div class="col-sm-9 ">
                <input type="text" class="form-control name" id="name" name="name">
                
              </div>
            </div>
            <div class="form-group row">
              <label for="city" class="col-sm-3 col-form-label">City Name:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control city" id="city" name="city">
              </div>
            </div>
            <div class="form-group row">
              <label for="state" class="col-sm-3 col-form-label">State</label>
              <div class="col-sm-9">
                <select id="state" class="form-control state" name="state">
                  <option value="">Select State</option>
                  @forelse ($states as $state)
                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                  @empty
                  @endforelse
                </select>
              </div>
            </div>
            
            <div class="form-group row">
              <label for="address" class="col-sm-3 col-form-label">Address</label>
              <div class="col-sm-9">
                <input type="tel" class="form-control address" id="address" name="address">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary schooladd">Save changes</button>
        </div>
          </form>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    
    $('body').on('click', '.schooladd', function(event) {
      event.preventDefault();
       var self = $(this)
       var form = self.closest('form');
       $('.is-invalid').removeClass('is-invalid');

       $.ajax({
         url: form.attr('action'),
         type: 'POST',
         dataType: 'json',
         data: form.serialize(),
       })
       .done(function(data) {
        $('#school_id').append('<option value="'+data.id+'">'+data.name+'</option>')
        $('#exampleModalCenter').modal('hide')
       })
       .fail(function(data) {
          $.each(data.responseJSON.errors, function(index, val) {
             form.find('.' + index).addClass('is-invalid');
          });
       })
       .always(function() {
         
       });
       
    });
  });
</script>
@endsection
