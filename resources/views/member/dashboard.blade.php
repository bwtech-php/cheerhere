@extends('member.layouts.master')
@section('mainContent')

<div class="coach-form">
    <div class="container">
      <div class="row">
        @include('member.layouts.sidebar')
        <div class="col-9 naves-content">
          <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active coach-dashboard" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
              @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
              <h2>Member Dashboard</h2>
              <div class="row">
                @forelse ($clinics as $clinic)

                <div class="col-lg-3">
                  <div class="clinic-box">
                    <div class="img-box">
                      <img src="/assets/front/images/1.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="overlay">
                      <h4>{{ $clinic->school->name??  null }}</h4>
                      {{--  <h5>{{ assigned_waiver($clinic->assigned_waiver) }}</h5>  --}}
                      <h5><i  class="far fa-calendar-check"></i> {{ $clinic->pay_by_date }} <br><small class="ml-2"><i class="far fa-clock"></i> {{ $clinic->time }}</small></h5>
                      <a href="{{ route('clinic.detail', $clinic->id) }}" class="btn">Get Started<i class="fas fa-angle-right ml-2"></i></a>
                    </div>
                  </div>
                </div>
                @empty
                <p>We apologize, there are currently no clinics scheduled in your specified schools. Please check back regularly so that you can get updates about newly scheduled clinics.</p>
                @endforelse
               {{--  @forelse($enrolls as $clinic)
                <div class="col-lg-3">
                    <div class="clinic-box">
                      <div class="img-box">
                        <img src="/assets/front/images/1.jpg" class="img-fluid" alt="">
                      </div>
                      <div class="overlay">
                        <h4>{{ $clinic->clinic->school->name ?? null }}</h4>
                        <h5>{{ assigned_waiver($clinic->clinic->assigned_waiver) ?? null }}</h5>
                        <a href="{{ route('clinic.detail' , $clinic->clinic_id) }}" class="btn">View Clinic<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-2" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg><!-- <i class="fas fa-angle-right ml-2"></i> --></a>
                      </div>
                    </div>
                </div>
                @empty
                @endforelse  --}}

              </div>
              <div class="row mt-5">
                  <div class="col-lg-8">
                      <div id='calendar'></div>
                  </div>
              </div>

            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('style')
<link rel="stylesheet" href="/assets/front/css/calender/core_main.css" >
<link rel="stylesheet" href="/assets/front/css/calender/daygrid_main.css" >
<link rel="stylesheet" href="/assets/front/css/calender/timegrid_main.css" >
@endsection
@section('script')
<script src="/assets/front/js/calender/core_js.js" ></script>
<script src="/assets/front/js/calender/interaction_js.js" ></script>
<script src="/assets/front/js/calender/daygrid_js.js" ></script>
<script src="/assets/front/js/calender/timegrid_js.js" ></script>

@stop
