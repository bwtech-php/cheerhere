@extends('member.layouts.master')

@section('mainContent')
{{-- @dd($errors) --}}
      <div class="coach-form">
        <div class="container">
          <div class="row">
            <div class="col-3 naves">
              <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Login</a>
                <!--<a class="nav-link " href="/coach_profile" >Coach Profile</a>-->
                <!--<a class="nav-link " href="/schedule_clinic" >Schedule a Clinic</a>-->
                <!--<a class="nav-link " id="v-pills-Schedule-tab" data-toggle="pill" href="#v-pills-Schedule" role="tab" aria-controls="v-pills-Schedule" aria-selected="false">Schedule a Clinic</a>-->
                
              </div>
            </div>
            <div class="col-9 naves-content">
              <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active " id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                  <h2>Login</h2>
                  @if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <div class="login-page">
                     <div class="form">

                        <form class="login-form" method="post" action="{{ route('login') }}">

                          @csrf

                          <input type="email" placeholder="Email" id="email" name="email" style="text-transform:lowercase" class="form-control @error('email') is-invalid @enderror" />

                          @error('email')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                          @enderror

                          <input type="password" placeholder="password" id="password" style="text-transform:lowercase" name="password" class="form-control @error('password') is-invalid @enderror" />

                          @error('password')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                          @enderror

                          <button type="submit">login</button>

                          <p class="message">Forgot Your Password? <a href="{{ route('password.request') }}">Click Here</a></p>

                          <p class="message">Not registered? <a href="{{ route('member.register.form') }}">Create an account</a></p>

                        </form>

                      </div>
                    </div>
                  
                </div>
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
     

@endsection



@section('style')

@endsection



 