<div class="col-3 naves">

  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

    <a class="nav-link {{ Request::segment(2) == 'dashboard' ? 'active' : null }}" href="{{ route('member.dashboard') }}">Dashboard</a>

    <a class="nav-link {{ Request::segment(2) == 'profile' ? 'active' : null }}" href="{{ route('member.profile') }}" >Profile</a>

    {{-- <a class="nav-link " href="{{ route('member.schedule.clinic') }}" >Schedule a Clinic</a> --}}

    <!--<a class="nav-link " id="v-pills-Schedule-tab" data-toggle="pill" href="#v-pills-Schedule" role="tab" aria-controls="v-pills-Schedule" aria-selected="false">Schedule a Clinic</a>-->

    <a class="nav-link {{request()->url() == url('/member/enrolled/clinics') ? 'active' : null }}" href="{{ url('/member/enrolled/clinics') }}">Enrolled Clinic</a>

    <a class="nav-link {{request()->url() == url('/member/tickets') ? 'active' : null }}" href="{{ url('/tickets') }}">My tickets</a>

  </div>

</div>
