@extends('member.layouts.master')
@section('mainContent')
 {{-- @dd(Auth::user()) --}}
 {{-- @dd(Auth::user()->schools) --}}
 {{-- @dd($errors) --}}
      <div class="coach-form">
        <div class="container">
          <form action="{{ route('member.profile.update') }}" method="post" enctype="multipart/form-data">
            @csrf
              <div class="row">
           @include('member.layouts.sidebar')
            <div class="col-9 naves-content">
              <div class="tab-content" id="v-pills-tabContent">
                <!-- Profile -->
                <div class="tab-pane fade show active profile" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                  <h2>Update Your Profile</h2>
                  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <div class="row">
                    <div class="col-lg-8">
                      
                    <div class="form-group row">
                      <label for="inputname1"  class="col-sm-2 col-form-label">First Name:</label>
                      <div class="col-sm-10">
                        <input type="text" name="first_name" value="{{ Auth::user()->first_name }}" class="form-control @error('first_name') is-invalid @enderror" id="inputnam1">

                        @error('first_name')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Last Name:</label>
                      <div class="col-sm-10">
                        <input type="text" name="last_name" value="{{ Auth::user()->last_name }}" class="form-control" id="inputnam2">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Address:</label>
                      <div class="col-sm-10">
                        <input type="text" name="address" value="{{ Auth::user()->address }}" class="form-control" id="inputnam2">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="phonenumber" class="col-sm-2 col-form-label">Phone Number:</label>
                      <div class="col-sm-10">
                        <input type="text" name="phone" value="{{ Auth::user()->phone }}" class="form-control" id="phonenumber">
                      </div>
                    </div>
                    
                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">City Name:</label>
                      <div class="col-sm-10">
                        <input type="text" name="city" value="{{ Auth::user()->city }}" class="form-control" id="inputnam1">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="inputname1" class="col-sm-4 col-form-label">State</label>
                          <div class="col-sm-7 addnew2">
                            <select name="state" id="inputState" class="form-control" style="-webkit-appearance: none;">
                              <option value="">Select State</option>
                              @forelse ($states as $state)
                              <option value="{{ $state->id }}" {{ $state->id == Auth::user()->state ? 'selected'  : null }}>{{ $state->name }}</option>
                              @empty
                              @endforelse
                            </select>
                            
                          </div>
                          
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="inputname1" class="col-sm-4 col-form-label">Zip Code:</label>
                          <div class="col-sm-8">
                            <input type="text" name="zip" value="{{ Auth::user()->zip }}" class="form-control" id="inputnam1">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Date Of Birth:</label>
                      <div class="col-sm-10">
                        <input type="date" name="date_of_birth" value="{{ Auth::user()->date_of_birth }}" class="form-control @error('date_of_birth') is-invalid @enderror" id="inputEmail3">

                        @error('date_of_birth')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Graduation Year:</label>
                      <div class="col-sm-10">
                        {{-- <input type="date" onkeydown="return false" name="graduation_year" value="{{ Auth::user()->graduation_year }}" class="form-control" id="inputPassword3"> --}}
                        <select id="graduation_select" name="graduation_year" class="form-control">
                          <option disabled selected value="{{ Auth::user()->graduation_year }}">{{ Auth::user()->graduation_year }}</option>
                          <?php 
                            for($i = 1950 ; $i <= date('Y'); $i++){
                                print "<option>$i</option>";
                            }
                          ?>
                          </select>
                      </div>
                    </div>
                     <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Profile Picture</label>
                      <div class="col-sm-10">
                        <input type="file" name="profile"  class="form-control">
                      </div>
                    </div>
                    <div class="form-group row">
                          <label for="inputname1" class="col-sm-2 col-form-label">Shirt Size:</label>
                          <div class="col-sm-10 addnew2">
                            <select name="shirt_size" id="inputState" class="form-control" style="-webkit-appearance: none;">
                              <option value="Extra Small" {{ Auth::user()->shirt_size == 'Extra Small' ? 'selected' : null }}>Extra Small</option>
                              <option value="Small" {{ Auth::user()->shirt_size == 'Small' ? 'selected' : null }}>Small</option>
                              <option value="Medium" {{ Auth::user()->shirt_size == 'Medium' ? 'selected' : null }}>Medium</option>
                              <option value="Large" {{ Auth::user()->shirt_size == 'Large' ? 'selected' : null }}>Large</option>
                              <option value="Extra Large" {{ Auth::user()->shirt_size == 'Extra Large' ? 'selected' : null }}>Extra Large</option>
                              <option value="Extra Extra Large" {{ Auth::user()->shirt_size == 'Extra Extra Large' ? 'selected' : null }}>Extra Extra Large</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="youtubelinks">Please enter the links of your YouTube videos (1 link per line)</label>
                          <textarea name="youtubelinks" id="" cols="50" rows="5">{{ Auth::user()->youtubelinks }}</textarea> 
                        </div>
                    <div class="form-group">
                      <label for="inputPassword4" class="form-label">Select Colleges you want notifications from:</label>
                      <div class="col-sm-9">
                        <select class="js-example-basic-single" name="schools[]" multiple="multiple">
                          @forelse ($schools as $school)
                            <option value="{{ $school->id }}" {{ selectOptionSelect(Auth::user()->schools , $school->id) }}>{{ $school->name }}</option> 
                          @empty
                          @endforelse
                        </select>
                        {{-- <select id="multiple-checkboxess"  name="schools[]" multiple="multiple">
                        @forelse ($schools as $school)
                          <option value="{{ $school->id }}" {{ selectOptionSelect(Auth::user()->schools , $school->id) }}>{{ $school->name }}</option> 
                        @empty
                        @endforelse
                        </select> --}}
                      </div>
                    </div>
                    <!--<div class="form-group row">-->
                    <!--  <label for="exampleFormControlFile1" class="col-sm-3 col-form-label">Upload Photo</label>-->
                    <!--  <div class="col-sm-10">-->
                    <!--    <input type="file" class="form-control-file" id="exampleFormControlFile1">-->
                    <!--  </div>-->
                    <!--</div>-->
                    <div class="form-group row checkbox-in">
                      <div class="col-sm-12 checkbx">Do you participate in Competitive Cheer:</div>
                      <div class="col-sm-2 p-0">
                        {{-- <div class="form-check">
                          <input class="form-check-input" {{ Auth::user()->participate ==1 ? 'checked' : null }} type="radio" value="1" name="participate" id="gridCheck03">
                          <label class="form-check-label" for="gridCheck03">
                            Yes
                          </label>
                        </div> --}}
                        <div class="">
                          <input class="form-check-input" {{ Auth::user()->participate ==1 ? 'checked' : null }} type="radio" name="participate" id="gridCheck03" value="1">
                          <label class="form-check-label" for="gridCheck03">
                            Yes
                          </label>
                        </div>
                      </div>
                      <div class="col-sm-2 p-0">
                        {{-- <div class="form-check">
                          <input class="form-check-input" {{ Auth::user()->participate == 0 ? 'checked' : null }} type="radio" value="0" name="participate" id="gridCheck04">
                          <label class="form-check-label" for="gridCheck04">
                            No
                          </label>
                        </div> --}}
                        <div class="">
                          <input class="form-check-input" {{ Auth::user()->participate == 0 ? 'checked' : null }} type="radio" name="participate" id="gridCheck04" value="0">
                          <label class="form-check-label" for="gridCheck04">
                            No
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row sygmshow" style="display: {{ Auth::user()->participate == 0 ? 'none' : 'block' }}" >
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Home Gym:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" value="{{ Auth::user()->home_gym }}" name="home_gym" id="inputPassword3">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <button type="submit" class="btn btn-primary float-left">Update</button>
                      </div>
                    </div>
                  
                    </div>
                    <div class="col-lg-4">
                        <div class="img-box">
                            <img src="{{ image(Auth::user()->profile) }}" class="img-fluid">
                        </div>
                        <div class="skills">
                            <h3>Skills</h3>
                            <label for="">Select all that apply:</label>
                            <ul class="list-unstyled">
                              @forelse ($skills as $skill)
                                <li>
                                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="customControlInline{{ $skill->id }}" name="skills[]" {{ checkboxChecked(Auth::user()->skills , $skill->id) }} value="{{ $skill->id }}">
                                        <label class="custom-control-label" for="customControlInline{{ $skill->id }}">{{ $skill->title }}</label>
                                    </div>
                                </li>
                              @empty
                              @endforelse
                                 
                                 
                            </ul>
                        </div>
                        {{-- <div class="skills">
                          <iframe width="560" height="315" src="{{ Auth::user()->city . "&output=embed" }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> --}}
                    </div>
                  </div>
                </div>
                <!-- Profile -->
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
     
@stop

@section('script')
<script>
  $(document).ready(function($) {
    
    $('body').on('click', 'input[name=participate]', function( ) {
      
      if ($(this).val() == 1) {
        $('.sygmshow').show();
      }else{

        $('.sygmshow').hide();
      }
    });

    $('#graduation_select').select2();
    $('.js-example-basic-single').select2({
      closeOnSelect: false
    });

  });
</script>
@endsection