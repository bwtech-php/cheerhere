@extends('member.layouts.master')
@section('mainContent')
<div class="coach-form">
        <div class="container">
          <div class="row">
            @include('member.layouts.sidebar')
            <div class="col-9 naves-content">
              <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active coach-dashboard" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                  <h2>Coach Dashboard</h2>
                  <div class="row"> 
                   {{-- @forelse($clinics as $clinic) --}}
                    <div class="col-lg-3">
                        <div class="clinic-box">
                          <div class="img-box">
                            <img src="/assets/front/images/1.jpg" class="img-fluid" alt="">
                          </div>
                          <div class="overlay">
                            {{-- <h4>{{ $clinic->school->name ?? null }}</h4> --}}
                            <h4>testing </h4>
                            <h5>Contact Your State</h5>
                            <a href="#" class="btn">View Clinic<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-2" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg><!-- <i class="fas fa-angle-right ml-2"></i> --></a>
                          </div>
                        </div>
                    </div> 
                   {{--  @empty
                    @endforelse --}}
                  
                  </div>
                  <div class="row mt-5">
                      <div class="col-lg-8">
                          <div id='calendar'></div>
                      </div>
                  </div>
                  
                </div>
             
                
              </div>
            </div>
          </div>
        </div>
      </div>


@endsection

@section('style')
@endsection

