@extends('layouts.registermaster')

@section('mainContent')
<div class="coach-form">
        <div class="container">
          <div class="row">
            <div class="col-3 naves">
              <div class="nav flex-column nav-pills" id="v-pills-tab" >
                <a class="nav-link " id="v-pills-home-tab" href="{{ route('coach.register.form') }}" role="tab" aria-controls="v-pills-home" aria-selected="true">Coach Registration</a>
                <a class="nav-link active" id="v-pills-profile-tab" href="{{ route('member.register.form') }}"   aria-selected="true">Member Registration</a>

              </div>
            </div>
            <div class="col-9 naves-content">
                  <h2>Member Registration</h2>
                  <form action="{{ route('member.register.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="Member" name="type">
                    <div class="form-group row">
                      <label for="first_name" class="col-sm-2 col-form-label">First Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" name="first_name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="last_name" class="col-sm-2 col-form-label">Last Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" name="last_name">
                      </div>
                    </div>
                     <div class="form-group row">
                      <label for="email" class="col-sm-2 col-form-label">Email:</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email">
                      </div>
                    </div>
                     <div class="form-group row">
                      <label for="password" class="col-sm-2 col-form-label">Password:</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="address" class="col-sm-2 col-form-label">Address:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="city" class="col-sm-2 col-form-label">City Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('city') is-invalid @enderror" id="city" name="city">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="state" class="col-sm-4 col-form-label">State</label>
                          <div class="col-sm-7 addnew2">
                            <select id="state" name="state" class="form-control @error('state') is-invalid @enderror" style="-webkit-appearance: none;">
                              <option value="">Select State</option>
                              @forelse ($states as $state)
                                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                              @empty
                              @endforelse
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="zip" class="col-sm-4 col-form-label">Zip Code:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control @error('zip') is-invalid @enderror" id="zip" name="zip">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="date_of_birth" class="col-sm-2 col-form-label">Date Of Birth:</label>
                      <div class="col-sm-10">
                        <input onkeydown="return false" type="date" class="form-control @error('date_of_birth') is-invalid @enderror" id="date_of_birth" name="date_of_birth">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="graduation_year" class="col-sm-2 col-form-label">Graduation Year:</label>
                      <div class="col-sm-10">
                        <input onkeydown="return false" type="date" class="form-control @error('graduation_year') is-invalid @enderror" id="graduation_year" name="graduation_year">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-3 col-form-label">Select Colleges you want notifications from:</label>
                      <div class="col-sm-9">
                        <select id="multiple-checkboxes" name="schools[]" multiple="multiple">
                          @forelse ($schools as $school)
                            <option value="{{ $school->id }}">{{ $school->name }}</option>
                          @empty
                          @endforelse

                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleFormControlFile1" class="col-sm-3 col-form-label">Upload Photo</label>
                      <div class="col-sm-10">
                        <input type="file" class="form-control-file" name="profile" id="exampleFormControlFile1">
                      </div>
                    </div>
                    <div class="form-group">
                          <label for="youtubelinks">Please enter the links of your YouTube videos (1 link per line)</label>
                          <textarea name="youtubelinks" id="" cols="50" rows="5"></textarea> 
                        </div>
                    <div class="form-group row checkbox-in">
                      <div class="col-sm-12 checkbx">Do you participate in Competitive Cheer:</div>
                      <div class="col-sm-2 p-0">
                        <div class="form-check">
                          <input class="form-check-input" name="participate" type="radio" value="1" id="gridCheck01">
                          <label class="form-check-label" for="gridCheck01">
                            Yes
                          </label>
                        </div>
                      </div>
                      <div class="col-sm-2 p-0">
                        <div class="form-check">
                          <input class="form-check-input" name="participate" type="radio" value="0" id="gridCheck02">
                          <label class="form-check-label" for="gridCheck02">
                            No
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Home Gym:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control @error('home_gym') is-invalid @enderror" name="home_gym" id="inputPassword3">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn btn-primary">Reset</button>
                      </div>
                    </div>
                  </form>
            </div>
          </div>
        </div>
      </div>


@endsection
