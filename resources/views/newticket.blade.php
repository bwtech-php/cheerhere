@extends('layouts.master')
@section('mainContent')

{{-- @dd($__data) --}}

<section class="main-content inner-page">
  <div class="owl-carousel owl-theme">
    <div class="item">
      <img src="/assets/front/images/about-banner.jpg" alt="images not found">
      <div class="cover">
        <div class="container">
          <div class="header-content">
            <!-- <div class="line"></div> -->
            <h1>New Ticket</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Detail -->

<!-- Contact-form -->
<section class="main-form-sec contact-page">
  <div class="container">
    <a class="btn btn-primary mb-3" href="javascript:history.back()">Back</a>
    <div class="row">
      <div class="col-lg-12">
        <div class="form-in">
          <div class="content text-center">
            {{-- <h2 class="sec-heading">Please fill this form to create a new ticket</h2> --}}
            <p class="sec-para">Please fill this form to create a new ticket</p>
          </div>
          <form method="POST" action="{{ route('createTicket') }}">
            @csrf
            <div class="form-row">
              <div class="form-group col-md-12 col-lg-12">
                <label>Subject</label>
                <textarea name="title" cols="" rows="1" placeholder="Please enter your Subject"></textarea>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12 col-lg-12">
                <label>Description</label>
                <textarea name="description" id="" cols="" rows="5" placeholder="Tell Us How We Can Help"></textarea>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6 col-lg-6">
                <label>Category</label>
                <select class="form-control" name="category">
                  <option>Enrollment</option>
                  <option>Finance</option>
                  <option>Tech Support</option>
                </select>
              </div>
            </div>
            {{-- @if(Auth::user()->type != 'Coach')
            <div class="form-row">
            <div class="form-group col-md-6 col-lg-6">
                <div class="custom-control custom-checkbox">
                  <input name="hidden" type="checkbox" class="custom-control-input" value="1" id="customCheck1">
                  <label class="custom-control-label" for="customCheck1">Hide from coach?</label>
                </div>
              </div>
            </div>
            @endif --}}
            <div class="form-row">
            <div class="form-group col-md-6 col-lg-6">
                <label>Send to: </label>
                <select class="form-control" name="recipient_id">
                  @foreach($coaches as $coach)
                  <option value="{{ $coach->id }}">{{ $coach->first_name . ' ' . $coach->last_name . ' (' . $coach->type . ')' }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 text-center">
                <button type="submit" class="btn btn-more">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Contact-form -->
@endsection