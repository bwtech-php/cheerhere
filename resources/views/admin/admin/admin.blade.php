@extends('admin.layouts.master')
@section('title' , 'Admins')
{{-- @dd($__data) --}}
{{-- {{ dd(auth()->user()) }} --}}
@section('mainContent')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Admins</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Admins</h4>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row mb-2">
                                    <div class="col-lg-8">
                                       <h4 class="header-title">Admins</h4>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-lg-right">
                                            <a href="{{ route('newAdmin') }}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-basket mr-1"></i> Add New Admin</a>
                                        </div>
                                    </div><!-- end col-->
                                </div>

                                <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($admins as $admin)
                                          <tr>
                                            <td>{{ $admin->id }}</td>
                                            <td>{{ $admin->first_name }}</td>
                                            <td>{{ $admin->last_name }}</td>
                                            <td>{{ $admin->email }}</td>
                                            <td>{{ $admin->type }}</td>
                                            <td>
                                                @if($admin->type == 'Admin')
                                                <a href="{{ Route('editAdmin' , $admin->id) }}" class="btn btn-info waves-effect waves-light btn-xs" title="Edit details"><i class="mdi mdi-square-edit-outline"></i></a>
                                                @if(!Auth()->user()->type == 'Admin')
                                                <a onclick="return confirm('Are you sure?')" href="{{ Route('deleteAdmin' , $admin->id) }}" class="btn btn-danger waves-effect waves-light btn-xs" title="Delete"><i class="mdi mdi-close"></i></a>
                                                @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @empty
                                        @endforelse
                                    </tbody>
                                </table>

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->

            </div> <!-- container -->

        </div> <!-- content -->

       

    </div>


@stop

@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link href="/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
<script src="/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="/assets/app/indexMaster.js"></script>
@endsection