@extends('admin.layouts.master')
@section('title' , 'Coaches')

@section('mainContent')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Coaches</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Coaches</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row mb-2">
                                    <div class="col-lg-8">
                                       <h4 class="header-title">Coaches</h4>
                                       @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-lg-right">
                                            <a href="{{ route('coach.create') }}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-basket mr-1"></i> Add New Coach</a>
                                        </div>
                                    </div><!-- end col-->
                                </div>

                                <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($coachs as $coach)
                                          <tr>
                                            <td>{{ $coach->first_name }}</td>
                                            <td>{{ $coach->email }}</td>
                                            <td>{{ $coach->phone }}</td>
                                            <td>
                                                <a href="{{ Route('coach.edit' , $coach->id) }}" class="btn btn-info waves-effect waves-light btn-xs" title="Edit details"><i class="mdi mdi-square-edit-outline"></i></a>
                                                <a onclick="return confirm('Are you sure?')" href="{{ Route('coachDelete' , $coach->id) }}" class=" btn btn-danger waves-effect waves-light btn-xs"><i class="mdi mdi-close"></i></a>
                                            </td>
                                        </tr>
                                        @empty
                                        @endforelse
                                    </tbody>
                                </table>

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->

            </div> <!-- container -->

        </div> <!-- content -->

       

    </div>


@stop

@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link href="/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
<script src="/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="/assets/app/indexMaster.js"></script>
@endsection