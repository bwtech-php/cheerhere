@extends('admin.layouts.master')
@section('title' , 'Clinics')
{{-- @dd($__data) --}}
{{-- {{ dd(auth()->user()) }} --}}
@section('mainContent')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Clinics</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Clinics</h4>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row mb-2">
                                    <div class="col-lg-8">
                                       <h4 class="header-title">Clinics</h4>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-lg-right">
                                            {{-- <a href="{{ route('newAdmin') }}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-basket mr-1"></i> Add New Admin</a> --}}
                                        </div>
                                    </div><!-- end col-->
                                </div>

                                <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            {{-- <th>Open Age From</th>
                                            <th>Open Age To</th> --}}
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>In Time</th>
                                            <th>Cost</th>
                                            <th>Pay by Date</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($clinics as $clinic)
                                          <tr>
                                            <td>{{ $clinic->id }}</td>
                                            {{-- <td>{{ $clinic->open_age_from }}</td>
                                            <td>{{ $clinic->open_age_to }}</td> --}}
                                            <td>{{ $clinic->date }}</td>
                                            <td>{{ $clinic->time }}</td>
                                            <td>{{ $clinic->in_time }}</td>
                                            <td>{{ $clinic->cost }}</td>
                                            <td>{{ $clinic->pay_by_date }}</td>
                                            <td>{{ $clinic->created_at }}</td>
                                            {{-- <td>
                                                @if($clinic->type == 'Admin')
                                                <a href="{{ Route('editAdmin' , $clinic->id) }}" class="btn btn-info waves-effect waves-light btn-xs" title="Edit details"><i class="mdi mdi-square-edit-outline"></i></a>
                                                @if(!Auth()->user()->type == 'Admin')
                                                <a onclick="return confirm('Are you sure?')" href="{{ Route('deleteAdmin' , $clinic->id) }}" class="btn btn-danger waves-effect waves-light btn-xs" title="Delete"><i class="mdi mdi-close"></i></a>
                                                @endif
                                                @endif
                                            </td> --}}
                                        </tr>
                                        @empty
                                        @endforelse
                                    </tbody>
                                </table>

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->

            </div> <!-- container -->

        </div> <!-- content -->

       

    </div>


@stop

@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link href="/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
<script src="/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="/assets/app/indexMaster.js"></script>
@endsection