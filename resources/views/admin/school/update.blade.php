@extends('admin.layouts.master')

@section('title' , 'Create / update School')

@section('mainContent')
{{-- @dd($__data) --}}

@php
    if (isset($row->id)) {
       $action = Route('admin.school.update' , $row->id);
    }else{
        $action = Route('admin.school.store');
    }
@endphp

   <div class="content-page">
<div class="content">
    <!-- Start Content-->
    <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.school.index') }}">Schools</a></li>
                                <li class="breadcrumb-item active">Create / Update School</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create / Update School</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Fill Out Your School Information</h4>

                        <hr>

                        <div class="clearfix"></div>
                        <div class="row">
                    		<div class="col-12 text-center">
                    			<div class="form-group">
                        			<h3>General Information</h3>
                    			</div>
		                    </div>
		                </div>

                        <form  method="post" action="{{ $action }}" id="eventForm" enctype="multipart/form-data" >

					    @csrf
                        @isset ($row->id)
                            @method('PUT')
                        @endisset
                        	<div class="row">
                    			<div class="col-8 m-auto">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="name" class="col-form-label">Name</label>
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" required value="{{ isset($row->name)? $row->name : old('name') }}">
                                             @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="state" class="col-form-label">State</label>
                                            {{-- <input type="text" name="state" id="state" class="form-control @error('state') is-invalid @enderror" required value="{{ isset($row->state)? $row->state : old('state') }}"> --}}
                                            <select class="form-control" name="state">
                                                @if(isset($row->state))
                                                <option value="{{ $selectedState->id }}" selected>{{ $selectedState->name }}</option>
                                                <br>
                                                @foreach($states as $s)
                                                <option value="{{ $s->id }}">{{ $s->name }}</option>
                                                @endforeach
                                                @else
                                                @foreach($states as $s)
                                                <option value="{{ $s->id }}">{{ $s->name }}</option>
                                                @endforeach
                                                @endif
                                              </select>
                                             @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="city" class="col-form-label">City</label>
                                            <input type="text" name="city" id="city" class="form-control @error('city') is-invalid @enderror" required value="{{ isset($row->city)? $row->city : old('city') }}">
                                             @error('city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="address" class="col-form-label">Address</label>
                                            <textarea name="address" id="address" class="form-control @error('address') is-invalid @enderror" cols="30" rows="5">{{ isset($row->address)? $row->address : old('address') }}</textarea>

                                             @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <hr>

                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox">
                                             <a href="{{ route('admin.team.index') }}" class="btn btn-secondary  waves-effect waves-light">Cancel</a>
                                             <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>


								</div> <!-- end card-body -->
                        	</div>
                        </form>
                	</div>
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->


    </div> <!-- container -->

    </div> <!-- content -->


@stop

@section('top-style')
@stop

@section('style')

@endsection

@section('script')
@endsection