@extends('admin.layouts.master')
@section('title' , 'Dashboard')

@section('mainContent')

<div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">

                                    </div>
                                    <h4 class="page-title">Dashboard</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col-md-6 col-xl-4">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                                <i class="fe-heart font-22 avatar-title text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1">$<span data-plugin="counterup">{{ $totalRevenue ?? null }}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Total Revenue</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </div> <!-- end col-->

                            <div class="col-md-6 col-xl-4">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                                                <i class="fe-shopping-cart font-22 avatar-title text-success"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1">$<span data-plugin="counterup">{{ $todaysRevenue ?? null }}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Today's Revenue</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </div> <!-- end col-->

                           

                            <div class="col-md-6 col-xl-4">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
                                                <i class="fe-eye font-22 avatar-title text-warning"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                @if($totalClinics != null)
                                                <a href="{{ route('clinics') }}"><h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $totalClinics ?? null }}</span><i class="fas fa-arrow-right ml-2"></i></h3></a>
                                                @else
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $totalClinics ?? null }}</span></h3>
                                                @endif
                                                <p class="text-muted mb-1 text-truncate">Total Clinics</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end row-->

                        <div class="row">
                          {{--   <div class="col-xl-4">
                                <div class="card-box">
                                    <h4 class="header-title mb-3">Total Revenue</h4>

                                    <div class="widget-chart text-center" dir="ltr">
                                        <input data-plugin="knob" data-width="160" data-height="160" data-linecap=round data-fgColor="#f1556c" value="60" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".12"/>
                                        <h5 class="text-muted mt-3">Total sales made today</h5>
                                        <h2>$178</h2>

                                        <p class="text-muted w-75 mx-auto sp-line-2">Traditional heading elements are designed to work best in the meat of your page content.</p>

                                        <div class="row mt-3">
                                            <div class="col-4">
                                                <p class="text-muted font-15 mb-1 text-truncate">Target</p>
                                                <h4><i class="fe-arrow-down text-danger mr-1"></i>$7.8k</h4>
                                            </div>
                                            <div class="col-4">
                                                <p class="text-muted font-15 mb-1 text-truncate">Last week</p>
                                                <h4><i class="fe-arrow-up text-success mr-1"></i>$1.4k</h4>
                                            </div>
                                            <div class="col-4">
                                                <p class="text-muted font-15 mb-1 text-truncate">Last Month</p>
                                                <h4><i class="fe-arrow-down text-danger mr-1"></i>$15k</h4>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div> --}}

                            <div class="col-xl-12">
                                <div class="card-box">
                                    <h4 class="header-title mb-3">Today's Enrollments</h4>

                                    <div class="table-responsive">
                                        <table class="table table-borderless table-hover table-centered m-0">

                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Member Name</th>
                                                    <th>Clinic Name</th>
                                                    <th>Clinic Owner</th>
                                                    <th>Clinic Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($todaysEnrollments as $data)
                                                <tr>
                                                    <td>{{ $data->first_name . " " . $data->last_name }}</td>
                                                    <td>{{ $data->name }}</td>
                                                    <td>{{ $data->clinic_first_name . " " . $data->clinic_last_name }}</td>
                                                    <td>${{ $data->cost }}</td>
                                                    <td><a class="btn btn-primary" href="{{ route('clinic.enrolled.detail', $data->clinic_id) }}">View Clinic</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div> <!-- end .table-responsive-->
                                </div> <!-- end card-box-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end row -->


                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

             

            </div>


@stop

@section('style')
@endsection

@section('script')

        <!-- Plugins js-->
        <script src="/assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="/assets/libs/jquery-knob/jquery.knob.min.js"></script>
        <script src="/assets/libs/jquery-sparkline/jquery.sparkline.min.js"></script>
        <script src="/assets/libs/flot-charts/jquery.flot.js"></script>
        <script src="/assets/libs/flot-charts/jquery.flot.time.js"></script>
        <script src="/assets/libs/flot-charts/jquery.flot.tooltip.min.js"></script>
        <script src="/assets/libs/flot-charts/jquery.flot.selection.js"></script>
        <script src="/assets/libs/flot-charts/jquery.flot.crosshair.js"></script>

        <!-- Dashboar 1 init js-->
        <script src="/assets/js/pages/dashboard-1.init.js"></script>
@endsection