@extends('admin.layouts.master')
@section('title' , 'Skills')

@section('mainContent')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Skills</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Skills</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row mb-2">
                                    <div class="col-lg-8">
                                       <h4 class="header-title">Skills</h4>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-lg-right">
                                            <a href="{{ route('admin.skill.create') }}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-basket mr-1"></i> Add New Skill</a>
                                        </div>
                                    </div><!-- end col-->
                                </div>

                                <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($skills as $skill)
                                          <tr>
                                            <td>{{ $skill->title }}</td>
                                            <td>
                                                <a href="{{ Route('admin.skill.edit' , $skill->id) }}" class="btn btn-info waves-effect waves-light btn-xs" title="Edit details"><i class="mdi mdi-square-edit-outline"></i></a>
                                                <a href="{{ Route('admin.skill.destroy' , $skill->id) }}" class="singleDelete btn btn-danger waves-effect waves-light btn-xs" title="Delete"><i class="mdi mdi-close"></i></a>
                                            </td>
                                        </tr>
                                        @empty
                                        @endforelse
                                    </tbody>
                                </table>

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->

            </div> <!-- container -->

        </div> <!-- content -->

       

    </div>


@stop

@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link href="/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
<script src="/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="/assets/app/indexMaster.js"></script>
@endsection