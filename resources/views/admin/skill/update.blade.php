@extends('admin.layouts.master')

@section('title' , 'Create / update Skill')

@section('mainContent')

@php
    if (isset($row->id)) {
       $action = Route('admin.skill.update' , $row->id);
    }else{
        $action = Route('admin.skill.store');
    }
@endphp

   <div class="content-page">
<div class="content">
    <!-- Start Content-->
    <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.skill.index') }}">Teams</a></li>
                                <li class="breadcrumb-item active">Create / Update Skill</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create / Update Skill</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Fill Out Your Skill Information</h4>

                        <hr>

                        <div class="clearfix"></div>
                        <div class="row">
                    		<div class="col-12 text-center">
                    			<div class="form-group">
                        			<h3>General Information</h3>
                    			</div>
		                    </div>
		                </div>

                        <form  method="post" action="{{ $action }}" id="eventForm" enctype="multipart/form-data" >

					    @csrf
                        @isset ($row->id)
                            @method('PUT')
                        @endisset
                        	<div class="row">
                    			<div class="col-8 m-auto">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="title" class="col-form-label">Title</label>
                                            <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" required value="{{ isset($row->title)? $row->title : old('title') }}">
                                             @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="description" class="col-form-label">Description</label>
                                            <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" cols="30" rows="10">{{ isset($row->description)? $row->description : old('description') }}</textarea>

                                             @error('description')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <hr>

                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox">
                                             <a href="{{ route('admin.skill.index') }}" class="btn btn-secondary  waves-effect waves-light">Cancel</a>
                                             <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>


								</div> <!-- end card-body -->
                        	</div>
                        </form>
                	</div>
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->


    </div> <!-- container -->

    </div> <!-- content -->


@stop

@section('top-style')
@stop

@section('style')

@endsection

@section('script')
@endsection