@extends('admin.layouts.master')

@section('title' , 'Create / update Member')

@section('mainContent')

@php
    if (isset($row->id)) {
       $action = Route('member.update' , $row->id);
    }else{
        $action = Route('member.store');
    }
@endphp

   <div class="content-page">
<div class="content">
    <!-- Start Content-->
    <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('member.index') }}">Coaches</a></li>
                                <li class="breadcrumb-item active">Create / Update Member</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create / Update Member</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Fill Out Your Coach Information</h4>

                        <hr>

                        <div class="clearfix"></div>
                        <div class="row">
                    		<div class="col-12 text-center">
                    			<div class="form-group">
                        			<h3>General Information</h3>
                    			</div>
		                    </div>
		                </div>

                        <form  method="post" action="{{ $action }}" id="eventForm" enctype="multipart/form-data" >

					    @csrf
                        @isset ($row->id)
                            @method('PUT')
                        @endisset
                        	<div class="row">
                    			<div class="col-12 m-auto">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="first_name" class="col-form-label">First Name</label>
                                            <input type="text" name="first_name" id="first_name" class="form-control @error('first_name') is-invalid @enderror" required value="{{ isset($row->first_name)? $row->first_name : old('first_name') }}">
                                             @error('first_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="last_name" class="col-form-label">Last Name</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control @error('last_name') is-invalid @enderror" required value="{{ isset($row->last_name)? $row->last_name : old('last_name') }}">
                                             @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="first_name" class="col-form-label">School</label>
                                            <a href="{{ route('admin.school.create') }}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-basket mr-1"></i> Add New School</a>
                                            <select name="school_id" id="school_id" class="form-control @error('school_id') @enderror">
                                                <option value="">Select School</option>
                                                @forelse ($schools as $school)
                                                <option value="{{ $school->id }}" {{ isset($row->school_id) ? $row->school_id == $school->id ? 'selected' : null : null }}>{{ $school->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                             @error('school_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            {{-- <select name="schools[]" id="school_id" multiple class="form-control @error('school_id') @enderror">
                                                <option value="">Select School</option>
                                                @forelse ($schools as $school)
                                                <option value="{{ $school->id }}"  @isset($row->id) @forelse ($row->schools as $schl)
                                                    @if ($school->id == $schl->id )
                                                    selected
                                                    @endif
                                                @empty
                                                    
                                                @endforelse  @endisset>{{ $school->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                             @error('school_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror --}}
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="phone" class="col-form-label">Phone</label>
                                            <input type="number" name="phone" id="phone" class="form-control @error('phone') is-invalid @enderror" required value="{{ isset($row->phone)? $row->phone : old('phone') }}">
                                             @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="city" class="col-form-label">City</label>
                                            <input type="text" name="city" id="city" class="form-control @error('city') is-invalid @enderror" required value="{{ isset($row->city)? $row->city : old('city') }}">
                                             @error('city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="zip" class="col-form-label">Zip</label>
                                            <input type="text" name="zip" id="zip" class="form-control @error('zip') is-invalid @enderror" required value="{{ isset($row->zip)? $row->zip : old('zip') }}">
                                             @error('zip')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="state" class="col-form-label">State</label>
                                            <select name="state" id="state" class="form-control @error('state') @enderror">
                                                <option value="">Select State</option>
                                                @forelse ($states as $state)
                                                <option value="{{ $state->id }}" {{ isset($row->state) ? $row->state == $state->id ? 'selected' : null : null }}>{{ $state->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                             @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="address" class="col-form-label">Address</label>
                                            <input type="text" name="address" id="address" class="form-control @error('address') is-invalid @enderror"  value="{{ isset($row->address)? $row->address : old('address') }}">
                                             @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="email" class="col-form-label">Email</label>
                                            <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror"  value="{{ isset($row->email)? $row->email : old('email') }}">
                                             @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="password" class="col-form-label">Password</label>
                                            <input type="text" name="password" id="password" class="form-control @error('password') is-invalid @enderror" >
                                             @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div> 

                                    <hr>

                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox">
                                             <a href="{{ route('member.index') }}" class="btn btn-secondary  waves-effect waves-light">Cancel</a>
                                             <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>


								</div> <!-- end card-body -->
                        	</div>
                        </form>
                	</div>
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->


    </div> <!-- container -->

    </div> <!-- content -->


@stop

@section('top-style')
<link href="/assets/libs/select2/select2.min.css" rel="stylesheet" type="text/css" />
@stop

@section('style')
@endsection

@section('script')
<script src="/assets/libs/select2/select2.min.js"></script>
<script>
    // $(document).ready(function() {
    //     $('select').select2();
    // });
</script>
@endsection