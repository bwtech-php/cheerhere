@extends('layouts.master')
@section('mainContent')
<section class="main-content inner-page">
  <div class="owl-carousel owl-theme">
    <div class="item">
      <img src="/assets/front/images/about-banner.jpg" alt="images not found">
      <div class="cover">
        <div class="container">
          <div class="header-content">
            <!-- <div class="line"></div> -->
            <h1>about us</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Detail -->
<!-- About -->
<div class="main-about-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="content">
          <h2 class="sec-heading">About Cheer Here USA</h2>
          <p class="sec-para">Cheer Here USA was created because we saw a need and knew how to fill it.
            When my daughter, who had competed in competition cheer and high school
            cheer, was looking for the right college the search went beyond academic
            options. She wanted to continue to be involved in cheer and the up and
          coming sport of Stunt.</p>
          <p class="sec-para">She wanted to stay involved and create her new group as she went off away from all her friends. We found the best way to do this was to attend college cheer clinics. This allowed her to do campus tours at the same time and meet the teams to see where she felt the best connection.
          </p>
          <p class="sec-para">So the search began and I quickly found there was no simple way to track when clinics were available. I got on all social media platforms, followed all of the colleges she was interested in. This required daily checking for postings in order to register and schedule. Especially important because we were traveling across states to visit these schools. </p>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="img-box border-over">
          <img src="/assets/front/images/aboutpage.jpg" class="img-fluid" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- About -->
<!-- Vision -->
<div class="main-vision-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="content">
          <h2 class="sec-heading">Our Vision</h2>
          <p class="sec-para">Our vision was to simplify this.  Athletes can join and save all of the schools you are interested in to your own profile.  When your selected colleges release a clinic you will instantly be notified.  Because colleges use these clinics as both a fundraiser for their current team and a recruiting event, our site gives athletes and coaches one simple place to connect.</p>
          <p class="sec-para semi-bold">We hope that you find the perfect place to call home for your college career just as my daughter has and we hope that our platform gives you all the tools you need to get there!
          </p>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row vision-pics">
          <div class="col-lg-4">
            <div class="img-box border-over">
              <img src="/assets/front/images/vision-1.jpg" class="img-fluid" alt="">
            </div>
          </div>
          <div class="col-lg-4">
            <div class="img-box border-over">
              <img src="/assets/front/images/vision-2.jpg" class="img-fluid" alt="">
            </div>
          </div>
          <div class="col-lg-4">
            <div class="img-box border-over">
              <img src="/assets/front/images/vision-3.jpg" class="img-fluid" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Vision -->
@endsection
