@extends('layouts.master')
@section('mainContent')
<section class="main-content inner-page">
  <div class="owl-carousel owl-theme">
    <div class="item">
      <img src="/assets/front/images/about-banner.jpg" alt="images not found">
      <div class="cover">
        <div class="container">
          <div class="header-content">
            <!-- <div class="line"></div> -->
            <h1>contact us</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Detail -->

<!-- Contact-form -->
<section class="main-form-sec contact-page">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="form-in">
          <div class="content text-center">
            <!-- <h2 class="sec-heading">Sign Up For Updates!</h2>
            <p class="sec-para">We are experts in this industry with over 100 years experience. What that means is you are going to get right solution. please find our services.</p> -->
          </div>
          <form>
            <div class="form-row">
              <div class="form-group col-md-6 col-lg-6">
                <input type="text" class="form-control" id="inputEmail4" placeholder="First Name">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <input type="text" class="form-control" id="inputEmail4" placeholder="Last Name">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <input type="text" class="form-control" id="inputEmail4" placeholder="Email">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <input type="text" class="form-control" id="inputEmail4" placeholder="Phone">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12 col-lg-12">
                <textarea name="" id="" cols="" rows="10" placeholder="Tell Us How We Can Help"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 text-center">
                <a href="#" class="btn btn-more">Submit</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Contact-form -->
@endsection