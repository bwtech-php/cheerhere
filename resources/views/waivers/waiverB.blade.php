<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
{{-- @dd($__data) --}}
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title></title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container mt-4">

        <div class="row "  style="margin-top: 80px;">
            <div class="col-md-3">
                <img src="{{ url('/assets/front/images/print_img.jpg') }}" alt="" srcset="" style="width: 200px;">
            </div>
            <div class="col-md-5 ">
                <h5>Oklahoma State University
                    Sports Medicine & Athletic Training
                    170 Athletics Center Stillwater, OK
                    74078 Participation
                    Release/Waiver For PSA</h5>
            </div>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-12">
                <div  class="row">

                    <div class="col-md-4">
                        {{-- <strong>Name:</strong>  <strong><u>{{ $waiver_data->name }}</u></strong> --}}
                        <strong>Name:</strong>  <strong><u></u></strong>
                    </div>
                    <div class="col-md-4">
                    {{-- <strong>Sport:</strong> <strong><u>{{ $waiver_data->sport }}</u></strong> --}}
                    <strong>Sport:</strong> <strong><u></u></strong>
                    </div>
                    <div class="col-md-4">
                    {{-- <strong >DOB:</strong> <strong><u>{{ $waiver_data->dob }}</u></strong> --}}
                    <strong >DOB:</strong> <strong><u></u></strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 40px;">
            <h6>Catastrophic Injury, Illness & Assumption
                of Risk</h6>
                <div class="col-md-12">
                    <span>The possibility of sustaining a catastrophic injury that could lead to permanent disability or even death is
                        inherent in any athletic activity. I am assuming the risk that without a proper examination and physical, an
                        underlying health condition may go undetected. Therefore, the possibility exists that participation in any sports
                        program can result in serious unforeseeable medical health problems. With this information, I understand the
                        importance of rules and procedures as well as proper technique and that the possibility of a catastrophic
                        injury or death does exist even when followed to the fullest and I assume all risk and all liability for any
                        illnesses, injuries or medical conditions that may occur during my participation in any OSU Athletic activity.
                        </span>
                        <div class="col-md-12  mt-2">



                                    <div class="col-md-3">
                            {{-- <span>Date:<u><span class="">{{ $waiver_data->date_1 }}</span></u></span> --}}
                            <span>Date:<u><span class=""></span></u></span>
                                    </div>
                                    <div class="col-md-4 ">

                                            <span >Signature:</span>
                                            {{-- <img  src="{{ url('/img/waiver/'.$waiver_data->sign_1) }}" alt="" style="width:90px; height:60px;"> --}}
                                            <img  src="" alt="" style="width:90px; height:60px;">
                                        </div>





                                <div class="col-md-5">
                             <span >Parent/Guardian signature if under age 18:</span>



                            {{-- <img src="{{ url('/img/waiver/'.$waiver_data->p_sign_1) }}" alt="" style="width:90px; height:60px;"> --}}
                            <img src="" alt="" style="width:90px; height:60px;">

                                </div>
                        </div>
                </div>

            <div class="col-md-12">
            <h6>
                Release of Liability
            </h6>
            <span>
                Until I am officially on the roster as an Oklahoma State University intercollegiate-level student-athlete and I
have had a pre-participation physical and have been cleared by an OSU Team Physician, I understand that
by signing below, I certify that if I suffer any injury, illness or health related condition athletic related or not, I
release Oklahoma State University of any liability, responsibility, financially or otherwise. If I have medical
insurance, I agree to supply OSU with a front and back copy of my insurance card for emergency purposes.

            </span>
            <div class="col-md-12  mt-2">



                <div class="col-md-3">
        {{-- <span>Date:<u><span class="">{{ $waiver_data->date_2 }}</span></u></span> --}}
        <span>Date:<u><span class=""></span></u></span>
                </div>
                <div class="col-md-4 ">

                        <span >Signature:</span>
                        {{-- <img  src="{{ url('/img/waiver/'.$waiver_data->sign_2) }}" alt="" style="width:90px; height:60px;"> --}}
                        <img  src="" alt="" style="width:90px; height:60px;">
                    </div>

            <div class="col-md-5">
         <span >Parent/Guardian signature if under age 18:</span>



        {{-- <img src="{{ url('/img/waiver/'.$waiver_data->p_sign_2) }}" alt="" style="width:90px; height:60px;"> --}}
        <img src="" alt="" style="width:90px; height:60px;">

            </div>
    </div>
            </div>
            <div class="col-md-12">
                <h6>
                    Consent to
Treat

                </h6>
                <span>
                    I give authorization to the staff Athletic Trainer and/or Team Physicians to evaluate and treat any injuries,
illnesses, or medical conditions that occur during my athletic participation/tryout at Oklahoma State University
(this includes immediate first aid, medication, treatment, x-ray and physical exam). I understand the Team
Physician(s) have the authority to eliminate me from further participation due to an injury, illness or other
medical condition and/or the undue liability risk of Oklahoma State University.


                </span>
                <div class="col-md-12  mt-2">



                    <div class="col-md-3">
            {{-- <span>Date:<u><span class="">{{ $waiver_data->date_3 }}</span></u></span> --}}
            <span>Date:<u><span class=""></span></u></span>
                    </div>
                    <div class="col-md-4 ">

                            <span >Signature:</span>
                            {{-- <img  src="{{ url('/img/waiver/'.$waiver_data->sign_3) }}" alt="" style="width:90px; height:60px;"> --}}
                            <img  src="" alt="" style="width:90px; height:60px;">
                        </div>





                <div class="col-md-5">
             <span >Parent/Guardian signature if under age 18:</span>



            {{-- <img src="{{ url('/img/waiver/'.$waiver_data->p_sign_3) }}" alt="" style="width:90px; height:60px;"> --}}
            <img src="" alt="" style="width:90px; height:60px;">

                </div>
        </div>
                </div>

        </div>
    </div>

    <script type="text/javascript">
        window.onload = function() { window.print(); }
   </script>
</body>
</html>
