<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
{{-- @dd($__data) --}}
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--  <title>{{ config('app.name', 'Laravel') }}</title>  --}}

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container mt-4">

        <div class="row "  style="margin-top: 20px;">
            <div class="col-md-3">
                <span>2020 COLLEGE CHEERLEADING AND
                    DANCE TEAM NATIONAL CHAMPIONSHIP
                    Minor Release / Waiver Form</span>
            </div>
            <div class="col-md-5 ">
                <h3>RELEASE/WAIVER FORM</h3>
                <span class="text-md-center">Please mail ALL copies
                    Do Not Staple </span>
            </div>
            <div class="col-md-3">
                <h5>Organization / Team Name</h5>
                {{-- <span>{{ $waiver_data->team_name_1 }}</span> --}}
                <span>{{ isset($waiver_data->team_name_1) ? $waiver_data->team_name_1 : "" }}</span>
            </div>
        </div>
        <div class="row" style="margin-top:30px;">
            <div class="col-md-3">
                <span> <strong class="mr-2"> Minor’s Name:</strong>{{ isset($waiver_data->minor_name_1) ? $waiver_data->minor_name_1 : "" }}</span>
            </div>

        </div>
        <div class="row">
            <div class="col-md-5">
                <span> <strong class=""> Address: </strong> {{ isset($waiver_data->address_1) ? $waiver_data->address_1 : "" }}</span>
            </div>
            <div class="col-md-2">
                <span><strong class="">City: </strong>{{ isset($waiver_data->city) ? $waiver_data->city : "" }}</span>
            </div>
            <div class="col-md-2">
                <span><strong class="">ST: </strong>{{ isset($waiver_data->state) ? $waiver_data->state : "" }}</span>
            </div>
            <div class="col-md-2">
                <span><strong class="">Zip:</strong>{{ isset($waiver_data->zip) ? $waiver_data->zip : "" }}</span>
            </div>

            <div class="col-md-4">
                <span><strong class="">Phone: </strong>{{ isset($waiver_data->phone) ? $waiver_data->phone : "" }}</span>
            </div>
            <div class="col-md-4">
                <span><strong class="">Email:
                    </strong> {{ isset($waiver_data->email) ? $waiver_data->email : "" }}</span>
            </div>
        </div>
        <div class="row mt-4">

            <small>As used below, “Varsity” shall mean Varsity Spirit LLC and their subsidiary and other affiliated companies, and the officers, directors, employees, agents, successors and assigns
                of each of the foregoing; and “Disney” shall mean Disney Destinations, LLC, Walt Disney Parks and Resorts U.S., Inc., and their respective parent, subsidiary and other affiliated
                or related companies, and the officers, directors, employees, agents, successors and assigns of each of the foregoing.</small>

        </div>
        <div class="row justify-content-center mt-3">
            <h5>TERMS AND CONDITIONS OF PARTICIPATION - READ CAREFULLY BEFORE SIGNING</h5>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>In consideration of my minor child or ward’s participation in the cheerleading, dance or other activities conducted by Varsity at the Walt Disney World ® Resort on or about
                    January 15-21, 2020 pursuant to the 2020 College Cheerleading and Dance Team National Championship (the “Event”), wherever the Event and/or activities may occur, you hereby
                    attest that, after reading this Form completely and carefully, including the notice above your signature, as required by Florida Statutes 744.301, you acknowledge that participation
                    in the Event by your minor child or ward is entirely voluntary, and that you understand and agree as follows:</span>
            </div>
            <div class="col-md-12 mt-1">
                <span><b class="mr-2">RELEASE OF LIABILITY:</b>  I agree, on behalf of my child or ward, to waive and release all liabilities, claims, actions, damages, costs or expenses of any nature (“Claims”) associated
                    with all risks that are inherent to his or her participation in the Event or other activities conducted in conjunction there with (which risks may include, among other things, exposure to
                    Naegliria Fowlerii and coliform bacteria, muscle injuries, heat and stress related issues, cuts, lacerations and broken bones), whether such risks are open and obvious or otherwise.
                    Further on behalf of myself and my minor child or ward, I hereby release, covenant not to sue, and forever discharge the Released Parties (as defined under “INDEMNITY/
                    INSURANCE” below) of and from all Claims arising in any manner out of or in anyway connected with my child’s or ward’s participation in the Event.</span>
            </div>
            <div class="col-md-12 mt-1">
                <span><b class="mr-2">INDEMNITY/INSURANCE:</b>  I agree to indemnify and hold each of Disney Destinations, LLC, Walt Disney Parks and Resorts U.S., Inc., ESPN, Inc. and each of their respective
                    parent, subsidiary and other affiliated or related companies; Varsity Spirit, LLC, all Event sponsors and charities having a presence at the Event and their respective parent,
                    subsidiary and other affiliated or related companies; Reedy Creek Improvement District and its Board of Supervisors; and the officers, directors, employees, agents, contractors,
                    subcontractors, representatives, successors, assigns, and volunteers of each of the foregoing entities (collectively, the “Released Parties”) harmless from and against any and all
                    Claims arising out of or in anyway connected with my child’s or ward’s participation in the Event, wherever the Event may occur, including, but not limited to, all attorneys’ fees
                    and disbursements through and including any appeal. I understand and agree that this indemnity includes any Claims based on the negligence, action or inaction of any of the
                    Released Parties and covers bodily injury (including death), property damage, and loss by theft or otherwise, whether suffered by me or my child or ward either before, during
                    or after participation in the Event. I agree that I am not relying on the Released Parties to have arranged for, or carry, any insurance of any kind for my benefit or that of my child
                    or ward relative to my child’s or ward’s participation in the activities and the Event, and that I am solely responsible for obtaining any mandatory or desired life, travel, accident,
                    property, or other insurance related to my child’s or ward’s participation in the Event, at my own expense.</span>
            </div>

            <div class="col-md-12 mt-1">
                <span><b class="mr-2">PHYSICAL CONDITION/MEDICAL AUTHORIZATION:</b>  I agree to indemnify and hold each of Disney Destinations, LLC, Walt Disney Parks and Resorts U.S., Inc., ESPN, Inc. and each of their respective
                    parent, subsidiary and other affiliated or related companies; Varsity Spirit, LLC, all Event sponsors and charities having a presence at the Event and their respective parent,
                    subsidiary and other affiliated or related companies; Reedy Creek Improvement District and its Board of Supervisors; and the officers, directors, employees, agents, contractors,
                    subcontractors, representatives, successors, assigns, and volunteers of each of the foregoing entities (collectively, the “Released Parties”) harmless from and against any and all
                    Claims arising out of or in anyway connected with my child’s or ward’s participation in the Event, wherever the Event may occur, including, but not limited to, all attorneys’ fees
                    and disbursements through and including any appeal. I understand and agree that this indemnity includes any Claims based on the negligence, action or inaction of any of the
                    Released Parties and covers bodily injury (including death), property damage, and loss by theft or otherwise, whether suffered by me or my child or ward either before, during
                    or after participation in the Event. I agree that I am not relying on the Released Parties to have arranged for, or carry, any insurance of any kind for my benefit or that of my child
                    or ward relative to my child’s or ward’s participation in the activities and the Event, and that I am solely responsible for obtaining any mandatory or desired life, travel, accident,
                    property, or other insurance related to my child’s or ward’s participation in the Event, at my own expense.</span>
            </div>
            <div class="col-md-12 mt-1">
                <span><b class="mr-2">EQUIPMENT AND FACILITIES INSPECTION: </b>  I, or my child or ward if I am not in attendance at the Event, will immediately advise the Event manager of any unsafe condition that
                    I, or my child or ward if I am not in attendance at the Event, observe. My child or ward will refuse to participate, and I will refuse to let my child or ward participate, in the Event
                    until all unsafe conditions observed by me, or my child or ward, have been remedied.</span>
            </div>
            <br>
            <br>

        </div>
        <hr>
        <div class="row" style="margin-top: 100px;">
            <div class="col-md-12 ">
                <span><b class="mr-2">PUBLICITY RIGHTS:  </b>  I further grant the Released Parties the right to photograph, record and/or videotape me and my child or ward and further to display, edit, use and/or
                    otherwise exploit my or my child’s or ward’s name, face, likeness, voice, and appearance, in all media, whether now known or here after devised (including, without limitation, in
                    computer or other device applications, online webcasts, television programming (including broadcasts on ESPN platforms), in motion pictures, films, newspapers, and magazines)
                    and in all forms including, without limitation, digitized images or video, throughout the universe in perpetuity, whether for advertising, publicity, or promotional purposes, including,
                    without limitation, publication and use of Event results and standings, without compensation, residual obligations, reservation or limitation, or further approval, and I agree to
                    indemnify and hold harmless the Released Parties for any Claims associated with such grant and right to use. The Released Parties are, however, under no obligation to exercise
                    any rights granted herein</span>
            </div>
            <div class="col-md-12 mt-2">
                <span><b class="mr-2">GOVERNING LAW:  </b> This Form will be governed by the laws of the State of Florida, and any legal action relating to or arising out of this Form will be commenced
                    exclusively in the Circuit Court of the Ninth Judicial Circuit in and for Orange County, Florida (or if such Circuit Court does not have jurisdiction over the subject
                    matter thereof, then to such other court sitting in such county and having subject matter jurisdiction), AND I SPECIFICALLY WAIVE THE RIGHT TO TRIAL
                    BY JURY.</span>
            </div>

        </div>
        <div class="row justify-content-center">

                <h5>NOTICE TO THE MINOR CHILD’S NATURAL GUARDIAN(S) </h5>

        </div>
        <div class="row">
            <div class="col-md-12">
                <h6>READ THIS FORM COMPLETELY AND CAREFULLY. YOU ARE AGREEING TO LET YOUR MINOR CHILD ENGAGE IN
                    A POTENTIALLY DANGEROUS ACTIVITY. YOU ARE AGREEING THAT, EVEN IF EACH OF THE RELEASED PARTIES
                    (THAT IS, WALT DISNEY PARKS AND RESORTS U.S., INC., DISNEY DESTINATIONS, LLC, ESPN, INC. AND THEIR
                    RESPECTIVE PARENT, SUBSIDIARY AND OTHER AFFILIATED OR RELATED COMPANIES (COLLECTIVELY, THE “DISNEY
                    COMPANIES”); VARSITY SPIRIT, LLC, ALL EVENT SPONSORS AND CHARITIES HAVING A PRESENCE AT THE EVENT
                    AND THEIR RESPECTIVE PARENT, SUBSIDIARY AND OTHER AFFILIATED OR RELATED COMPANIES (COLLECTIVELY,
                    THE “EVENT HOST/SPONSORS/CHARITIES”); REEDY CREEK IMPROVEMENT DISTRICT AND ITS BOARD OF
                    SUPERVISORS (COLLECTIVELY, “RCID”); AND THE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, CONTRACTORS,
                    SUBCONTRACTORS, REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF EACH OF THE FOREGOING
                    ENTITIES) USE REASONABLE CARE IN PROVIDING THIS ACTIVITY, THERE IS A CHANCE YOUR CHILD MAY BE
                    SERIOUSLY INJURED OR KILLED BY PARTICIPATING IN THIS ACTIVITY BECAUSE THERE ARE CERTAIN
                    DANGERS INHERENT IN THE ACTIVITY THAT CANNOT BE AVOIDED OR ELIMINATED. BY SIGNING THIS FORM YOU</h6>
            </div>

        </div>
        <div class="row mt-4">

            <div class="col-md-6 ">
                <span><strong class="mr-2">Minor’s Name: </strong>{{ isset($waiver_data->minor_name_2) ? $waiver_data->minor_name_2 : "" }}
                    </span>
            </div>
            <div class="col-md-6">
                <span><strong class="mr-2">  Organization/ Team Name: </strong>{{ isset($waiver_data->team_name_2) ? $waiver_data->team_name_2 : "" }}
                </span>


            </div>
            <div class="col-md-12 mt-2">
                <h6>ARE GIVING UP YOUR CHILD’S RIGHT AND YOUR RIGHT TO RECOVER FROM THE RELEASED PARTIES (THAT IS,
                    WALT DISNEY PARKS AND RESORTS U.S., INC. AND THE OTHER DISNEY COMPANIES; THE EVENT HOST/SPONSORS/
                    CHARITIES; RCID; AND THE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, CONTRACTORS, SUBCONTRACTORS,
                    REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF EACH OF THE FOREGOING ENTITIES) IN A
                    LAWSUIT FOR ANY PERSONAL INJURY, INCLUDING DEATH, TO YOUR CHILD OR ANY PROPERTY DAMAGE THAT
                    RESULTS FROM THE RISKS THAT ARE A NATURAL PART OF THE ACTIVITY. YOU HAVE THE RIGHT TO REFUSE TO SIGN
                    THIS FORM, AND THE RELEASED PARTIES (THAT IS, WALT DISNEY PARKS AND RESORTS U.S., INC. AND THE OTHER
                    DISNEY COMPANIES; THE EVENT HOST/SPONSORS/CHARITIES; RCID; AND THE OFFICERS, DIRECTORS, EMPLOYEES,
                    AGENTS, CONTRACTORS, SUBCONTRACTORS, REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF
                    EACH OF THE FOREGOING ENTITIES) HAVE THE RIGHT TO REFUSE TO LET YOUR CHILD PARTICIPATE IF YOU DO NOT
                    SIGN THIS FORM.
                    THE NOTICE ABOVE IS ALSO GIVEN AND APPLICABLE TO LEGAL GUARDIANS AND THEIR MINOR WARD(S) WHO
                    YOU ARE AGREEING TO LET ENGAGE IN POTENTIALLY DANGEROUS ACTIVITIES. ALL REFERENCES TO “CHILD”
                    ABOVE ARE APPLICABLE YOUR MINOR WARD(S) AND YOUR AND YOUR WARD’S RIGHTS TO RECOVER FROM THE
                    RELEASED PARTIES (THAT IS, WALT DISNEY PARKS AND RESORTS U.S., INC. AND THE OTHER DISNEY COMPANIES;
                    THE EVENT HOST/SPONSORS/CHARITIES; RCID; AND THE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS,
                    CONTRACTORS, SUBCONTRACTORS, REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF EACH
                    OF THE FOREGOING ENTITIES)</h6>

            </div>


        </div>
        <div class="row mt-3">
            <div class="col-md-4">
                @if(isset($waiver_data->sign_1))
                    <img src="{{ url('/img/waiver/'.$waiver_data->sign_1) }}" alt="" style="width:150px; height:170px;">
                @endif
            </div>
            <div class="col-md-2">
                <span class="float-right"></span>
            </div>
            <div class="col-md-4 float-right">
                <span>{{ isset($waiver_data->witness_1) ? $waiver_data->witness_1 : "" }}</span>
                <br>
                <small>
                    Witness</small>
            </div>
            <div class="col-md-2 ">
                <span class="float-right">{{ isset($waiver_data->created_at) ? $waiver_data->created_at->todatestring() : "" }}</span>
                <br>
                <span class="float-right">Date:</span>
            </div>
            <div class="col-md-4">
                <small>
                 Signature of Parent(s) or Legal Guardian(s)</small>
            </div>
            <div class="col-md-2">
                <span >Date:{{ isset($waiver_data->created_at) ? $waiver_data->created_at->todatestring() : "" }}</span>
            </div>


        </div>
        <div class="row">
            <div class="col-md-12">

                <span><b>SUPERVISION:</b> A chaperone/adult (age 21 or over) is required to attend with participants. This chaperone will be responsible for the participants at all times. I/we acknowledge
                    that Disney and Varsity are not responsible for supervising my/our child.</span>
            </div>
            <div class="col-md-12">

                <span><b>RESPONSIBILITY DISCLOSURE NOTICE:</b> Varsity acts only as an agent in connection with the tour offered herein and its liability is limited. The travel services including air
                    transportation, carriage by land, hotel accommodations, restaurants, and related services are provided by independent third parties not under the control of Varsity. Varsity
                    shall NOT bear any liability to the passenger or any person claiming by or through the passenger for any injury, damage, loss, accident, delay, or irregularity which may be
                    occasioned either by reason of or through the acts or defaults of any company or person engaged in conveying the passengers or in carrying out the arrangements of the tour
                    and/or performance events, venues, etc. as a direct or indirect result of acts of God, dangers incident to fire, breakdown in machinery or equipment, acts of governments or other
                    authorities, civil disturbances, strikes, riots, acts of terrorism, theft, unhealthy conditions, pilferage, epidemics, quarantines, medical or customs regulations, or from any other
                    cause beyond the control of Varsity. Varsity shall not be liable for any losses or additional expenses due to delay or changes in schedule or other causes. The right is reserved to
                    decline, to accept, or to retain any tour passenger should such person’s health or general deportment impede the operation of the tour to the detriment of other passengers. No
                    refunds for your portions of unused services can be made unless agreed to prior to the scheduled deadlines. Your retention of tickets, reservations, or bookings after issuance
                    shall constitute a consent to the above and agreement on your part to convey the contents herein to your traveling companions. Payment of any deposit or final payment shall be
                    deemed to constitute consent by each passenger to these terms. Baggage is carried at the owner’s risk and baggage insurance is strongly recommended. It is also recommended
                    that each participant in this tour have his or her own attorney review this RESPONSIBILITY DISCLOSURE NOTICE before indicating his or her consent by signing this consent
                    form. Nothing in this paragraph is intended to or shall affect in any way the respective rights or relationship between Varsity and any person other than the passenger and any
                    person claiming by or through the passenger.</span>
            </div>
            <hr>
            <div class="col-md-12">

                <span><b class="mr-2">MEDICAL RELEASE:</b> I/we authorize Disney and/or Varsity to procure at my/our expense, any medical care reasonably required by my/our child during his/her visit at hospitals or
                    facilities chosen by Disney and/or Varsity. I/we have listed below any medication that my/our child is currently taking. I/we will ensure that my/our child brings the medication with
                    him/her to the Walt Disney World® Resort and that my/our child is responsible for taking the medication. I/we have also listed below any medications my/our child is allergic to.
                    By signing below, I certify that: (1) I fully and completely read and understand this Form; (2) I am 18 years of age or older; (3) I am the legal guardian of the minor child identified
                    above; (4) the information set forth above pertaining to my child or ward is true and complete; and (5) I consent and agree to all of the foregoing on behalf of myself and my minor
                    child or ward identified above.</span>
            </div>


        </div>
        <div class="row mt-2">
            <div class="col-md-12 mt-2">
                <span><b>Medications my/our child is taking (if any):</b>{{ isset($waiver_data->m_taking) ? $waiver_data->m_taking : "" }}  </span>
            </div>
            <div class="col-md-12 mt-2">
                <span><b>Medications my/our child is allergic to (if any):</b>{{ isset($waiver_data->m_allergic) ? $waiver_data->m_allergic : "" }}</span>
            </div>
            <div class="col-md-12 mt-2">
                <span><b>Organization / Team Name (of child):</b>{{ isset($waiver_data->team_name_3) ? $waiver_data->team_name_3 : "" }}</span>
            </div>
            <div class="col-md-12 mt-2">
                <span><b>Minor’s Name :</b>{{ isset($waiver_data->minor_name_3) ? $waiver_data->minor_name_3 : "" }}</span>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-4">
                @if(isset($waiver_data->sign_2)) 
                    <img src="{{ url('/img/waiver/'.$waiver_data->sign_2) }}" alt="" style="width:150px; height:170px;">
                @endif
            </div>
            <div class="col-md-2">
                <span class="float-right"></span>
            </div>
            <div class="col-md-4 float-right">
                <span>{{ isset($waiver_data->witness_2) ? $waiver_data->witness_2 : "" }}</span>
                <br>
                <small>
                    Witness</small>
            </div>
            <div class="col-md-2 ">
                <span class="float-right">{{ isset($waiver_data->created_at) ? $waiver_data->created_at->todatestring() : "" }}</span>
                <br>
                <span class="float-right">Date</span>
            </div>
            <div class="col-md-4">
                <small>
                 Signature of Parent(s) or Legal Guardian(s)</small>
            </div>
            <div class="col-md-2">
                <span >Date:{{ isset($waiver_data->created_at) ? $waiver_data->created_at->todatestring() : "" }}</span>
            </div>


        </div>
        <div class="row">
            <div class="col-md-12 mt-2">
                <span>EMERGENCY INFORMATION: (Not traveling with the minor)</span>
            </div>
            <div class="col-md-4">
            <span><b>Name:</b>{{ isset($waiver_data->name) ? $waiver_data->name : "" }}</span>
            </div>
            <div class="col-md-8">
                <span><b>Address:</b>{{ isset($waiver_data->address_2) ? $waiver_data->address_2 : "" }}</span>
            </div>
            <div class="col-md-4">

                <span><b>Telephone:</b>{{ isset($waiver_data->telephone) ? $waiver_data->telephone : "" }}</span>
            </div>
            <div class="col-md-4">

                <span><b>home:</b>{{ isset($waiver_data->home) ? $waiver_data->home : "" }}</span>
            </div>
            <div class="col-md-4">

                <span><b>Work:</b>{{ isset($waiver_data->work) ? $waiver_data->work : "" }}</span>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <h6>EVERY PARENT OR LEGAL GUARDIAN OF A MINOR PARTICIPATING IN THE EVENT MUST COMPLETE THIS FORM AND TURN IN
                    TO THE COACH TO BE MAILED TO THE VARSITY OFFICE NO LATER THAN 3 WEEKS PRIOR TO THE EVENT.
                    </h6>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        window.onload = function() { window.print(); }
   </script>
</body>
</html>
