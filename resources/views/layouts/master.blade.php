<!doctype html>
<html lang="en">
  <head>
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon"/>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/front/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/front/css/style.css">
    <link rel="stylesheet" href="/assets/front/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
    <title>Cheer Here USA</title>
  </head>
  <body>
    <header>
      <div class="container head-logo">
        <div class="first-bar">
          <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-9">
              <ul class="list-unstyled">
                <li class="list-inline-item">
                  <span>Welcome To <strong>Cheer Here USA.</strong></span>
                </li>
                <li class="list-inline-item">
                  <a href="#"><i class="fas fa-envelope mr-2"></i>info@cheerhereusa.com</a>
                </li>
                <li class="list-inline-item">
                  <div class="social-icons">
                    <ul class="list-unstyled">
                      <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                      <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                      <li class="list-inline-item"><a href="#"><i class="fab fa-snapchat"></i></a></li>
                      <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="menu-bar">
        <div class="container">
          <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 disflex">
              <ul class="list-unstyled navs">
                <li class="list-inline-item @if(request()->url()==url('/')) active @endif"><a href="/">Home</a></li>
                <li class="list-inline-item @if(request()->url()==url('/about')) active @endif"><a href="{{ route('about') }}">About</a></li>
                <li class="list-inline-item @if(request()->url()==url('/clinics')) active @endif"><a href="{{ route('clinic.index') }}">Clinics</a></li>
                <li class="list-inline-item @if(request()->url()==url('/contact-us')) active @endif"><a href="{{ route('contact') }}">Contact us</a></li>
              </ul>
            </div>
            <div class="col-lg-3 disflex">
              <ul class="list-unstyled">
                <li class="list-inline-item">
                  <div class="icon-box click-s">
                    <i class="fas fa-search"></i>
                  </div>
                  <div class="search0" id="shoow-s" style="display: none;">
                    <div class="bg-search">
                      <form action="{{ route('clinic.index') }}" method="get">
                        <div class="input-group">
                          <input type="text" requried="" class="form-control" id="s" name="q" placeholder="Search">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">GO</button>
                          </span>
                        </div>
                      </form>
                    </div>
                  </div>
                </li>
                @if (Auth::check())
                <li class="list-inline-item">
                  @if(Auth::user()->type == 'Super Admin')
                  <a href="{{ route('dashboard') }}" class="btn"><i class="fas fa-lock mr-2"></i>Dashboard</a>
                  @else
                  <a href="{{ authredirect() }}" class="btn"><i class="fas fa-lock mr-2"></i>Dashboard</a>
                  @endif
                </li>
                @else
                <li class="list-inline-item">
                  <a href="{{ route('login') }}" class="btn"><i class="fas fa-lock mr-2"></i>Login</a>
                </li>
                @endif


                 @if (Auth::check())
                    <li class="list-inline-item"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn  ">Logout</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  @else
                  <li class="list-inline-item"><a href="{{ route('member.register.form') }}" class="btn"><i class="fas fa-user-alt mr-2"></i>Register</a></li>
                  @endif


                <li></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
  @yield('mainContent')
   {{--   <section class="main-post-sec">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2>Es gibt viele Variationen <strong>Der Passages des Lorem</strong></h2>
          </div>
          <div class="col-lg-5 disflexend">
            <ul class="list-unstyled float-right">
              <li class="list-inline-item"><a href="#" class="btn btn-business">post a job</a></li>
              <li class="list-inline-item"><a href="#" class="btn btn-business">upload resume</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section> --}}
    <!-- post -->
    <!-- Unnamed -->
    {{--  <section class="main-unnamed-sec">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="content bg-black">
              <h2>Es gibt viele Variationen <strong>Schedule and Location</strong></h2>
            </div>
          </div>
          <div class="col-lg-6 ">
            <div class="content bg-blue">
              <h2>Es gibt viele Variationen <strong>Schedule and Location</strong></h2>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Unnamed -->
    <!-- Contact-form -->
    <section class="main-form-sec">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="form-in">
              <div class="content text-center">
                <h2 class="sec-heading">Sign Up For Updates!</h2>
              <p class="sec-para">We are experts in this industry with over 100 years experience. What that means is you are going to get right solution. please find our services.</p>
              </div>
              <form>
                <div class="form-row">
                  <div class="form-group col-md-6 col-lg-6">
                    <input type="text" class="form-control" id="inputEmail4" placeholder="First Name">
                  </div>
                  <div class="form-group col-md-6 col-lg-6">
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Last Name">
                  </div>
                  <div class="form-group col-md-6 col-lg-6">
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Email">
                  </div>
                  <div class="form-group col-md-6 col-lg-6">
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Phone">
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-12 col-lg-12">
                    <textarea name="" id="" cols="" rows="10" placeholder="Describe you need to us"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 text-center">
                    <a href="#" class="btn btn-more">Submit</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>  --}}


    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <a href="#">© Copyright 2020 CheerHereUSA.com</a>
          </div>
          <div class="col-lg-6 disflexend">
            <div class="privacy"><a href="#">Privacy Policy </a> &nbsp;|&nbsp; <a href="#">Terms & Conditions</a></div>
          </div>
        </div>
      </div>
  </footer>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="/assets/front/js/bootstrap.min.js"></script>
    <script src="/assets/front/js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
          loop:true,
          margin:10,
          dots:false,
          nav:true,
          mouseDrag:false,
          autoplay:false,
          animateOut: 'slideOutUp',
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
        });
      });
    </script>
    <script>
      jQuery( document ).ready(function($) {
        $('.click-s').click(function(e) {
        e.preventDefault();
        $('#shoow-s').toggle();
        });
      });
</script>
<script>
$(function() {
    $('.popup-youtube, .popup-vimeo').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });
});
</script>
  </body>
</body>
</html>
