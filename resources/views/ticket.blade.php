@extends('layouts.master')
@section('mainContent')

{{-- @dd($__data) --}}

<section class="main-content inner-page">
  <div class="owl-carousel owl-theme">
    <div class="item">
      <img src="/assets/front/images/about-banner.jpg" alt="images not found">
      <div class="cover">
        <div class="container">
          <div class="header-content">
            <!-- <div class="line"></div> -->
            <h1>Tickets</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Detail -->

<!-- Contact-form -->
<section class="main-form-sec contact-page">
  
  <div class="container">

  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
    <a class="btn btn-primary mb-3 shadow-lg" href="javascript:history.back()">Back</a>
    <a class="btn btn-primary mb-3 float-right text-white shadow-lg" href="{{ route('newTicketForm') }}">New Ticket</a>
    <table class="table table-responsive">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Ticket ID</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Status</th>
      <th scope="col">Category</th>
      <th scope="col">Ticket Owner</th>
      <th scope="col">Ticket Recipient</th>
      <th scope="col">Created</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($tickets as $ticket)
    <tr>
      <input type="hidden" value="{{ $ticket->id }}" name="ticket_id">
      <td scope="row">{{ $ticket->id }}</td>
      <td><a href="{{ route('ticketDetail', $ticket->id) }}">{{ $ticket->title }}</a></td>
      <td>{{ $ticket->description }}</td>
      @if($ticket->status == 0)
      <td><span class="badge badge-pill badge-success">Closed</span></td>
      @elseif($ticket->status == 1)
      <td><span class="badge badge-pill badge-danger">Open</span></td>
      @endif
      <td>{{ $ticket->category }}</td>
      <td>{{ $ticket->first_name . ' ' . $ticket->last_name }}</td>
      <td>{{ $ticket->recipient_first_name . ' ' . $ticket->recipient_last_name }}</td>
      <td>{{ $ticket->created_at->diffForHumans() }}</td>
      @if(Auth::user()->type == 'Admin' || Auth::user()->type == 'Super Admin')
      <td><a onclick="return confirm('Are you sure?')" href="{{ route('ticketDelete', $ticket->id) }}" class="btn btn-outline-danger shadow">Delete</a></td>
      @elseif($ticket->user_id == Auth::user()->id)
      <td><a onclick="return confirm('Are you sure?')" href="{{ route('ticketDelete', $ticket->id) }}" class="btn btn-outline-danger shadow">Delete</a></td>
      @else
      <td><button disabled class="btn btn-outline-secondary">Delete</button></td>
      @endif
    </tr>
    @endforeach
  </tbody>
</table>
  </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
  function ticketClose(id) {
    console.log(id);
    Swal.fire({
      title: 'Close this ticket?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, close it!'
    }).then((result) => {
      if (result.isConfirmed) {
        axios.post('tickets/close/' + id)
        .then(function (response) {
          console.log(response);
          Swal.fire(
            'Closed!',
            'This ticket has been closed.',
            'success'
          )
          location.reload();
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    })
  
  }
</script>

@endsection