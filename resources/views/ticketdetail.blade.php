@extends('layouts.master')
@section('mainContent')

{{-- @dd($__data) --}}

<section class="main-content inner-page">
  <div class="owl-carousel owl-theme">
    <div class="item">
      <img src="/assets/front/images/about-banner.jpg" alt="images not found">
      <div class="cover">
        <div class="container">
          <div class="header-content">
            <!-- <div class="line"></div> -->
            <h1>Tickets</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Ticket Details -->
<section class="main-form-sec contact-page">
  <div class="container">
      
    @if (session('status'))
    <div class="alert alert-success" role="alert">
      {{ session('status') }}
    </div>
    @endif
    
    <a href="/tickets" class="btn btn-primary mb-3">Back</a>

    <div class="card text-center shadow-lg">
      <div class="card-header">
        <h2>{{ $ticket->title }}</h2>
      </div>
      <div class="card-body">
        <h5 class="card-title">{{ $ticket->description }}</h5>
        <p class="card-text">Category: {{ $ticket->category }}</p>
        @if($ticket->status == 0)
        <p class="card-text">Status: <span class="badge badge-pill badge-success">Closed</span></p>
        @elseif($ticket->status == 1)
        <p class="card-text">Status: <span class="badge badge-pill badge-danger">Open</span></p>
        @endif
        @if($ticket->status == 1)
        <a href="{{ route('closeTicket', $ticket->id) }}" class="btn btn-outline-primary shadow-lg">Close Ticket</a>
        @else
        <a href="{{ route('closeTicket', $ticket->id) }}" class="btn btn-outline-secondary shadow-lg disabled">Close Ticket</a>
        @endif
      </div>
      <div class="card-footer text-muted">
        {{ $ticket->created_at->diffforhumans() }}
      </div>
    </div>

      <hr>

      <br>
      <h3>Post a Comment</h3>

      <div class="card text-center">
        <div class="card-body">
          <form method="POST" action="{{ route('createComment') }}">
            @csrf
            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
            <div class="form-group">
              <input type="text" name="comment" class="form-control">
            </div>
            @if($ticket->status == 1)
            <button type="submit" name="submit" value="open" class="btn btn-outline-primary">Submit as Open</button>
            @else
            <button type="submit" name="submit" value="open" class="btn btn-outline-primary">Submit</button>
            @endif
            @if($ticket->status == 1)
            <button type="submit" name="submit" value="close" class="btn btn-outline-primary">Submit as Closed</button>
            @else
            <button disabled type="submit" name="submit" value="close" class="btn btn-outline-secondary">Submit as Closed</button>
            @endif
          </form>
        </div>
      </div>

      <hr>
      
      <br>
      <h3>Comments ({{ $ticket->comments->count() }})</h3>

      @if($ticket->comments->count() > 0)
      @foreach($ticket->comments as $comment)
      <div class="card text-center mb-3">
        <div class="card-header">
          <p style="float: left">{{ $comment->user->first_name . ' ' . $comment->user->last_name . ' (' . $comment->user->type . ')' }}</p>
          <p style="float: right">{{ $comment->created_at->format('Y-m-d h:m:s A') }}</p>
        </div>
        <div class="card-body">
          <h5 class="card-title" style="float: left">{{ $comment->comment }}</h5>
        </div>
        {{-- <div class="card-footer text-muted">
          2 days ago
        </div> --}}
      </div>
      @endforeach
      @else
      No comments
      @endif

</div>
</section>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

@endsection