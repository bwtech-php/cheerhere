@extends('layouts.master')

@section('mainContent')
{{-- @dd($__data) --}}
  <section class="main-content">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <img src="/assets/front/images/banner2.jpg" alt="images not found">
                <div class="cover">
                    <div class="container">
                        <div class="header-content">
                          <div style="background-color: rgba(0,0,0,0.3);">  
                            <!-- <div class="line"></div> -->
                            <div class="img-box">
                              <img src="/assets/front/images/banner-before.png" alt="">
                            </div>
                            <h2>Cheer & Stunt Clinics</h2>
                            <h1>Training & Recruitment</h1>
                            <p> Never miss another cheer clinic again! Become a member today to be notified as soon as your preferred colleges release a clinic.</p>
                            <ul class="list-unstyled">
                              <li class="list-inline-item"><a href="/about" class="btn btn-business">read more</a></li>
                              <li class="list-inline-item"><a href="/clinics" class="btn btn-business">enroll now!</a></li>
                            </ul>
                          </div>
                        </div>
                    </div>
                 </div>
            </div>
            <div class="item">
                <img src="/assets/front/images/banner.jpg" alt="images not found">
                <div class="cover">
                    <div class="container">
                        <div class="header-content">
                          <div style="background-color: rgba(0,0,0,0.3);">  
                            <!-- <div class="line animated bounceInLeft"></div> -->
                            <div class="img-box">
                              <img src="/assets/front/images/banner-before.png" alt="">
                            </div>
                            <h2>Cheer & Stunt Clinics</h2>
                            <h1>Training & Recruitment</h1>
                            <p> Never miss another cheer clinic again! Become a member today to be notified as soon as your preferred colleges release a clinic.</p>
                            <ul class="list-unstyled">
                              <li class="list-inline-item"><a href="/about" class="btn btn-business">read more</a></li>
                              <li class="list-inline-item"><a href="/clinics" class="btn btn-business">enroll now!</a></li>
                            </ul>
                          </div>
                        </div>
                    </div>
                 </div>
            </div>
            <div class="item">
                <img src="/assets/front/images/banner.jpg" alt="images not found">
                <div class="cover">
                    <div class="container">
                        <div class="header-content">
                          <div style="background-color: rgba(0,0,0,0.3);">  
                            <!-- <div class="line animated bounceInLeft"></div> -->
                            <div class="img-box">
                              <img src="/assets/front/images/banner-before.png" alt="">
                            </div>
                            <h2>Cheer & Stunt Clinics</h2>
                            <h1>Training & Recruitment</h1>
                            <p> Never miss another cheer clinic again! Become a member today to be notified as soon as your preferred colleges release a clinic.</p>
                            <ul class="list-unstyled">
                              <li class="list-inline-item"><a href="/about"class="btn btn-business">read more</a></li>
                              <li class="list-inline-item"><a href="/clinics" class="btn btn-business">enroll now!</a></li>
                            </ul>
                          </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </section>
    <!-- Detail -->
    <section class="detail">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="detail-in">
              <div class="dropdowns">
              <form method="GET" action="{{ route('clinicSearch') }}">
              <ul class="list-unstyled">
                <!-- 1st -->
                {{-- State: <li> --}}
                  {{-- <div> --}}
                  
                    <div class="col">
                    <select class="form-control" name="state">
                      {{-- <div class="dropdown-menu"> --}}
                        <option>Search By State</option>
                        @foreach($states as $s)
                        <option>{{ $s->name }}</option>
                        @endforeach
                      {{-- </div> --}}
                    </select>
                    </div>
                    {{-- <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Search By State
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                      <a class="dropdown-item" href="#">Separated link</a>
                    </div> --}}
                  {{-- </div> --}}
                {{-- </li> --}}
                <!-- 1st -->
                <!-- 2nd -->
                {{-- <li class="list-inline-item">
                  <div class="btn-group">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Search By School
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                  </div>
                </li> --}}
                <div class="col">
                <select class="ml-2 form-control" name="school">
                  <option>Search By School</option>
                  @foreach($search as $s)
                  <option>{{ $s->name }}</option>
                  @endforeach
                </select>
                </div>
                <!-- 2nd -->
                <!-- 3rd -->
                {{-- City: <li class="list-inline-item">
                  <div class="btn-group">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Search By City
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                  </div>
                </li> --}}
                <div class="col">
                <select class="ml-2 form-control" name="city">
                  <option>Search by City</option>
                  @foreach($search as $s)
                  <option>{{ $s->city }}</option>
                  @endforeach
                </select>
                </div>
                <!-- 3rd -->
                <!-- 4th -->
                {{-- <li class="list-inline-item ml-2"> --}}
                  <div class="col">
                  <input name="date" class="ml-2 form-control" type="date" id="exampleFormControlInput1" placeholder="Date From" onkeydown="return false">
                  </div>
                  {{-- </li> --}}
                <!-- 4th -->
                <!-- 5th -->
                {{-- <li class="list-inline-item"> --}}
                  <div class="col">
                  <input name="cost" type="text" class="ml-2 form-control" id="exampleFormControlInput1" placeholder="Cost">
                  </div>
                {{-- </li> --}}
                <!-- 5th -->
              </ul>
            </div>
            <ul class="list-unstyled text-center btns">
                <li class="list-inline-item">
                  <button type="reset" class="btn btn-more">clear</button>
                </li>
                <li class="list-inline-item">
                  <button type="submit" class="btn btn-more">Search</button>
                </li>
              
              </ul>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Detail -->
    <!-- About -->
    <div class="main-about-sec">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="content">
              <h2 class="sec-heading">About Cheer Here USA</h2>
              <p class="sec-para">Cheer Here USA was created because we saw a need and knew how to fill it.
              When my daughter, who had competed in competition cheer and high school
              cheer, was looking for the right college the search went beyond academic
              options. She wanted to continue to be involved in cheer and the up and
              coming sport of Stunt.</p>
              <p class="sec-para">She wanted to stay involved and create her new group
              as she went off away from all her friends. We found the best way to do this
              was to attend college cheer clinics. </p>
              <a href="/about" class="btn btn-more">Read More</a>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="img-box">
              <img src="assets/front/images/aboutsection.jpg" class="img-fluid" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- About -->
    <!-- Clinics -->
    {{--  <section class="main-clinics-sec">
      <div class="container">
        <div class="row text-center">
          <div class="col-lg-12">
            <h2 class="sec-heading">The World's Largest Selection of Clinics</h2>
              <p class="sec-para">Choose from 100,000 online video clinics with new additions published every month</p>
          </div>
        </div>
        <div class="row mb-4">
          @forelse ($clinics as $clinic)

          <div class="col-lg-3">
            <div class="clinic-box">
              <div class="img-box">
                <img src="/assets/front/images/1.jpg" class="img-fluid" alt="">
              </div>
              <div class="overlay">
                <h4>{{ $clinic->school->name ?? null }}</h4>
                <h5>{{ assigned_waiver($clinic->assigned_waiver) }}</h5>
                <a href="{{ route('clinic.detail', $clinic->id) }}" class="btn">Get Started<i class="fas fa-angle-right ml-2"></i></a>
              </div>
            </div>
          </div>
          @empty
          <p>We apologize, there are currently no clinics scheduled.  Please check back regularly or register today to be notified as soon as clinics are announced</p>
          @endforelse
        </div>

      </div>
    </section>  --}}
    <!-- Clinics -->
    <!-- Video -->
   {{--  <section class="main-video-sec">
      <div class="container">
        <div class="video-in">
          <div class="row">
            <div class="col-lg-6">
              <div class="img-box"><img src="/assets/front/images/video-img.jpg" class="img-fluid" alt=""></div>
              <div class="overlay">
                <a class="popup-youtube" href="https://youtu.be/6rSSXxIL1RQ"><img src="/assets/front/images/play-icon.png" alt=""></a>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="content">
                <h2 class="sec-heading">Summer Camp - <br>Make The Connection!</h2>
                <p class="sec-para">Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird, wenn er sich ein Layout ansieht. Der Punkt, Lorem Ipsum zu nutzen, ist, dass es mehr oder weniger die.</p>
              <p class="sec-para">Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird, wenn er sich ein Layout ansieht. Der Punkt, Lorem Ipsum zu nutzen, ist, dass es mehr oder weniger die normale ist. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> --}}
    <!-- Video -->
    <!-- post -->

@endsection
