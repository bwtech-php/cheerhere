@extends('layouts.master')

@section('mainContent')

<section class="main-content inner-page">
  {{-- @dd(Auth::user()->date_of_birth) --}}
    <div class="owl-carousel owl-theme">
    <div class="item">
      <img src="/assets/front/images/about-banner.jpg" alt="images not found">
      <div class="cover">
        <div class="container">
          <div class="header-content">
            <!-- <div class="line"></div> -->
            <h1>Enroll Here</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Detail -->
<!--Main-content-->
<section class="dashboard ">
    <diiv class="coach-form">
        <div class="container">
          <div class="row">
            <div class="col-3 naves">
              <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Enroll</a>

              </div>
            </div>
            <div class="col-9 naves-content">
              <div class="tab-content" id="v-pills-tabContent">

                <div class="tab-pane fade show active " id="v-pills-member" role="tabpanel" aria-labelledby="v-pills-member-tab">
                  <h2>Enroll</h2>
                  <form action="{{ route('clinic.enroll.update'  , $clinic->id) }}" method="post">
                    @csrf

                      <h5><strong>{{ $clinic->school->name }} Clinic</strong></h5>
                      <br>
                      <p>To change your First Name, Last Name, Address, City, State, Zip Code, Date of Birth, Phone or Email, go to <a href="{{ url('/member/profile') }}">Edit Profile</a>.</p>
                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">First Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" id="inputnam1" value="{{ Auth::user()->first_name }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Last Name:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="inputnam2" name="last_name" value="{{ Auth::user()->last_name }}"  readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Address:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="inputnam2" value="{{ Auth::user()->address }}"  readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">City:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('city') is-invalid @enderror" name="city" id="inputnam1" value="{{ Auth::user()->city }}" readonly>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="inputname1" class="col-sm-4 col-form-label">State:</label>
                          <div class="col-sm-7">
                            {{-- <select id="inputState" class="form-control @error('state') is-invalid @enderror" name="state" style="-webkit-appearance: none;"  readonly>
                              <option value="" >Select State</option>
                              @forelse ($states as $state)
                              <option value="{{ $state->id }}"  {{ Auth::user()->state == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                              @empty
                              @endforelse
                            </select> --}}
                            <input type="text" class="form-control @error('state') is-invalid @enderror" name="state" id="inputState" value="{{ $state->name }}" readonly >

                          </div>

                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label for="inputname1" class="col-sm-4 col-form-label">Zip Code:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control @error('zip') is-invalid @enderror" name="zip" id="inputnam1" value="{{ Auth::user()->zip }}" readonly >
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Date Of Birth:</label>
                      <div class="col-sm-4">
                        <input type="date" class="form-control @error('age') is-invalid @enderror" name="age" id="inputEmail3" value="{{ (Auth::user()->date_of_birth) }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Phone:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="inputEmail3" value="{{ Auth::user()->phone }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Email:</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="inputEmail3" value="{{ Auth::user()->email }}" readonly>
                      </div>
                    </div>

                    <div class="form-group row checkbox-in">
                      <div class="col-sm-12 checkbx">Do you participate in Competitive Cheering:</div>
                      <div class="col-sm-2 p-0">
                        <div class="form-check">
                          <input class="form-check-input @error('participate') is-invalid @enderror" name="participate" value="1" type="radio" id="gridCheck01"  >
                          <label class="form-check-label" for="gridCheck01">
                            Yes
                          </label>
                        </div>
                      </div>
                      <div class="col-sm-2 p-0">
                        <div class="form-check">
                          <input class="form-check-input" name="participate" value="0" type="radio" id="gridCheck02" >
                          <label class="form-check-label" for="gridCheck02">
                            No
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row sygmshow">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Home Gym:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="home_gym" id="inputPassword3" value="{{ Auth::user()->home_gym }}"  >
                        </div>
                      </div>
                    <div class="form-group row checkbox-in">
                      <div class="col-sm-12 checkbx">Profile Link:</div>


                        <div class="col-sm-2 p-0">
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio"  name="profileLink" id="inlineRadio1" value="1">
                              <label class="form-check-label" for="inlineRadio1">Yes</label>
                            </div>
                        </div>
                        <div class="col-sm-2 p-0">
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="profileLink" id="inlineRadio2" value="0" >
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                        </div>
                        <div class="col-sm-12 checkbx"><small id="emailHelp" class="form-text text-muted">Upgrade your membership now to link your  profile for coaches to see your skills and videos.</small></div>
                    </div>


                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        <button type="submit" class="btn btn-primary float-left">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
</section>

@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    jQuery(document).ready(function($) {

      $('body').on('click', 'input[name=participate]', function( ) {
        console.log($(this).val())
        if ($(this).val() == 1) {
          $('.sygmshow').show();
        }else{

          $('.sygmshow').hide();
        }
      });

    });
  </script>
