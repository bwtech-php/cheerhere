@extends('layouts.master')
@section('mainContent')
{{-- @dd($__data) --}}
<section class="main-content inner-page">
  <div class="owl-carousel owl-theme">
    <div class="item">
      <img src="/assets/front/images/about-banner.jpg" alt="images not found">
      <div class="cover">
        <div class="container">
          <div class="header-content">
            <!-- <div class="line"></div> -->
            <h1>{{ $school->name}}<br><i  class="far fa-calendar-check mt-2"></i> {{ $row->clinic_pay_by_date }} <br><small class="ml-2"><i class="far fa-clock"></i> {{ $row->clinic_time }}</small>  </h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Detail -->
<!--Main-content-->
<section class="dashboard ">
    <div class="coach-form">
        <div class="container">
          <div class="row">
            <div class="col-3 naves">
              <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                    {{ $school->name}}
                </a>

              </div>
            </div>
            <div class="col-9 naves-content">
              @if(session('success_msg'))
                            <div class="alert alert-success fade in alert-dismissible show">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true" style="font-size:20px">×</span>
                                </button>
                                {{ session('success_msg') }}
                            </div>
                            @endif
                            @if(session('error_msg'))
                            <div class="alert alert-danger fade in alert-dismissible show">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true" style="font-size:20px">×</span>
                                </button>
                                {{ session('error_msg') }}
                            </div>
                            @endif
              <div class="tab-content" id="v-pills-tabContent">
                <!-- Profile -->
                <div class="tab-pane fade show active profile single_clinic" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                  <h2></h2>
                  @if (Session::has('msg'))
                  <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-success">{{ Session::get('msg') }}</div>
                    </div>
                  </div>
                  @endif
                  <div class="row">
                    <div class="col-lg-12">
                        <form>
                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">Clinic Location:</label>
                      <div class="col-sm-10">

                        <input type="text" class="form-control" id="inputnam1" value="{{ $row->clinic_location_name}}, {{ $row->clinic_location_address }}, {{ $row->state_name }}, {{ $row->clinic_location_city }}, {{ $row->clinic_location_zip }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Open to Ages:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam2" value="{{ $row->clinic_open_age_from }} To {{$row->clinic_open_age_to  }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputname2" class="col-sm-2 col-form-label">Assigned Waiver:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam2" value="{{ assigned_waiver($row->waiver_name) }}" readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputname1" class="col-sm-2 col-form-label">Clinic Date:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputnam1" value="{{ \Carbon\Carbon::parse($row->clinic_date)->format('m/d/Y')}}" readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Clinic Time:</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" value="{{ date('h:i a', strtotime($row->clinic_time)) }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Check in Time:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword3" value="{{ date('h:i a', strtotime($row->clinic_in_time)) }}" readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Clinic Cost:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword3" value="${{ $row->clinic_cost }}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword4" class="col-sm-2 col-form-label">What to Wear:</label>
                      <div class="col-sm-10">
                        {{-- <input type="tel" class="form-control" id="inputPassword4" value="{{ $row->clinic_what_to_wear }}" readonly> --}}
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Uniform Type</th>
                              <th>Sports Bra</th>
                              <th>Shorts</th>
                              <th>T-shirt</th>
                              <th>Bow</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($clinicDetails as $clinicDetail)
                              <tr>
                                <td>{{ $clinicDetail->uniform_type }}</td>
                                @isset($clinicDetail->sports_bra)
                                  <td>{{ $clinicDetail->sports_bra }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                                @isset($clinicDetail->shorts)
                                  <td>{{ $clinicDetail->shorts }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                                @isset($clinicDetail->t_shirt)
                                  <td>{{ $clinicDetail->t_shirt }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                                @isset($clinicDetail->bow)
                                  <td>{{ $clinicDetail->bow }}</td>
                                @else
                                  <td>-</td>
                                @endisset
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                        {{-- @foreach($clinicDetails as $clinicDetail)
                          <p>{{ $clinicDetail->uniform_type }}</p>
                        @endforeach --}}
                        {{-- <div class="row">
                          @isset($clinicDetails->freshman)
                            <div class="col-4"><strong class="mr-3">Freshman:</strong></div> <div class="col-8"> <i class="fas fa-check-circle"></i> </div>
                          @else
                          <div class="col-4"><strong class="mr-3">Freshman:</strong></div> <div class="col-8"> <i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div>
                        <div class="row">
                          @isset($clinicDetails->sportsbra)
                            <div class="col-4"><strong class="mr-3">Sports Bra:</strong></div> <div class="col-8"> <i class="fas fa-check-circle"></i> {{ $clinicDetails->sportsbra }}</div>
                          @else
                          <div class="col-4"><strong class="mr-3">Sports Bra:</strong> </div> <div class="col-8"><i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div>
                        <div class="row">
                          @isset($clinicDetails->shorts)
                            <div class="col-4"><strong class="mr-3">Shorts:</strong> </div> <div class="col-8"><i class="fas fa-check-circle"></i> {{ $clinicDetails->shorts }}</div>
                          @else
                          <div class="col-4"><strong class="mr-3">Shorts:</strong></div> <div class="col-8"> <i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div>
                        <div class="row">
                          @isset($clinicDetails->t_shirt)
                            <div class="col-4"><strong class="mr-3">T-Shirt:</strong> </div> <div class="col-8"><i class="fas fa-check-circle"></i> {{ $clinicDetails->t_shirt }}</div>
                          @else
                          <div class="col-4"><strong class="mr-3">T-Shirt:</strong>  </div> <div class="col-8"><i class="fas fa-times-circle"></i></div>
                          @endisset
                        </div> --}}
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword5" class="col-sm-2 col-form-label">Pay by Date:</label>
                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPassword4" value="{{ \Carbon\Carbon::parse($row->clinic_pay_by_date)->format('m/d/Y')}}" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 btns">
                        @if (Auth::check())

                        @if(Auth::user()->type!="Coach")
                            @if($enrollCheck)
                            @if($enrollCheck->member_id!=Auth::user()->id)
                            <a href="{{ url('clinic/enroll/'.$id) }}"  class="btn btn-primary float-left">Enroll</a>

                            @else
                            <span style="height: 72.4px;" class="alert alert-info float-left">You are already enrolled. </span>
                            {{-- @dd($enrollCheck) --}}
                            {{--  Waiver check  --}}
                            @if($enrollCheck->waiver_status==0)
                            @if($row->clinic_assigned_waiver == 1)
                            <span class="alert alert-danger float-left ml-2">Waiver not signed: <i class="fas fa-times"></i><button type="button" class="btn btn-dark rounded shadow-lg text-white ml-2" data-toggle="modal" data-target="#waiverA">Sign Now</button></span>
                            @elseif($row->clinic_assigned_waiver == 2)
                            <span class="alert alert-danger float-left ml-2">Waiver not signed: <i class="fas fa-times"></i><button type="button" class="btn btn-dark rounded shadow-lg text-white ml-2" data-toggle="modal" data-target="#waiverB">Sign Now</button></span>
                            @endif
                            @elseif($enrollCheck->waiver_status==1)
                            <span style="height: 72.4px;" class="alert alert-success float-left ml-2">Waiver signed: <i class="fas fa-check"></i></span>
                            @endif
                            {{--  Payment check  --}}
                            @if($enrollCheck->payment_status==0)
                            <span class="alert alert-danger float-left ml-2">Payment not received: <i class="fas fa-times"></i><button type="button" class="btn btn-dark rounded shadow-lg text-white ml-2" data-toggle="modal" data-target="#paymentModal">Pay Now</button></span>
                            @elseif($enrollCheck->payment_status==1)
                            <span style="height: 72.4px;" class="alert alert-success float-left ml-2">Payment received: <i class="fas fa-check"></i></span>
                            @endif

                            @endif
                            @else
                            <a href="{{ url('clinic/enroll/'.$id) }}"  class="btn btn-primary float-left">Enroll</a>
                            @endif

                        @endif
                        @if(Auth::user()->type=="Coach" && $row->clinic_user_id==Auth::user()->id)
                        <div class="form-group row">
                            <div class="col-sm-12 btns">
                              <a href="{{ route('coach.clinic.enroll', $row->clinic_id) }}" class="btn btn-primary float-left"> View Registration</a>
                              <a href="{{ route('coach.clinic.edit' , $row->clinic_id) }}" class="btn btn-primary float-left ml-2">Edit Clinic</a>
                              <a href="{{ route('coach.clinic.delete' , $row->clinic_id) }}" class="btn btn-danger float-left ml-2">Delete</a>
                            </div>
                          </div>
                        @endif
                        @else
                        <button type="submit" class="btn btn-primary float-left"><a href="{{ route('login') }}">Enroll</a></button>
                        @endif

                      </div>
                    </div>
                  </form>
                    </div>

                  </div>
                </div>
                <!-- Profile -->
              </div>
            </div>
          </div>
        </div>
      </div>
</section>

  <!-- Waiver B Modal -->
<div class="modal fade" id="waiverB" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Waiver Submission</h5>
        {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">  --}}
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

<form action="{{ url('/waiverB/submit') }}"  method="post" enctype="multipart/form-data">
    @csrf

    <input type="hidden" name="clinic_id" value="{{ $row->clinic_id }}">
    @if($enrollCheck)
    <input type="hidden" name="enroll_id" value="{{ $enrollCheck->id }}">
    @endif
    <input type="hidden" name="waiver" value="{{ $row->clinic_assigned_waiver }}">
    @if(session('error_msg'))
      <div class="alert alert-danger fade in alert-dismissible show">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="font-size:20px">×</span>
          </button>
          {{ session('error_msg') }}
      </div>
    @endif
    <div class="row "  style="margin-top: 20px;">
            
      <div class="col-md-3">
                <img src="{{ url('/assets/front/images/print_img.jpg') }}" alt="" srcset="" style="width: 200px;">
            </div>
            <div class="col-md-5 ">
                <h5> {{ $school->name}}, {{ $school->address }},{{ $school->city }},{{ $school->country }}</h5>
            </div>
        </div>
        <hr>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-12">
                <div  class="row">

                    <div class="col-md-4">
                        <strong>Name:</strong>
                        {{--  <input type="text" name="name" id="name" value="{{ Auth::user()->first_name." ". Auth::user()->last_name }}">  --}}
                        <input type="text" name="name" id="name">
                    </div>
                    <div class="col-md-4">
                    <strong>Sport:</strong>
                        <input type="text" name="sport" id="" value="" id="sport">

                    </div>
                    <div class="col-md-4">
                    <strong >Date Of Birth:</strong>
                    {{--  <input type="date" name="dob" value="{{ Auth::user()->date_of_birth }}" id="dob" >  --}}
                    <input type="date" name="dob" id="dob">
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <h6 class="ml-2"><strong> Catastrophic Injury, Illness & Assumption
                        of Risk</strong></h6>
                        <div class="col-md-12">
                            <span>The possibility of sustaining a catastrophic injury that could lead to permanent disability or even death is
                                inherent in any athletic activity. I am assuming the risk that without a proper examination and physical, an
                                underlying health condition may go undetected. Therefore, the possibility exists that participation in any sports
                                program can result in serious unforeseeable medical health problems. With this information, I understand the
                                importance of rules and procedures as well as proper technique and that the possibility of a catastrophic
                                injury or death does exist even when followed to the fullest and I assume all risk and all liability for any
                                illnesses, injuries or medical conditions that may occur during my participation in any OSU Athletic activity.
                                </span>
                        </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                        <div class="col-md-6">
                            <span>Signature: </span>
                            {{-- <input type="file" name="sign_1" id="sign_1" accept="image/*" > --}}
                            <input type="text" name="sign_1">
                            {{-- <br><small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                        </div>
                        <div class="col-md-6">
                            <span>Parent/Guardian signature if under age 18: </span>
                            {{-- <input type="file" name="p_sign_1" id="p_sign_1" accept="image/*" > --}}
                            <input type="text" name="p_sign_1">
                            {{-- <br><small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                        </div>

                </div>

                <div class="row" style="margin-top: 20px;">
                    <h6 class="ml-2"><strong>Release of Liability</strong></h6>
                        <div class="col-md-12">
                            <span>Until I am officially on the roster as an Oklahoma State University intercollegiate-level student-athlete and I have had a pre-participation physical and have been cleared by an OSU Team Physician, I understand that by signing below, I certify that if I suffer any injury, illness or health related condition athletic related or not, I release Oklahoma State University of any liability, responsibility, financially or otherwise. If I have medical insurance, I agree to supply OSU with a front and back copy of my insurance card for emergency purposes.
                                </span>
                        </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                        <div class="col-md-6">
                            <span>Signature: </span>
                            {{-- <input type="file" name="sign_2" id="sign_2" accept="image/*" > --}}
                            <input type="text" name="sign_2">
                            {{-- <br><small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                        </div>
                        <div class="col-md-6">
                            <span>Parent/Guardian signature if under age 18: </span>
                            {{-- <input type="file" name="p_sign_2" id="p_sign_2" accept="image/*" > --}}
                            <input type="text" name="p_sign_2">
                            {{-- <br><small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                        </div>

                </div>

                <div class="row" style="margin-top: 20px;">
                    <h6 class="ml-2"><strong>Consent to Treat</strong></h6>
                        <div class="col-md-12">
                            <span>I give authorization to the staff Athletic Trainer and/or Team Physicians to evaluate and treat any injuries, illnesses, or medical conditions that occur during my athletic participation/tryout at Oklahoma State University (this includes immediate first aid, medication, treatment, x-ray and physical exam). I understand the Team Physician(s) have the authority to eliminate me from further participation due to an injury, illness or other medical condition and/or the undue liability risk of Oklahoma State University.
                                </span>
                        </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                        <div class="col-md-6">
                            <span>Signature: </span>
                            {{-- <input type="file" name="sign_3" id="sign_3"> --}}
                            <input type="text" name="sign_3">
                            {{-- <br><small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                        </div>
                        <div class="col-md-6">
                            <span>Parent/Guardian signature if under age 18: </span>
                            {{-- <input type="file" name="p_sign_3" id="p_sign_3"> --}}
                            <input type="text" name="p_sign_3">
                            {{-- <br><small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                        </div>

                </div>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        {{--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  --}}
        
        <button type="submit" class="btn btn-primary" id="submitButton" >Submit</button>
        <ul class="list-unstyled">
          <li>
            <div class="custom-control custom-checkbox my-1 mr-sm-2">
              <input type="checkbox" class="custom-control-input" id="customControlInlinesign" name="esign">
              <label class="custom-control-label" for="customControlInlinesign">I confirm that these signatures are mine</label>
            </div>
          </li>
        </ul>
      </div>
    </form>
    </div>
  </div>
</div>

  <!--Payment Modal -->
  <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Clinic Payment</h5>
          <div class="alert alert-light ml-3" role="alert">
            <strong>Amount: ${{ $row->clinic_cost }}</strong>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @php
            $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
          @endphp
        <section class="container-fluid inner-Page" >
            <div class="card-panel">
                <div class="media wow fadeInUp" data-wow-duration="1s">
                    <div class="companyIcon">
                    </div>
                    <div class="media-body">

                        <div class="container">
                            @if(session('success_msg'))
                            <div class="alert alert-success fade in alert-dismissible show">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true" style="font-size:20px">×</span>
                                </button>
                                {{ session('success_msg') }}
                            </div>
                            @endif
                            @if(session('error_msg'))
                            <div class="alert alert-danger fade in alert-dismissible show">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true" style="font-size:20px">×</span>
                                </button>
                                {{ session('error_msg') }}
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-12 col-md-12" style="s border-radius: 5px; padding: 10px;">
                                    <div class="panel panel-primary">
                                        <div class="creditCardForm">
                                            <div class="payment">
                                                <form id="payment-card-info" method="post" action="{{ route('dopay.online') }}">
                                                    @csrf
                                                    <input type="hidden" name="amount" value="{{ $row->clinic_cost }}">
                                                    @if($enrollCheck)
                                                    <input type="hidden" name="enroll_id" value="{{ $enrollCheck->id }}">
                                                    @endif
                                                      <div class="row">
                                                        <div class="form-group col-md">
                                                            <input type="text" class="form-control" id="firstname" name="firstName" value="{{ old('firstName') }}" required placeholder="First name">
                                                        </div>
                                                        <div class="form-group col-md">
                                                            <input type="text" class="form-control" id="lastname" name="lastName" value="{{ old('lastName') }}" required placeholder="Last name">
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="form-group col-md">
                                                          <input type="text" class="form-control" id="cardNumber" name="cardNumber" value="{{ old('cardNumber') }}" required placeholder="1111 1111 1111 1111">
                                                        </div>
                                                        <div class="form-group col-md" id="card-number-field">
                                                          <input type="number" class="form-control" id="cvv" name="cvv" value="{{ old('cvv') }}" required placeholder="CVV">
                                                        </div>
                                                        <div class="form-group col-md" id="card-number-field">
                                                          <input type="text" class="form-control" id="company" name="company" value="{{ old('company') }}" required placeholder="Company Name">
                                                        </div>
                                                      </div>
                                                        {{-- <div class="form-group col-md" >
                                                            <label for="amount">Amount</label>
                                                            <input type="number" value="{{ $row->clinic_cost }}" readonly class="form-control" id="amount" name="amount" min="1" value="{{ old('amount') }}" required>
                                                            <span id="amount-error" class="error text-red">Clinic Fee</span>
                                                        </div> --}}
                                                    {{-- </div> --}}
                                                    <div class="row">
                                                        <div class="form-group col-md">
                                                            {{-- <label>Expiration Date</label><br/> --}}
                                                            <select class="form-control" id="expiration-month" name="expiration-month">
                                                              @foreach($months as $k=>$v)
                                                                    <option value="{{ $k }}" {{ old('expiration-month') == $k ? 'selected' : '' }}>{{ $v }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md">
                                                            <select class="form-control" id="expiration-year" name="expiration-year">
                                                              @for($i = date('Y'); $i <= (date('Y') + 15); $i++)
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md">
                                                          <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required placeholder="Email">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md">
                                                            <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" required placeholder="Address">
                                                        </div>
                                                        <div class="form-group col-md">
                                                          <input type="text" class="form-control form-group" id="city" name="city" value="{{ old('city') }}" required placeholder="City">
                                                        </div>
                                                        <div class="form-group col-md">
                                                          <select class="form-control" id="state" name="state" required>
                                                            <option value="AL">Alabama</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="DC">District Of Columbia</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="IA">Iowa</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TN">Tennessee</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="UT">Utah</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>
                                                            <option value="WA">Washington</option>
                                                            <option value="WV">West Virginia</option>
                                                            <option value="WI">Wisconsin</option>
                                                            <option value="WY">Wyoming</option>
                                                          </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                      <div class="form-group col-md">
                                                        <input type="text" class="form-control form-group" id="zip" name="zip" value="{{ old('zip') }}" required placeholder="Zip">
                                                      </div>  
                                                      <div class="form-group col-md">
                                                          <input type="text" class="form-control" id="country" name="country" value="{{ old('country') }}" required placeholder="Country">
                                                      </div>
                                                      <div class="form-group col-md">
                                                        <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" required placeholder="Phone">
                                                      </div>
                                                    </div>
                                                    <div class="form-group" id="pay-now">
                                                        <button type="button" class="btn btn-warning" data-dismiss="modal">Payment later</button>
                                                        <button type="submit" class="btn btn-success themeButton" id="confirm-purchase">Confirm Payment</button>
                                                            <img src="{{ asset('assets/cards/visa.png') }}" style="width: 50px; float:right;" id="visa">
                                                            <img src="{{ asset('assets/cards/mastercard.png') }}" style="width: 50px; float:right;" id="mastercard">
                                                            <img src="{{ asset('assets/cards/amex.png') }}" style="width: 50px; float:right;" id="amex">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </section>
        </div>
      </div>
    </div>
  </div>


<!-- Waiver A Modal -->
<div class="modal fade" id="waiverA" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Waiver Submission</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <div class="modal-body">

            <form action="{{ url('/waiver/submit') }}"  method="post" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="clinic_id" value="{{ $row->clinic_id }}">
                @if($enrollCheck)
                <input type="hidden" name="enroll_id" value="{{ $enrollCheck->id }}">
                @endif
                <input type="hidden" name="waiver" value="{{ $row->clinic_assigned_waiver }}">
                @if(session('error_msg'))
                  <div class="alert alert-danger fade in alert-dismissible show">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true" style="font-size:20px">×</span>
                      </button>
                      {{ session('error_msg') }}
                  </div>
                @endif
                <div class="row "  style="margin-top: 20px;">
                    <div class="col-md-4">
                        <h6>2020 COLLEGE CHEERLEADING AND
                            DANCE TEAM NATIONAL CHAMPIONSHIP
                            Minor Release / Waiver Form
                            </h6>
                    </div>
                    <div class="col-md-4">
                        <h5>RELEASE/WAIVER FORM</h5>
                    </div>
                    <div class="col-md-3">
                        <span>Organization / Team Name
                        </span>
                        <input type="text" name="team_name_1" id="team_name">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-4">
                        <span>Minor’s Name
                        </span>
                        @if(Auth::check())
                        <input type="text" name="minor_name_1" id="minor_name" value="{{ Auth::user()->first_name.' '.Auth::user()->last_name }}">
                        @else
                          <input type="text" name="minor_name_1" id="minor_name">
                        @endif
                    </div>
                    <div class="col-md-4">
                        <span>Address
                        </span>
                        @if(Auth::check())
                        <input type="text" name="address_1" id="address" value="{{ Auth::user()->address }}">
                        @else
                          <input type="text" name="address_1" id="address">
                        @endif
                    </div>
                    <div class="col-md-4">
                        <span>City
                        </span>
                        @if(Auth::check())
                        <input type="text" name="city" id="city" value="{{ Auth::user()->city }}">
                        @else
                          <input type="text" name="city" id="city">
                        @endif
                    </div>


                </div>
                <div class="row mt-2">
                    <div class="col-md-3 ">
                        <span>State
                        </span>
                        @if(Auth::check())
                        <input type="text" name="state" id="state" value="{{ $state->name }}">
                        @else
                          <input type="text" name="state" id="state">
                        @endif
                    </div>
                    <div class="col-md-3">
                        <span>Zip
                        </span>
                        @if(Auth::check())
                        <input type="number" name="zip" id="state" value="{{ Auth::user()->zip }}">
                        @else
                          <input type="number" name="zip" id="state">
                        @endif
                    </div>
                    <div class="col-md-3">
                        <span>Phone
                        </span>
                        @if(Auth::check())
                        <input type="text" name="phone" id="phone" value="{{ Auth::user()->phone }}">
                        @else
                          <input type="text" name="phone" id="phone">
                        @endif
                    </div>
                    <div class="col-md-3">
                        <span>Email
                        </span>
                        @if(Auth::check())
                        <input type="email" name="email" id="email" value="{{ Auth::user()->email }}">
                        @else
                          <input type="email" name="email" id="email">
                        @endif
                    </div>

                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                       <small>As used below, “Varsity” shall mean Varsity Spirit LLC and their subsidiary and other affiliated companies, and the officers, directors, employees, agents, successors and assigns
                        of each of the foregoing; and “Disney” shall mean Disney Destinations, LLC, Walt Disney Parks and Resorts U.S., Inc., and their respective parent, subsidiary and other affiliated
                        or related companies, and the officers, directors, employees, agents, successors and assigns of each of the foregoing.
                        TERMS AND CONDITIONS OF PARTICIPATION - READ CAREFULLY BEFORE</small>
                    </div>
                </div>
                <div class="row justify-content-center mt-2">
                    <h6> <b> TERMS AND CONDITIONS OF PARTICIPATION - READ CAREFULLY BEFORE SIGNING</b></h6>
                </div>

                <div class="row mt-2">
                    <div class="col-md-12">
                        <small>In consideration of my minor child or ward’s participation in the cheerleading, dance or other activities conducted by Varsity at the Walt Disney World ® Resort on or about
                            January 15-21, 2020 pursuant to the 2020 College Cheerleading and Dance Team National Championship (the “Event”), wherever the Event and/or activities may occur, you hereby
                            attest that, after reading this Form completely and carefully, including the notice above your signature, as required by Florida Statutes 744.301, you acknowledge that participation
                            in the Event by your minor child or ward is entirely voluntary, and that you understand and agree as follows:</small>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12 mt-1">
                        <small><b class="mr-2">RELEASE OF LIABILITY:</b>  I agree, on behalf of my child or ward, to waive and release all liabilities, claims, actions, damages, costs or expenses of any nature (“Claims”) associated
                            with all risks that are inherent to his or her participation in the Event or other activities conducted in conjunction there with (which risks may include, among other things, exposure to
                            Naegliria Fowlerii and coliform bacteria, muscle injuries, heat and stress related issues, cuts, lacerations and broken bones), whether such risks are open and obvious or otherwise.
                            Further on behalf of myself and my minor child or ward, I hereby release, covenant not to sue, and forever discharge the Released Parties (as defined under “INDEMNITY/
                            INSURANCE” below) of and from all Claims arising in any manner out of or in anyway connected with my child’s or ward’s participation in the Event.</small>
                    </div>
                    <div class="col-md-12 mt-1">
                        <small><b class="mr-2">INDEMNITY/INSURANCE:</b>  I agree to indemnify and hold each of Disney Destinations, LLC, Walt Disney Parks and Resorts U.S., Inc., ESPN, Inc. and each of their respective
                            parent, subsidiary and other affiliated or related companies; Varsity Spirit, LLC, all Event sponsors and charities having a presence at the Event and their respective parent,
                            subsidiary and other affiliated or related companies; Reedy Creek Improvement District and its Board of Supervisors; and the officers, directors, employees, agents, contractors,
                            subcontractors, representatives, successors, assigns, and volunteers of each of the foregoing entities (collectively, the “Released Parties”) harmless from and against any and all
                            Claims arising out of or in anyway connected with my child’s or ward’s participation in the Event, wherever the Event may occur, including, but not limited to, all attorneys’ fees
                            and disbursements through and including any appeal. I understand and agree that this indemnity includes any Claims based on the negligence, action or inaction of any of the
                            Released Parties and covers bodily injury (including death), property damage, and loss by theft or otherwise, whether suffered by me or my child or ward either before, during
                            or after participation in the Event. I agree that I am not relying on the Released Parties to have arranged for, or carry, any insurance of any kind for my benefit or that of my child
                            or ward relative to my child’s or ward’s participation in the activities and the Event, and that I am solely responsible for obtaining any mandatory or desired life, travel, accident,
                            property, or other insurance related to my child’s or ward’s participation in the Event, at my own expense.</small>
                    </div>

                    <div class="col-md-12 mt-1">
                        <small><b class="mr-2">PHYSICAL CONDITION/MEDICAL AUTHORIZATION:</b>  I agree to indemnify and hold each of Disney Destinations, LLC, Walt Disney Parks and Resorts U.S., Inc., ESPN, Inc. and each of their respective
                            parent, subsidiary and other affiliated or related companies; Varsity Spirit, LLC, all Event sponsors and charities having a presence at the Event and their respective parent,
                            subsidiary and other affiliated or related companies; Reedy Creek Improvement District and its Board of Supervisors; and the officers, directors, employees, agents, contractors,
                            subcontractors, representatives, successors, assigns, and volunteers of each of the foregoing entities (collectively, the “Released Parties”) harmless from and against any and all
                            Claims arising out of or in anyway connected with my child’s or ward’s participation in the Event, wherever the Event may occur, including, but not limited to, all attorneys’ fees
                            and disbursements through and including any appeal. I understand and agree that this indemnity includes any Claims based on the negligence, action or inaction of any of the
                            Released Parties and covers bodily injury (including death), property damage, and loss by theft or otherwise, whether suffered by me or my child or ward either before, during
                            or after participation in the Event. I agree that I am not relying on the Released Parties to have arranged for, or carry, any insurance of any kind for my benefit or that of my child
                            or ward relative to my child’s or ward’s participation in the activities and the Event, and that I am solely responsible for obtaining any mandatory or desired life, travel, accident,
                            property, or other insurance related to my child’s or ward’s participation in the Event, at my own expense.</small>
                    </div>
                    <div class="col-md-12 mt-1">
                        <small><b class="mr-2">EQUIPMENT AND FACILITIES INSPECTION: </b>  I, or my child or ward if I am not in attendance at the Event, will immediately advise the Event manager of any unsafe condition that
                            I, or my child or ward if I am not in attendance at the Event, observe. My child or ward will refuse to participate, and I will refuse to let my child or ward participate, in the Event
                            until all unsafe conditions observed by me, or my child or ward, have been remedied.</small>
                    </div>
                    <br>
                    <br>

                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12 ">
                        <small><b class="mr-2">PUBLICITY RIGHTS:  </b>  I further grant the Released Parties the right to photograph, record and/or videotape me and my child or ward and further to display, edit, use and/or
                            otherwise exploit my or my child’s or ward’s name, face, likeness, voice, and appearance, in all media, whether now known or here after devised (including, without limitation, in
                            computer or other device applications, online webcasts, television programming (including broadcasts on ESPN platforms), in motion pictures, films, newspapers, and magazines)
                            and in all forms including, without limitation, digitized images or video, throughout the universe in perpetuity, whether for advertising, publicity, or promotional purposes, including,
                            without limitation, publication and use of Event results and standings, without compensation, residual obligations, reservation or limitation, or further approval, and I agree to
                            indemnify and hold harmless the Released Parties for any Claims associated with such grant and right to use. The Released Parties are, however, under no obligation to exercise
                            any rights granted herein</small>
                    </div>
                    <div class="col-md-12 mt-2">
                        <small><b class="mr-2">GOVERNING LAW:  </b> This Form will be governed by the laws of the State of Florida, and any legal action relating to or arising out of this Form will be commenced
                            exclusively in the Circuit Court of the Ninth Judicial Circuit in and for Orange County, Florida (or if such Circuit Court does not have jurisdiction over the subject
                            matter thereof, then to such other court sitting in such county and having subject matter jurisdiction), AND I SPECIFICALLY WAIVE THE RIGHT TO TRIAL
                            BY JURY.</small>
                    </div>

                </div>
                <div class="row justify-content-center">

                        <h5>NOTICE TO THE MINOR CHILD’S NATURAL GUARDIAN(S) </h5>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>READ THIS FORM COMPLETELY AND CAREFULLY. YOU ARE AGREEING TO LET YOUR MINOR CHILD ENGAGE IN
                            A POTENTIALLY DANGEROUS ACTIVITY. YOU ARE AGREEING THAT, EVEN IF EACH OF THE RELEASED PARTIES
                            (THAT IS, WALT DISNEY PARKS AND RESORTS U.S., INC., DISNEY DESTINATIONS, LLC, ESPN, INC. AND THEIR
                            RESPECTIVE PARENT, SUBSIDIARY AND OTHER AFFILIATED OR RELATED COMPANIES (COLLECTIVELY, THE “DISNEY
                            COMPANIES”); VARSITY SPIRIT, LLC, ALL EVENT SPONSORS AND CHARITIES HAVING A PRESENCE AT THE EVENT
                            AND THEIR RESPECTIVE PARENT, SUBSIDIARY AND OTHER AFFILIATED OR RELATED COMPANIES (COLLECTIVELY,
                            THE “EVENT HOST/SPONSORS/CHARITIES”); REEDY CREEK IMPROVEMENT DISTRICT AND ITS BOARD OF
                            SUPERVISORS (COLLECTIVELY, “RCID”); AND THE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, CONTRACTORS,
                            SUBCONTRACTORS, REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF EACH OF THE FOREGOING
                            ENTITIES) USE REASONABLE CARE IN PROVIDING THIS ACTIVITY, THERE IS A CHANCE YOUR CHILD MAY BE
                            SERIOUSLY INJURED OR KILLED BY PARTICIPATING IN THIS ACTIVITY BECAUSE THERE ARE CERTAIN
                            DANGERS INHERENT IN THE ACTIVITY THAT CANNOT BE AVOIDED OR ELIMINATED. BY SIGNING THIS FORM YOU</h6>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-4 ">
                        <span>Minor’s Name
                        </span>
                        <br>
                        @if(Auth::check())
                        <input type="text" name="minor_name_2" id="minor_name_1" value="{{ Auth::user()->first_name.' '.Auth::user()->last_name }}" >
                        @else
                          <input type="text" name="minor_name_2" id="minor_name_1">
                        @endif
                    </div>
                    <div class="col-md-4">
                        <span>Organization/ Team Name
                        </span>
                        <input type="text" name="team_name_2" id="team_name_1">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <small>ARE GIVING UP YOUR CHILD’S RIGHT AND YOUR RIGHT TO RECOVER FROM THE RELEASED PARTIES (THAT IS,
                            WALT DISNEY PARKS AND RESORTS U.S., INC. AND THE OTHER DISNEY COMPANIES; THE EVENT HOST/SPONSORS/
                            CHARITIES; RCID; AND THE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, CONTRACTORS, SUBCONTRACTORS,
                            REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF EACH OF THE FOREGOING ENTITIES) IN A
                            LAWSUIT FOR ANY PERSONAL INJURY, INCLUDING DEATH, TO YOUR CHILD OR ANY PROPERTY DAMAGE THAT
                            RESULTS FROM THE RISKS THAT ARE A NATURAL PART OF THE ACTIVITY. YOU HAVE THE RIGHT TO REFUSE TO SIGN
                            THIS FORM, AND THE RELEASED PARTIES (THAT IS, WALT DISNEY PARKS AND RESORTS U.S., INC. AND THE OTHER
                            DISNEY COMPANIES; THE EVENT HOST/SPONSORS/CHARITIES; RCID; AND THE OFFICERS, DIRECTORS, EMPLOYEES,
                            AGENTS, CONTRACTORS, SUBCONTRACTORS, REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF
                            EACH OF THE FOREGOING ENTITIES) HAVE THE RIGHT TO REFUSE TO LET YOUR CHILD PARTICIPATE IF YOU DO NOT
                            SIGN THIS FORM.
                            THE NOTICE ABOVE IS ALSO GIVEN AND APPLICABLE TO LEGAL GUARDIANS AND THEIR MINOR WARD(S) WHO
                            YOU ARE AGREEING TO LET ENGAGE IN POTENTIALLY DANGEROUS ACTIVITIES. ALL REFERENCES TO “CHILD”
                            ABOVE ARE APPLICABLE YOUR MINOR WARD(S) AND YOUR AND YOUR WARD’S RIGHTS TO RECOVER FROM THE
                            RELEASED PARTIES (THAT IS, WALT DISNEY PARKS AND RESORTS U.S., INC. AND THE OTHER DISNEY COMPANIES;
                            THE EVENT HOST/SPONSORS/CHARITIES; RCID; AND THE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS,
                            CONTRACTORS, SUBCONTRACTORS, REPRESENTATIVES, SUCCESSORS, ASSIGNS AND VOLUNTEERS OF EACH
                            OF THE FOREGOING ENTITIES).</small>
                    </div>

                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <span>Signature of Parent(s) or Legal Guardian(s)</span>
                        {{-- <input type="file" name="sign_1" id="" > <br> --}}
                        <input type="text" name="sign_1"> <br>
                        {{-- <small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                    </div>
                    <div class="col-md-6">
                        <span>Witness</span>
                        <input type="text" name="witness_1" id="" >
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">

                        <span><b>SUPERVISION:</b> A chaperone/adult (age 21 or over) is required to attend with participants. This chaperone will be responsible for the participants at all times. I/we acknowledge
                            that Disney and Varsity are not responsible for supervising my/our child.</span>
                    </div>
                    <div class="col-md-12">

                        <span><b>RESPONSIBILITY DISCLOSURE NOTICE:</b> Varsity acts only as an agent in connection with the tour offered herein and its liability is limited. The travel services including air
                            transportation, carriage by land, hotel accommodations, restaurants, and related services are provided by independent third parties not under the control of Varsity. Varsity
                            shall NOT bear any liability to the passenger or any person claiming by or through the passenger for any injury, damage, loss, accident, delay, or irregularity which may be
                            occasioned either by reason of or through the acts or defaults of any company or person engaged in conveying the passengers or in carrying out the arrangements of the tour
                            and/or performance events, venues, etc. as a direct or indirect result of acts of God, dangers incident to fire, breakdown in machinery or equipment, acts of governments or other
                            authorities, civil disturbances, strikes, riots, acts of terrorism, theft, unhealthy conditions, pilferage, epidemics, quarantines, medical or customs regulations, or from any other
                            cause beyond the control of Varsity. Varsity shall not be liable for any losses or additional expenses due to delay or changes in schedule or other causes. The right is reserved to
                            decline, to accept, or to retain any tour passenger should such person’s health or general deportment impede the operation of the tour to the detriment of other passengers. No
                            refunds for your portions of unused services can be made unless agreed to prior to the scheduled deadlines. Your retention of tickets, reservations, or bookings after issuance
                            shall constitute a consent to the above and agreement on your part to convey the contents herein to your traveling companions. Payment of any deposit or final payment shall be
                            deemed to constitute consent by each passenger to these terms. Baggage is carried at the owner’s risk and baggage insurance is strongly recommended. It is also recommended
                            that each participant in this tour have his or her own attorney review this RESPONSIBILITY DISCLOSURE NOTICE before indicating his or her consent by signing this consent
                            form. Nothing in this paragraph is intended to or shall affect in any way the respective rights or relationship between Varsity and any person other than the passenger and any
                            person claiming by or through the passenger.</span>
                    </div>
                    <hr>
                    <div class="col-md-12">

                        <span><b class="mr-2">MEDICAL RELEASE:</b> I/we authorize Disney and/or Varsity to procure at my/our expense, any medical care reasonably required by my/our child during his/her visit at hospitals or
                            facilities chosen by Disney and/or Varsity. I/we have listed below any medication that my/our child is currently taking. I/we will ensure that my/our child brings the medication with
                            him/her to the Walt Disney World® Resort and that my/our child is responsible for taking the medication. I/we have also listed below any medications my/our child is allergic to.
                            By signing below, I certify that: (1) I fully and completely read and understand this Form; (2) I am 18 years of age or older; (3) I am the legal guardian of the minor child identified
                            above; (4) the information set forth above pertaining to my child or ward is true and complete; and (5) I consent and agree to all of the foregoing on behalf of myself and my minor
                            child or ward identified above.</span>
                    </div>


                </div>
              <div class="row mt-4">
                    <div class="col-4">
                        <span>Medications my/our child is taking (if any):</span>
                        <input type="text" name="m_taking">
                    </div>
                    <div class="col-4">
                        <span>Medications my/our child is allergic to (if any):</span>
                        <input type="text" name="m_allergic">
                    </div>
                    <div class="col-4">
                        <span>Organization / Team Name (of child):</span>
                        <input type="text" name="team_name_3">
                    </div>
                    <div class="col-4 mt-2">
                        <span>Minor’s Name : </span>
                        @if(Auth::check())
                        <input type="text" name="minor_name_3" value="{{ Auth::user()->first_name.' '.Auth::user()->last_name }}">
                        @else
                          <input type="text" name="minor_name_3">
                        @endif
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <span>Signature of Parent(s) or Legal Guardian(s)</span>
                        {{-- <input type="file" name="sign_2" id="" > --}}
                        <input type="text" name="sign_2"> <br>
                        {{-- <br><small>(Please sign a white paper, take a picture of it and upload it here for verification purposes)</small> --}}
                    </div>
                    <div class="col-md-6">
                        <span>Witness</span>
                        <input type="text" name="witness_2" id="" >
                    </div>
                </div>

                <div class="row justify-content-center">
                    <h6><b>EMERGENCY INFORMATION: (Not traveling with the minor)</b> </h6>

                </div>
                <div class="row mt-2">
                    <div class="col-md-4">
                        <span>Name</span>
                        <input type="text" name="name" id="">
                    </div>
                    <div class="col-md-4">
                        <span>address</span>
                        <input type="text" name="address_2" id="">
                    </div>
                    <div class="col-md-4">
                        <span>telephone</span>
                        <input type="text" name="telephone" id="">
                    </div>
                    <div class="col-md-4 mt-2">
                        <span>home</span>
                        <input type="text" name="home" id="">
                    </div>
                    <div class="col-md-4 mt-2">
                        <span>work</span>
                        <input type="text" name="work" id="">
                    </div>

                </div>
                <div class="row">

                    <span>EVERY PARENT OR LEGAL GUARDIAN OF A MINOR PARTICIPATING IN THE EVENT MUST COMPLETE THIS FORM AND TURN IN TO THE COACH TO BE MAILED TO THE VARSITY OFFICE NO LATER THAN 3 WEEKS PRIOR TO THE EVENT.</span>

                </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        <ul class="list-unstyled">
          <li>
            <div class="custom-control custom-checkbox my-1 mr-sm-2">
              <input type="checkbox" class="custom-control-input" id="customControlInlinesign" name="esign">
              <label class="custom-control-label" for="customControlInlinesign">I confirm that these signatures are mine</label>
            </div>
          </li>
        </ul>
      </div>
    </form>
    </div>
  </div>
</div>

@endsection
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js"></script>
<script>
  $(document).ready(function() {
    // $('#paymentModal').modal('show')
    // $('#exampleModal').modal('show')
    // $('#waiverModal').modal('show')
    @if(session('waiver_submit') )
    Swal.fire({
        position: 'top-center',
        icon: 'success',
        title: 'Your waiver submited successfuly',
        showConfirmButton: false,
        timer: 1500
      })
    $('#paymentModal').modal('show')

    @endif
    @if (request()->waiver=="2")
    $('#waiverB').modal('show')
    @endif
    @if (request()->waiver=="1")
    $('#waiverA').modal('show')
    @endif
    {{--  $('#submitButton').click( function() {
        let frm_data=new FormData();
        frm_data.append("sign_1",document.getElementById('sign_1').value);
        frm_data.append("p_sign_1",document.getElementById('p_sign_1').value);
        axios.post("{{ url('/waiverB/submit') }}",frm_data).then((res)=>{

        });

  });  --}}
});
</script>

