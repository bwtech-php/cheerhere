@extends('layouts.master')
@section('mainContent')
{{-- @dd($__data) --}}
<section class="main-content inner-page">
    <div class="owl-carousel owl-theme">
        <div class="item">
            <img src="/assets/front/images/about-banner.jpg" alt="images not found">
            <div class="cover">
                <div class="container">
                    <div class="header-content">
                        <!-- <div class="line"></div> -->
                        <h1>Clinics</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="main-clinics-sec">
    <div class="container">
        {{-- <div class="row text-center">
            <div class="col-lg-12">
                <h2 class="sec-heading">The World's Largest Selection of Clinics</h2>
                <p class="sec-para">Choose from 100,000 online video clinics with new additions published every month</p>
            </div>
        </div> --}}
        <div class="row mb-4">

             @forelse ($clinics as $clinic)

              <div class="col-lg-3">
                <div class="clinic-box">
                  <div class="img-box">
                  <img src="{{ asset('img/schools/'. $clinic->image) }}" class="img-fluid" alt="">
                  </div>
                  <div class="overlay">
                    <h4>{{ $clinic->school->name ?? null }}</h4>
                    {{--  <h5>{{ assigned_waiver($clinic->assigned_waiver) }}</h5>  --}}
                    <h5><i  class="far fa-calendar-check"></i> {{ \Carbon\Carbon::parse($clinic->pay_by_date)->format('m/d/Y')}} <br><small class="ml-2"><i class="far fa-clock"></i> {{ date('h:i a', strtotime($clinic->time)) }}</small></h5>
                    <a href="{{ route('clinic.detail', $clinic->id) }}" class="btn">Click Here To Register<i class="fas fa-angle-right ml-2"></i></a>
                  </div>
                </div>
              </div>
              @empty
              <p>We apologize, there are currently no clinics scheduled.  Please check back regularly or register today to be notified as soon as clinics are announced</p>
              @endforelse
        </div>

    </div>
</section>
<!-- Clinics -->
<!-- Video -->
{{-- <section class="main-video-sec">
    <div class="container">
        <div class="video-in">
            <div class="row">
                <div class="col-lg-6">
                    <div class="img-box"><img src="/assets/front/images/video-img.jpg" class="img-fluid" alt=""></div>
                    <div class="overlay">
                        <a class="popup-youtube" href="https://youtu.be/6rSSXxIL1RQ"><img src="/assets/front/images/play-icon.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="content">
                        <h2 class="sec-heading">Summer Camp - <br>Make The Connection!</h2>
                        <p class="sec-para">Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird, wenn er sich ein Layout ansieht. Der Punkt, Lorem Ipsum zu nutzen, ist, dass es mehr oder weniger die.</p>
                        <p class="sec-para">Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird, wenn er sich ein Layout ansieht. Der Punkt, Lorem Ipsum zu nutzen, ist, dass es mehr oder weniger die normale ist. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- Video -->

@endsection
