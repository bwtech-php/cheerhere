<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaiverBSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waiver_b_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('team_name_1')->nullable();
            $table->string('minor_name_1')->nullable();
            $table->string('address_1')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('team_name_2')->nullable();
            $table->string('minor_name_2')->nullable();
            $table->string('sign_1')->nullable();
            $table->string('witness_1')->nullable();
            $table->string('
            m_taking')->nullable();
            $table->string('m_allergic')->nullable();
            $table->string('team_name_3')->nullable();
            $table->string('minor_name_3')->nullable();
            $table->string('sign_2')->nullable();
            $table->string('witness_2')->nullable();
            $table->string('name')->nullable();
            $table->string('address_2')->nullable();
            $table->string('
            telephone')->nullable();
            $table->string('home')->nullable();
            $table->string('work')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waiver_b_s');
    }
}
