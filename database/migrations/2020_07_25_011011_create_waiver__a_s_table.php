<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaiverASTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waiver__a_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('sport')->nullable();
            $table->string('dob')->nullable();
            $table->string('coach_id')->nullable();
            $table->string('clinic_id')->nullable();
            $table->string('member_id')->nullable();
            $table->string('enrolls_id')->nullable();
            $table->string('date_1')->nullable();
            $table->string('sign_1')->nullable();
            $table->string('p_sign_1')->nullable();
            $table->string('date_2')->nullable();
            $table->string('sign_2')->nullable();
            $table->string('p_sign_2')->nullable();
            $table->string('date_3')->nullable();
            $table->string('sign_3')->nullable();
            $table->string('p_sign_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waiver__a_s');
    }
}
